/****************************************************************************************
 *	
 *	ED_progressBar Javascript - Sept 2008
 *	@author Paul Lu
 *	Create and use Entity Describer Progress Bar
 *	
 *	requires jQuery v1.2 or later
 *
 *			
 ****************************************************************************************/

//ProgressBar values
	var __PROGRESS__ = 0; //curent progress count
	var __TOTAL_PROGRESS__ = 0; //total/final progress count	
	var __PROGRESS_ERROR_COUNT__ = 0; //current # of errors/restarts encountered during task (specifically getting submit status)
	var __PROGRESS_ERROR_LIMIT__ = 2; //Total # of errors/restarts available before ending attempt
		
/****************************************************************************************
 *	Creates a Progress Bar div and appends it to the body
 *	of the HTML page.  Only needs to be called once to create it
 */
function createProgressBar()
{
	var progressdiv = $('<div class="progressDisplay" id="progressDisplay" name="progressDisplay"></div>')
						.append('<h1 id="progressMessage" name="progressMessage"></h1>')
						.append('<p style="align:center;"><center>' + 
							'<table id=\'progressbar\' name=\'progressbar\' width=\'380px\' height=\'20px\' ><tr>' + 
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'<td bgcolor=\'#CCCCCC\'></td>' +
					   		'</tr>' +
							'</table> <img src="./ed/images/bigrotation2.gif"/> <span id="percentDone" name="percentDone" style="margin-bottom: 10px">0%</span></center>' + 
					   		'</p>')
							.css("overflow","auto")
							.css("display","none")
							.css("align","center")
							.css("opacity","100")
							.css("z-index","3")
							.css("width","70%").css("height","30%")
							.css("left","13%").css("top","30%")
					  		.css("position","absolute") 
							.css("border","3px solid black")
							.css("backgroundColor","#FFFFFF")
							.css("padding", "20px");
		 
		 $('body').append(progressdiv);
}

/****************************************************************************************
 *	Activates the Progress Bar for use and shows it.  Called when you want to start the
 *	Progress Bar
 *	
 *	@param totalIncrements
 *							The number of increments expected before task is complete
 *	@param message
 *							The message to be displayed in the Progress Bar eg. Loading...
 */
function activateProgressBar(totalIncrements, message)
{
	
	__TOTAL_PROGRESS__ = totalIncrements;
	$('#progressMessage').text(message);
	showProgressBar();
}

/****************************************************************************************
 *	Shows the ProgressBar
 *	
 */
function showProgressBar()
{
	$('#progressDisplay').show();
}

/****************************************************************************************
 *	Finishes the Progress Bar and resets its values.  Called when the task is complete
 *	
 */

function finishProgressBar()
{	
	__PROGRESS_ERROR_COUNT__ = 0;
	resetProgressBar();
	hideProgressBar();
	
	return true;
}

/****************************************************************************************
 *	Reset the values in the ProgressBar
 *	
 */

function resetProgressBar()
{
	__PROGRESS__ = 0;
	__TOTAL_PROGRESS__ = 0;
	$('#progressMessage').text('');
	
	var prog_bits = $('#progressbar').children('tbody').children('tr').children('td');
	
	for(var i = 0 ; i < 10 ; i++)
	{
		prog_bits[i].bgColor = "#CCCCCC";
	}
	
	$('#percentDone').text('0%');
	
	return true;	
}

/****************************************************************************************
 *	Hides the ProgressBar
 *	
 */
function hideProgressBar()
{
	$('#progressDisplay').hide();
}

/****************************************************************************************
 *	Increments the ProgressBar.  Called when part of the task is complete
 *	
 */
function incrementProgressBar()
{
	__PROGRESS__++;
	
	var new_percentDone = Math.floor((__PROGRESS__ / __TOTAL_PROGRESS__)*100);
		
	 
	setProgressBarValue(new_percentDone);
}

/****************************************************************************************
 *	Sets the value of the Progress Bar - (where it is at)
 *	
 *	@param val
 *				the value (1 - 100) you want the Progress Bar set to
 */
function setProgressBarValue(val)
{
	var prog_val = Math.floor(val/10);
	var prog_bits = $('#progressbar').children('tbody').children('tr').children('td');
	if(__PROGRESS__ <= __TOTAL_PROGRESS__)
	{
			for(var i = 0 ; i < prog_val ; i++)
			{
				prog_bits[i].bgColor= "#4169e1"; 
			}
	}
	
	$('#percentDone').text(val + "%");

}

/****************************************************************************************
 *	Ajax call to get the submit status of tagging event from the SubmitStatusServlet
 *	
 */
function getSubmitTaggingStatus()
{
	$.ajax({
		   type: "GET",
		   url: __URL_ROOT__ + 'TaggingProgressServlet',
		   data: "",
		   dataType: "jsonp",
		   cache: false,
		   success: processStatus,
		   error: function(XMLHttpRequest, textStatus, errorThrown) {
					processStatus('error');
			   		}
			 });
}

/****************************************************************************************
 *	Called when something has been submitted and the SubmitStatusServlet is in use as well
 *	
 */
function startSubmitTaggingStatus()
{
	//makeAjax call here to get submission status
	activateProgressBar(10, 'Tagging...')
	getSubmitTaggingStatus();	
}

/****************************************************************************************
 *	Function called on response from Ajax call to submitStatusServlet
 *	
 */
function processStatus(data)
{
		if(data == 'error' && __PROGRESS_ERROR_COUNT__ < __PROGRES_ERROR_LIMIT__)
		{
			setTimeout('getSubmitTaggingStatus()', 40);
			__PROGRESS_ERROR_COUNT__++;
			return;			
		}
		
		if(data.tagged == data.total && data.tagged != 0)
		{
			setProgressBarValue(100);
			return;
		}else{
			if(data.progress != 0)
			{
				setProgressBarValue(data.progress);
			}
			setTimeout('getSubmitTaggingStatus()', 40);
			return;
		}	
}

/****************************************************************************************
 *	Binds a submit Progress Tracking action to the name of the submit_button such that
 *	when the button is pressed, the Progress Bar is activated and begins checking the 
 *	submit status from the submitStatusServlet
 *	
 *	@param submit_button
 *							name of the submit button
 */
function bindsubmitProgressTracking(submit_button)
{
	var input = 'input[@name=' + submit_button + ']';
	
	 $(input).bind('click', function(e,data) 
		{
			var tag_spans = $('span[@name=tag_element]');
			if(tag_spans != null && tag_spans.length != 0)
			{
				startSubmitTaggingStatus();
				return false;
			}
		})
}