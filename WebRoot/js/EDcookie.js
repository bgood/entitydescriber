/**********************************************************
 * EDcookie.js Nov 2008
 * @author Ed Kawas
 * ED Cookie JavaScript contains functions for creating,
 * reading and erasing cookies from Javascript
 *
 * Originally part of ed_connotea.js
 * 
 *
 **********************************************************/

	/* create a cookie with a specific name, value and expires in x days*/
	function createCookie(name,value,days) 
	{
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
		
		//do not attach "path=/" since ED_HASH cookie is created in a servlet 
		if(name == "ED_HASH"){
		document.cookie = name+"="+value+expires+";";
		}
	}

	/* get a cookies' value by name */
	function readCookie(name) 
	{
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	/* delete a cookie by name */
	function eraseCookie(name) 
	{
		createCookie(name,"",-1);
	}

	/* method that deletes the users ED_USERNAME */
	function logout() 
	{
		if (confirm('Are you sure that you wish to log out of E.D?')) {
		
			eraseCookie('ED_USERNAME');
			eraseCookie('ed_user_topics');
			eraseCookie('ED_HASH');
			location.href= __URL_ROOT__ + 'openid.html';
			window.close();
		}
	}

