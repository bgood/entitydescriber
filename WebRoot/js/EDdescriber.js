/**************************************************************************
 * EDdescriber.js Nov 2008
 * @author Paul Lu
 * Essentially like ed_connotea.js with minor modifications that handle
 * not having connotea javascript available.  
 * Contains functions for creating the ED components such as freebase suggest
 * Freebase Suggest with custom data source: free-text tags
 * Type Chooser (to restrict the type of tags being presented)
 * Creating tagging xml
 * Submitting tagging xml to the SPARQLTaggingServlet
 * Logout function
 *
 **************************************************************************/

var __guid__ = new Array();
var __SEMANTIC__ = 'semantic_';
var __FREE__ = 'free_';	
var __TYPE__ = 'type_';
var __D_E_B_U_G__ = false;

//Parameters jquery form submit
var EDTaggingOptions = {
							type: 		"POST",
							url:		'SPARQLTaggingServlet',
							dataType:	"jsonp",
							cache:		false,
							success: 	handleTaggingResponse,
							error:		handleError
						}		
							
/**
 * SET UP THE BOOKMARK DESCRIBER PAGE
 **/	
	$(document).ready(function()
	{
		//set the top right corner options
		$('div.loggedinas').append('<p>Logged in as: <em>' + _E_D_U_S_E_R_N_A_M_E_ + '</em></p>');
		$('div.login-buttons').append('<p><a class="log" href="javascript:logout();">Log out of E.D.</a></p>');
		
		//Identified section
		if(_U_R_I_ != "")
		{
			$('body').append('<input type="hidden" value="' + _U_R_I_ + '" name="uri"/>');
			$('span.internet').append('<a class="preadd" target="_blank" title="' + _T_I_T_L_E_ + '" href="' + _U_R_I_ + '">' + _T_I_T_L_E_ + '</a>');
			var actualurl =  _U_R_I_.split("://")[1].split("?")[0];
			$('span.internet').append('<div class="actualurl">' + actualurl + '</div>');		
		}else{
				$('td.addformlabelcell:first').text('Bookmark URL:');
				$('td.identified').append('<input id="urlbox" class="textctl" type="text" size="54" value="" name="uri"/>').append('<br><span class="optional">(eg. http://www.connotea.org)</span><br><br>');
			 }
		
		//FilterByType section
		create_type_chooser('topic_choose_div');
		
		//Tagging Section
		$('#semantic_tags_div').append(maketagDisplayTable(__SEMANTIC__ + 'tag_table', 'suggest_table'));
		$('#personal_tags_div').append(maketagDisplayTable(__FREE__ + 'tag_table', 'suggest_table'));
		
		//create Suggest boxs
		var freebaseSuggestParams = get_freebase_suggest_params();
		var freetextSuggestParams = get_freetext_suggest_params();
		freetextSuggestParams.ac_param.type = '/freetext/' + _E_D_U_S_E_R_I_D_;
		$('#ed_semantic_autocomplete_div').append(makeSuggestInputBox(__SEMANTIC__ + 'textbox', 'suggest_textbox', freebaseSuggestParams));
		$('#ed_personal_autocomplete_div').append(makeSuggestInputBox(__FREE__ + 'textbox', 'suggest_textbox', freetextSuggestParams));
		
		//set the submit button
		$('#addbutton').click(
								function()
								{
									//check to make sure there is a URI
									if(_U_R_I_ != "" || $('#urlbox').val() != "")
									{ 
										if($('#urlbox').val()) //if there is a urlbox set _U_R_I_ to it
										{
											_U_R_I_  = $.trim($('#urlbox').val());
										}
										//check to make sure the uri is valid
										var regexp = /^[A-Za-z]+:(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/; 
										if(!regexp.test(_U_R_I_))
										{
											alert('Please enter a valid URL');
										}else{
											//check there are tags by looking for the default tags that are there when there are no tags
											if($('#semantic_tag_table_tag_default').text() && $('#free_tag_table_tag_default').text())
											{
												alert('Please enter a tag');
												return false;
											}else{
												//submit tags	
												submitTagging();
											}
										}
									}else{
										alert('Please enter a Bookmark URL');
									}
									return false;
								}
							 );
		
		appendDebugComponent("debug");		
	});
	
	
	/**
	 *	Returns the table where selected tags will be displayed
	 */
	function maketagDisplayTable(id, name)
	{
		var tagDisplayTable = $('<table id="' + id + '" name="' + name + '" class="tag_display_table"><tr><td><b>Selected Tags</b> (click to remove)</td></tr><tr><td><span id="' + id + '_tag_default" name="ed_tag_default">Tags will appear here</span></td></tr></table>');
		return tagDisplayTable;
	}
	
	/** 
	 *	Returns a textbox which has a freebaseSuggest bound to it.
	 *  The freebaseSuggest uses the parameters provided to it in the input parameters  
	 */
	function makeSuggestInputBox(id, name, suggestParams)
	{
		var input = $('<input id="' + id + '" name="' + name + '" type="text" class="textctl" size="54"></input>');
		
		$(input).freebaseSuggest(suggestParams);
		$(input).bind("suggest", function(e, data){
													ED_select_tag(e, data);
												  }
					 );
		//If There is a Suggest New Parameter
		if(suggestParams.suggest_new != 'undefined')
		{
			$(input).bind("fb-select-new", function(e, data) {		
																var txt = $.trim($(input).val());
																if(txt != "");
																	ED_suggest_new_tag(e, txt);
															}
						  );
		}			 
		return input;
	}
	
	/**
	 *	binds an existing input textbox to freebaseSuggest using the provided parameters
	 */
	function bindSuggest(textbox_id, suggestParams)
	{
		var input = $('#' + textbox_id);
		$(input).freebaseSuggest(suggestParams);
		$(input)
				.unbind("suggest")
				.bind("suggest", function(e, data){
													ED_select_tag(e, data);
												  }
					 );
		//If There is a Suggest New Parameter
		if(suggestParams.suggest_new != 'undefined')
		{
			$(input).bind("fb-select-new", function(e, data) {		
																var txt = $.trim($(input).val());
																if(txt != "");
																	ED_suggest_new_tag(e, txt);
															});
			
		}
	}
	
	/**
	 *	Function called when a tag is selected from FreebaseSuggest dropdown
	 */
	function ED_select_tag(e, data)
	{
		if(e.target.id == __SEMANTIC__ + 'textbox') //determine the textbox from which it came
		{
			displaySemanticTag(data);
		}
		
		if(e.target.id == __FREE__ + 'textbox')
		{
			displayFreeTag(data);
		}
		
		if(e.target.id == __TYPE__ + 'textbox')
		{
			var really = confirm("Really switch to the topic: '" + data.id + "'?");					
			if(really)
			{
				$('#' + __TYPE__ + 'textbox').val('');		
				setSearchType(data.id);				
			}
			
			
		}
	}
	
	/**
	 * Function called when the 'create new tag' option is selected from the FreebaseSuggest dropdown
	 */
	function ED_suggest_new_tag(e, txt)
	{
		if(e.target.id == __FREE__ + 'textbox')
		{
			var data = {"name": txt, "guid": "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/Nothing", "ontology": "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/"};
			displayFreeTag(data);
		}
	}
	
	/**
	 *	Function to display semantic tags
	 */
	function displaySemanticTag(data)
	{
		//remove default tag
		var default_tag_id = __SEMANTIC__ + 'tag_table_tag_default';
		if($('#' + default_tag_id) != null)
			$('#' + default_tag_id).parent().parent().remove();
		
		//Create new tag
		var txt = data.name;
		var tag_span_name = __SEMANTIC__ + 'tag_text';
		var tag_row = $('<tr><td align="center"><span style="cursor:pointer;" title="Click me to remove!" name="' + tag_span_name + '">' + txt + '</span></td></tr>');
           	//put in text of the Semantic Tag and give it the 'click to remove function
           		tag_row.click(function()
	           					{
									var tBody = $(this).parent(); //get tbody
							 		var table = $(this).parent().parent();
	           						$(this).remove();
	           			
			               			//check to see if there is only one row (header) - 
			               			if(tBody[0].rows.length == 1)
			               			{	//and put back in default tag
										var tr_default = '<tr><td align="center"><span  id="' + __SEMANTIC__ + 'tag_table_tag_default" name="ed_tag_default"></span></td></tr>'
										tr_default = $(tr_default);
										tr_default.find('span[@name=ed_tag_default]').text('Semantic Tags will appear here');
										$('#' + __SEMANTIC__ + 'tag_table').append(tr_default);
			                		}
			          			}
	                 		 ).hover(	
	                 		 			function(){$(this).css("background", "	#737CA1");},
				 			   			function(){$(this).css("background", '#cccccc');}
				 			   		);

		//Add the tag to the display table
		$('#' + __SEMANTIC__ + 'tag_table').append(tag_row);
		componentBackgroundColorFlash(tag_row, "#80C0ff", "#CCCCCC");
		$('#' + __SEMANTIC__ + 'textbox').val('').focus();
		
		if(!data.ontology)
		{
			data.ontology = 'http://www.freebase.com';
		}
		
		//Add the tag's meta data to the array of tags
		__guid__[escape(txt)] = {'guid':data.guid.substr(1), 'ontology':data.ontology, 'types': data.type, 'alias':data.alias};
		
		
	}
	
	/**
	 *	Function to display personal tags
	 */
	function displayFreeTag(data)
	{
		//remove default tag
		var default_tag_id = __FREE__ + 'tag_table_tag_default';
		if($('#' + default_tag_id) != null)
			$('#' + default_tag_id).parent().parent().remove();
		
		//Create new tag
		var txt = data.name;
		var tag_span_name = __FREE__ + 'tag_text';
		var tag_row = $('<tr><td align="center"><span style="cursor:pointer;" title="Click me to remove!" name="' + tag_span_name + '">' + txt + '</span></td></tr>');
           	//put in text of the Semantic Tag and give it the 'click to remove function
           		
           		tag_row.click(function()
           					{
								var tBody = $(this).parent(); //get tbody
						 		var table = $(this).parent().parent();
           						$(this).remove();
           			
		               			//check to see if there is only one row (header) - 
		               			if(tBody[0].rows.length == 1)
		               			{	//and put back in default tag
									var tr_default = '<tr><td align="center"><span  id="' + __FREE__ + 'tag_table_tag_default" name="ed_tag_default"></span></td></tr>'
									tr_default = $(tr_default);
									tr_default.find('span[@name=ed_tag_default]').text('Personal Tags will appear here');
									$('#' + __FREE__ + 'tag_table').append(tr_default);
		                		}
		          			}
                 		 ).hover(	
                 		 			function(){$(this).css("background", "	#737CA1");},
			 			   			function(){$(this).css("background", '#cccccc');}
			 			   		);

		//Add the tag to the display table
		$('#' + __FREE__ + 'tag_table').find('tbody tr:first').after(tag_row);
		componentBackgroundColorFlash(tag_row, "#80C0ff", "#CCCCCC");
		$('#' + __FREE__ + 'textbox').val('').focus();
		
		//Add the tag's meta data to the array of tags
		__guid__[escape(txt)] = {'guid':data.guid, 'ontology':data.ontology};
	}
	
	/**
	 *	Create Type chooser
	 */
	function create_type_chooser(typediv_id)
	{		
			var emptystr = get_default_type();
			var typeSuggestParams = get_freebase_suggest_types_params();
			var TypeInputBox = makeSuggestInputBox(__TYPE__ + 'textbox', 'suggest_textbox', typeSuggestParams);
			
			$('#' + typediv_id).append(TypeInputBox)
								.append('<br/><br/>')
								.append('<label>Recently used types: <select id="topic_select" class="textctl" name="topic_select"><option value="'+emptystr+'" selected="selected">All</option></select></label>');	
								
			// fill in the select element
		 	select = readCookie('ed_user_topics');
		 	if (select != null)
		 	{
		 		select = select.split(" ");
		 		$("#topic_select").empty();
		 		select.push(emptystr);
		 		select.sort();
		 		for (var i in select) 
		 		{
		 			$("#topic_select").append($('<option value="'+select[i]+'">' + select[i] + '</option><br/>'));
		 		}
		 		$("option[@value="+emptystr+"]").attr('selected','selected');
		 	}
		 	
		 	//set topic select function
		 	$("#topic_select").change(function(e) 
							 	{
							 		var selected = this.value;
							 		var really = true; //confirm("Really switch to the topic: '" + selected + "'?");
									if (really) {
										setSearchType(selected)
										current_topic = selected;
							
									}
							 	}
							 );							
	}
	
	/**
	 *	Restrict what type of the Suggest Search Results
	 */
	function setSearchType(current_topic)
	{
		var freebaseSuggestParams = get_freebase_suggest_params();
		freebaseSuggestParams.ac_param.type = current_topic;
		bindSuggest(__SEMANTIC__ + 'textbox', freebaseSuggestParams);
		
		$('#' + __SEMANTIC__ + 'textbox').focus();
		if ($('option[@value='+current_topic+']').length > 0) 
				{
					// already exists, just select it
					$('option[@value='+current_topic+']').attr('selected','selected')
				} else {
							// add to cookie and select box
							var select = readCookie('ed_user_topics');
							if (select == null) 
							{
								select = current_topic;
							}else{
									select += " " + current_topic;
								 }
							select = $.trim(select);
							createCookie('ed_user_topics', select, 21);
							$("#topic_select").append($('<option value="' + current_topic +'" selected="selected">' + current_topic + '</option>'));
						}
							
	}
	
	/**
	 *	Converts the tags into an xml format
	 */
	function convert_tags_to_xml(guidArray, tags, resource, userid) 
	{
			var tag_count = 0;
			var EDagent = userid;
	
	/**MAKE THE XML**/
	//HEADER
	var xml = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>';
	xml +='<tagging version="2.0"><agent>'+EDagent+'</agent><date>'+new Date().toString()+'</date><resource>'+escape(resource).replace(/&quot;/g,"\"").replace(/&apos;/g,"\'").replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&amp;/g,"&")+'</resource><tag-list>'
	
	//SEMANTIC TAGS		
	for (var i in tags) 
	{
		var name = tags[i];
		var escape_name = escape(name);
		tag_count++;
		
		if (guidArray[escape_name]) 
		{
			if(guidArray[escape_name].guid) 
			{
					var guid = guidArray[escape_name].guid;
					guid = guid.replace(/"/g,""); 
					
					var ontology = guidArray[escape_name].ontology;
									
					//if freebase - attach following prefix
						if(ontology == "http://www.freebase.com")
						{
							guid = 'http://www.freebase.com/view/guid/' + guid;
						}

					xml += '<tag ontology="' + ontology + '" name="'+ name +'" uri="' + guid + '">';
					//FREEBASE-TYPES - only if it contains /.../...
						xml += '<Freebase-Type-List>'
						if(guidArray[escape_name].types && guid != "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/Nothing")
						{
							    for (var j in guidArray[escape_name].types) 
							    	{
							    		xml += '<FType id="'+ $.trim(guidArray[escape_name].types[j].id) + '" name="' + $.trim(guidArray[escape_name].types[j].name) + '"/>'
							    	}
					    }
					     xml += '</Freebase-Type-List>'
				    //ALIASES
						xml +=  '<Alias-List>'
					    if (guidArray[escape_name].alias)
					    {
							for (var y in guidArray[escape_name].alias) 
							{
						    	xml += '<Alias name="' + guidArray[escape_name].alias[y] + '"/>'
							}
					    }
					    xml += '</Alias-List>'
				    
				xml += '</tag>'
				
			}
		} 
	}
	
	xml += '</tag-list></tagging>'
	
	if(tag_count == 0){
		return null;
	}else{
		return xml;
	}
}
	
	/**
	 *	Makes a dom element flash a color for 1 second
	 */
	function componentBackgroundColorFlash(component, flashColor, normalColor)
	{
		$(component).css("background", flashColor);
		setTimeout(
					function(thecomponent, thenormalColor)
							{
								$(thecomponent).css("background", thenormalColor);
							},1000, component, normalColor);
	}
	
	/**
	 *	Submits the tags
	 */
	function submitTagging()
	{
		var tags = new Array();
	
		var semantictagspanArray = 'span[@name=' + __SEMANTIC__ + 'tag_text]'; //all are semantic tags
		var freetagspanArray = 'span[@name=' + __FREE__ + 'tag_text]';
		

	//add the semantic tags to tags array for processing
	$(semantictagspanArray).each(function() {
										var word = $(this).text();
										tags.push($.trim(word))
									});
	//add the free text tags to tags array for processsing
	$(freetagspanArray).each(function() {
										var word = $(this).text();
										tags.push($.trim(word))
									});
									
		
		var xml = convert_tags_to_xml(__guid__, tags, _U_R_I_, _E_D_U_S_E_R_I_D_);
		
		if(__D_E_B_U_G__)
		{
			alert('DEBUG mode is on, nothing will be submited');
			$('#commentbox').val(xml);
			xml = null;	
		}
		
		if(xml != null)
		{
			createandsubmitTaggingForm(EDTaggingOptions, 'EDTagForm', get_SPARQL_EP_params(), xml, _U_R_I_);
		}
	}
	/**
	 *	-Creates a From on the fly for submitting tags to the SPARQL Endpoint and then submits it
	 *	-The actual part where the tags get sent to our taggingServlet
	 *	-Call this with the appropriate parameters and the tags will be submitted to the 
	 *	 SPARQL End point
	 *	@param options -
	 *					ajax options for ajax form submission
	 *	@param formId -
	 *					name of the form
	 *	@param SEP_params -
	 *					JSON object of SPARQL END POINT parameters
	 *	@param tagging_xml -
	 *					the tagging xml
	 *	@param resource
	 *					the uri being tagged 
	 */
	function createandsubmitTaggingForm(options, formId, SEP_params, tagging_xml, resource)
	{		
		//make ACTUAL FORM on the fly - 
			var tagging_form = $('<form id="' + formId + '"></form>');	
			
		//make hidden input on the fly - for TAGGING XML 
			var hid_tag_xml = $('<input type="hidden" name="tagging"></input>');
				$(hid_tag_xml).attr('value', tagging_xml); //set value to the tagging xml
			$(tagging_form).append(hid_tag_xml);
		
		//make hidden input on the fly - for resource
			var hid_resource = $('<input type="hidden" name="resource" value="' + resource + '"></input>');
			$(tagging_form).append(hid_resource);
			
		//Send the parameters for the SPARQL Endpoint here if SEP_params exists
			if(typeof(SEP_params) != 'undefined')
			{
				/**SPARQL ENDPOINT URL**/
				var SEPurl = SEP_params.SPARQL_EP_url;
					var hid_paramSEPurl = $('<input type="hidden" name="SPARQL_EP" value ="' + SEP_params.SPARQL_EP_url + '"></input>');
					$(tagging_form).append(hid_paramSEPurl);
				
			/**SPARQL ENDPOINT PARAMETER NAME OF query**/		
				var SEPqueryparam = SEP_params.SPARQL_EP_query_param_name;
					var hid_paramSEPquery = $('<input type="hidden" name="query_param_name" value="' + SEPqueryparam + '"></input>');
					$(tagging_form).append(hid_paramSEPquery);
			
			/**SPARQL ENDPOINT MISC parameters**/		
				for (var i in SEP_params.SPARQL_EP_misc_params) 
				{
					var hid_param = $('<input type="hidden" name="' + i + '" value="' + SEP_params.SPARQL_EP_misc_params[i] + '"></input>');
					$(tagging_form).append(hid_param);
				}
			}
			//Put hidden input: 'hid_tag_xml aka: 'tagging'' into the tagging_form
			//Put tagging_form in body
			$(tagging_form).appendTo('body');
			$(tagging_form).ajaxForm(options);
			
			//submit form
			$(tagging_form).submit();
			$(tagging_form).remove();
	}
	
	/** 
	 *	Must be implemented
	 */	
	function handleTaggingResponse(data)
	{	
		if(data.response.code == 200)
		{
			$('#form_add').submit();	
		}else{
				alert('Error occurred while tagging');
				//$('#form_add').submit();
			 }
	}	
	
	/** 
	 *	Must be implemented
	 */
	function handleError(data, message)
	{
		alert('Error occurred while tagging');
		//$('#form_add').submit();	
	}
	
	/**
	 *	Appends a debug component to the given elementid
	 */
	function appendDebugComponent(elementid)
	{		
		$('#' + elementid).append('<a style="text-decoration: none;" href="#" id="mydebugtoggle">Debug:<a/><label id="mydebuglabel" ><input type="checkbox" id="mydebug" class="checkboxctl" value="1" name="mydebug"/>Debug E.D.</label>');
		
		$('#' + elementid).append('<br><textarea id="commentbox" rows="7" cols="150"></textarea>');
		
		$('#mydebuglabel').hide();
		$('#commentbox').hide();
		// toggle the debug element on click
	   $('#mydebugtoggle').bind('click', function(e, data) 
	   									 { 
									   		$('#mydebuglabel').toggle();
									   		$('#commentbox').toggle();
									   		
									   		return false;    		 
	   									 }
	   							);
	   							
	// add an event listener to the check box
	   $('#mydebug').bind('change', function(e, data) 
	   								{ 
	   									__D_E_B_U_G__ = $('#mydebug').attr('checked') ? true : false;
								   		if (!__D_E_B_U_G__) 
								   		{
								   			$("#commentbox").val('')
								   		}
	   								}
	   						);
		}	
		
	/**
	 *Logs out by doing an ajax post the describer servlet telling it to logout the user	
 	 */
	function logout()
	{
			$.ajax({
			   type: 	"POST",
			   url: 	"describer",
			   data: 	"logout=true",
			   dataType:"text",
			   cache: 	false,
			   success: function(data, textStatus)
			   			{
					   		 location.href= __URL_ROOT__ + 'openid.html' + location.search;
					    },
			   error: 	function(XMLHttpRequest, textStatus, errorThrown) 
			   			{
			   	 			alert('failed to logout');
			   			}
	 		});
		
	}	