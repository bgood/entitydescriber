/*************************************************
 * edlayout.js Dec 2008
 * @author Paul Lu
 * Some basic functions that fill in the Header and Footer of a page that 
 * contain divs of a specific id.
 *
 * Requires: 
 * ed_properties_patched.js
 * jquery.js
 *************************************************/
 
 //document ready -> insert the banner image, navigator header and contacts footer
 // __URL_ROOT__ is from the ed_properties.js
 $(document).ready(function()
 					{
 						set_edbanner_image(__URL_ROOT__);
 						set_ednav(__URL_ROOT__);
 						set_edfooter(__URL_ROOT__);
 					}
 				   );
 				   
	 /**
	  * If there is a div with the id: ed-footer, appends the following to it
	  */				   
	 function set_edfooter(url_root)
	 {
	 	$('#ed-footer').append(
	 							'Brought to you by <a href="http://bioinfo.icapture.ubc.ca/bgood">Good,</a> ' + 
	 						   	'<a href="http://bioinfo.icapture.ubc.ca/ekawas">Kawas,</a> ' +
	 						   	'& <a href="http://www.blogger.com/profile/14066162084625579327">Lu</a> ' +
								'of the <a href="http://bioinfo.icapture.ubc.ca">Wilkinson Laboratory</a> ' +
								'<a href="mailto:goodb@interchange.ubc.ca"> <br/>Contact us</a>'
							  );	
	 }
	 
	 /**
	  * Add in the Navigator likns to ed-nav div
	  */
	 function set_ednav(url_root)
	 {
	 	$('#ed-nav').append(
	 							'<ul id="ed-main-menu"> ' +
	 							'<li><a href="' + url_root + 'index.html">Home</a></li> ' +
	 							'<li><a href="http://i9606.blogspot.com/search/label/ED">Blog</a></li> ' +
	 							'<li><a href="' + url_root + 'about.html">About E.D.</a></li> ' +
	 							'<li><a href="' + url_root + 'Help/index.html">Help</a></li> ' +
	 							'<li><a href="' + url_root + 'admin/ontologies.jsp">Admin</a></li> ' +
	 							'<li><a href="http://www.connotea.org/wiki/EntityDescriber">Wiki</a></li> ' +
	 							'<li><a href="' + url_root + 'Clients.html">Clients</a></li>' +
	 							'</ul>'
	 						);
	 }
	 
	 /**
	  *	Insert the ED logo bannger image 
	  */
	 function set_edbanner_image(url_root)
	 {
	 	$('#ed-banner-image').append(
	 							'<center>' +
	 							'<img src="' + url_root + 'ed/images/EDlogoBanner.jpg" alt="ED banner"/>' +
	 							'</center>'
	 							);
	 }				   