/*************************************************
 * EDopenid.js Nov 2008
 * @author Paul Lu
 * Functions used with the OpenID login
 *
 * Requires:
 * jquery.js
 *************************************************/
 
 	/**
 	 * Bind the openid submit button to validate and submit the openid for
 	 * processing
 	 */
	function load_openid_settings()
	{
		$('#openid_url').focus();
		$('#openidform').bind('submit', function(e, data) 
										{
										
											var openid = $('#openid_url').val();
												openid = $.trim(openid);
												
								   	   	  if(openid != '')
								   	   	  { 
							 		   		return true;
								   	   	  }else{	
									   	   	  	alert('Please enter your OpenID');
									   	   	  		$('#openid_url').focus();
									   	   	  	return false;
					 				   	   	   }
								     }
								);
									
		$('#paramString').val(location.search);
								
	}
	
	/**
	 * Check if the user is already logged in
	 */
	function checklogin()
	{
		var cookieprefix = $('#cookiePrefix').val();
		var nextPage = $('#nextPage').val();
		var username = readCookie(cookieprefix +'_USERNAME');
		if(readCookie(cookieprefix + '_USERNAME') != null && readCookie('EDES_HASH') != null)
			{
				alert('You are already logged in');
				location.href = __ROOT_URL__ + nextPage; 
				return false;
			}else{
				return true;
			}
	}
	
	/**
	 * Clear the value of the element
	 */
	function cleartext(element)
	{
		$(element).val('');
	}
