/****************************************************
 * ed_connotea.js Nov 2008
 * @authors Eddie Kawas, Paul Lu
 * 
 * Used in conjunction with Connotea.  Inserts the
 * Entity Describer components into the bookmark
 * page.  ie.  Freebase Suggest, Restrict Types, etc.
 *
 ****************************************************/

/* Global variables */ 
var __URL__ = "./";
var ontology_url = "../";
var __tagging_xml__ = "";
// a hash of freebase tags to internal guid 
var __guid__ = new Array();
// global variable that triggers debug actions 
var __D_E_B_U_G__ = false;

//Begin inserting components 
$(document).ready(function()
					{
						$('#body').css('font-size', '50%');
						
						// insert the type chooser so users can restrict the type of tag they're looking for
							create_type_chooser();
						
						//add on existing row to indicate "Semantic Tags will appear here"
						  	var tr_default = '<tr><td align="center"><span  id="ed_tag_default" name="ed_tag_default"></span></td></tr>'
						 	tr_default = $(tr_default);
						 	tr_default.find("span[@name=ed_tag_default]").text('Semantic Tags will appear here');
							$("#suggest_table").append(tr_default);
						
						//remove connotea footer	
						  	$('#footer_subscribe').empty()
						// remove googles urchin program
						  	$('body > script').remove();
						  	
						
						// get the users tags as a json object for Connotea section where a site has already 
						// been tagged once by a user
							if (typeof __E__D__U__S__E__R__ != "undefined") 
							{
								$.ajax({
										   type: 	"POST",
										   url: 	"JsonUserTags",
										   data: 	"username="+__E__D__U__S__E__R__,
										   dataType:"text",
										   cache: 	false,
										   success: function(data, textStatus)
										   			{
													   	try 
													   	{
													   	 	var e = eval("(" + data + ")");
													   	 	tags = e;
													   	 } catch (Exception) 
													   	 	{
													   	 		tags = new Object();
													   	 	}
												   	 
												   },
										   error: 	function(XMLHttpRequest, textStatus, errorThrown) 
										   			{
										   	 			tags = new Object();
										   			}
								 		});
							  } else{ //end of checking if it's already tagged by Connotea
								 		tags = new Object();
								 	}	
						
						// topic text box click action ie.  the Filter by Type link
							 $('#tag_topic_link').click(function(e)
							 							{
													 		$('#topic_choose_div').toggle('slow');
													 		return false;
													 	})
													 	.css("text-decoration","none");
							
						 //add an action for when the form is submited
						 //Call SPARQLTagging when submit button is pressed						    
						   	$('#form_add').bind('submit', function(e, data) 
														   	{ 
														   		if(__tagging_xml__ == "" || __tagging_xml__ == null)
															   	{	
															   		//create tagging xml
															   		__tagging_xml__ = save_tags_action(__guid__); 
	
															   		if(__tagging_xml__ != null || __tagging_xml__ != '')
															   			//send Tags for saving
															   			
																   			SPARQLTagging(); 
																   		
														   		}
														   		return false; 
														   	}
											   );
						     
						//add a click action to the submit button 
						//Call SPARQLTagging when the submit button is pressed
						   $('input[@type=submit][@name=add]').bind('click', function(e,data) 
							   									 				{
							   									 					if(__tagging_xml__ == "" || __tagging_xml__ == null)
															   						{	
															   							__tagging_xml__ = save_tags_action(__guid__);
															   						
															   							if(__tagging_xml__ != null || __tagging_xml__ != '')
																				
																					   			SPARQLTagging();
																				   	}
																				   		return false;
																				 }
																	);
						
						// append a debug element
						   var debug_element = '<tr><td id="mydebuglabelcell" class="addformlabelcell"><a style="text-decoration: none;" href="#" id="mydebugtoggle">Debug:<a/></td><td id="mydebugdatacell" class="addformdatacell"><label id="mydebuglabel" ><input type="checkbox" id="mydebug" class="checkboxctl" value="1" name="mydebug"/>Debug E.D.</label></td></tr>';
						   $('#tagslabelcell').parent().parent().append(debug_element);
						   $('#mydebuglabel').hide();
						
						// toggle the debug element on click
						   $('#mydebugtoggle').bind('click', function(e, data) 
						   									 { 
														   		$('#mydebuglabel').toggle();
														   		return false;    		 
						   									 }
						   							);
						   							
						// add an event listener to the check box
						   $('#mydebug').bind('change', function(e, data) 
						   								{ 
						   									__D_E_B_U_G__ = $('#mydebug').attr('checked') ? true : false;
													   		if (!__D_E_B_U_G__) 
													   		{
													   			$("#commentbox").val('')
													   		}
						   								}
						   						);
						   						
						// replace loggedinas with the current users name
							$('.loggedinas').html("<p>Logged into E.D. as: <em>" + (typeof __E__D__U__S__E__R__ != "undefined" ?unescape(__E__D__U__S__E__R__) : '') + "</em></p>");
						
						// check for users login ...	
								$('.login-buttons').append('<p class="divider"><a id="check_login" class="mylibrary" href="javascript:load_loggin_window()">Am I logged in to Connotea?</a></p>');
								
								$("body").append( '<div id="__modular__popup__" style="align:center;"/>' );
						   		$('#__modular__popup__').hide();
						   		$('#__modular__popup__').append('<h1>See your library?</h1><p style="align:center;">Please log into Connotea if your library doesn\'t show up. Simply fill out the form below and once you are done hit the \'Login\' button.<br/>This will log you into Connotea and enable you to use E.D. while bookmarking your resources.</p><iframe style="align=center;border: 1px solid rgb(0, 0, 0);" src="http://www.connotea.org/library" width="95%" height="60%"/><p><br/><input type="button" class="buttonctl" value="I am ready for E.D." onclick="javascript:$(\'#__modular__popup__\').hide(\'slow\');"></input></p>')
								 						.css("position","fixed").css("overflow","auto")
														.css("display","none").css("align","center")
														.css("opacity","100").css("zindex","1000")
														.css("width","80%").css("height","70%")
								 						.css("left","10%").css("top","10%")
								 						.css("border","3px solid black").css("backgroundColor","#FFDC75").hide()
						
								// bind an action to the share with members of the group ...
								$("input[@type=radio][@name=private][@value=2]").bind('click', function(e,data) 
																								{
																									var really = confirm('In order to use this function, you cannot use E.D. and I have to redirect you to Connotea.\nShall I redirect you?');
																									if (really) 
																									{
																										location.href = "http://www.connotea.org/add" + location.search
																									} else {
																												return false;
																											}
																								}
																					);   						
						   		if ($('a[@href=http://www.connotea.org/][@target=_blank]').length > 0 )
										$('a[@href=http://www.connotea.org/][@target=_blank]').attr('href',__URL_ROOT__);
						   		
						//bind the freebase suggest to the tag textbox			
								bind_suggest();		
						
						//Add in tag helpbox for help to appear when moused over		
								if ($('#tagslabelcell').length > 0) 
								{
									$('#tagslabelcell').text("Personal Tags: ");
									if($('#taghelpbox').length > 0) 
									{
										$('#tagslabelcell').css('cursor','pointer')
															.hover(
																	function (e) { $('#taghelpbox').show('slow');},
																	function (e) { $('#taghelpbox').hide('slow');}
																	);
										$('#taghelpbox').hide();
									}
								}
								
						// put up an annoying confirm box for those that click 'keep private'		
								if ($("input[@type=radio][@name=private][@value=1]").length > 0) 
								{
									$("input[@type=radio][@name=private][@value=1]").click(
																								function(e) 
																								{
																									var msg = 'Just so you know, private bookmarks are not currently stored in the Entity Describer\'s semantic database. They will be posted to Connotea as usual, but you will not be able to access them through the emerging tools provided by the Entity Describer team.\n\nContinue?'
																									var i = confirm(msg);
																									return i;
																								}
																							);
								}
								
						//create progressBar
							createProgressBar();
								
						//focus on tag textbox
							$("#ed_tagsbox").focus();							
					}
				);

	//Function to create the 'Filter By Type ?' section for ED 
	function create_type_chooser() 
	{
		var emptystr = get_default_type();
	
		$('#tagslabelcell').parent()
							.before(  
								 	'<tr><td id="type_chooser_td" class="addformlabelcell"><span><a title="Change the focus of the suggest box" id="tag_topic_link" href="#">Filter by Type:</a></span> <a target="_blank" style="text-decoration: none;" title="What is this \'Type\' stuff all about?" id="tag_topic_help" href="Help/FreebaseTypes.html">?</a></td><td id="type_chooser_td2" class="addformdatacell"><div id="topic_choose_div"><input type="text" id="typeSearchBox" class="textctl" autocomplete="off" size="50" value="" name="topic"/></div></td></tr>' + 
								 	'<tr><td id="ed_tags_table_td" class="addformlabelcell">Selected tags <span class="optional"></span></td><td id="ed_tag_selected_topics_table" class="addformdatacell"><table id="suggest_table" width="380px" border="0" cellpadding="0" bgcolor="#CCCCCC"><tr><td><center><b>Selected Semantic Tags</b> (click to remove)</center></td></tr></table></td></tr>' +
								 	'<tr><td id="ed_type_inputbox_td" class="addformlabelcell">Semantic Tags:</td><td id="ed_type_inputbox_td2" class="addformdatacell"><div id="ed_type_inputbox_div"><input type="text" id="ed_tagsbox" class="textctl" autocomplete="off" size="50" value="" name="ed_tagsbox_topic"/></div></td></tr>'  		
								 	);
	 	
	 	$('#topic_choose_div').hide();
	 	
	 	$('#taghelpbox').after(
						 		'<div id="semantictaghelpbox" class="ctlhelp">' +
						          'Semantic Tags are shared across the community<br/>'+
						          'and provide an additional level of organization<br/>'+
						          'for your bookmark collection.<br/>'+
						          'To add a semantic tag, start typing and then <br/>'+
						          'select a term from the drop-down list that appears.<br/>' +  
						          'You may add as many semantic tags as you like. <br/></div>'
						 	);
		$('#semantictaghelpbox').width('380px').hide();
						 	
	 	$('#ed_type_inputbox_td').css('cursor','pointer')
						 		 .hover(
										function (e) { $('#semantictaghelpbox').show('slow');},
										function (e) { $('#semantictaghelpbox').hide('slow');}
									   );
				
	 	$('#tagslabelcell').css('cursor','pointer')
					 		.hover(
					 				function (e) {$('#taghelpbox').show('slow');},
									function (e) {$('#taghelpbox').hide('slow');}
								  );
		
		// add a select element for choosing topics previously used
	 	var select = '<br/><br/><label>Recently used types: <select id="topic_select" class="textctl" name="topic_select"><option value="'+emptystr+'" selected="selected">All</option></select></label>';
	 	$('#typeSearchBox').parent().append($(select));
	
	 	// fill in the select element
	 	select = readCookie('ed_user_topics');
	 	if (select != null)
	 	{
	 		select = select.split(" ");
	 		$("#topic_select").empty();
	 		select.push(emptystr);
	 		select.sort();
	 		for (var i in select) 
	 		{
	 			$("#topic_select").append($('<option value="'+select[i]+'">' + select[i] + '</option><br/>'));
	 		}
	 		$("option[@value="+emptystr+"]").attr('selected','selected');
	 	}			
	 	
	 	$("#topic_select").change(function(e) 
								 	{
								 		var selected = this.value;
								 		var really = true; //confirm("Really switch to the topic: '" + selected + "'?");
										if (really) {
											bind_suggest(selected)
											current_topic = selected;
											$("#typeSearchBox").focus().blur();
											$("#tagsbox").focus();
								
										}
								 	}
								 );
	//bind the freebase suggest (topics set to type) to typeSearchbox 							 
		$("#typeSearchBox").freebaseSuggest(get_freebase_suggest_types_params());
		$("#typeSearchBox").bind("suggest", function(e, data) 
											{
												var really = confirm("Really switch to the topic: '" + data.id + "'?");
												if (really) 
												{
													bind_suggest(data.id)
													current_topic = data.id;
													
													if ($('option[@value='+current_topic+']').length > 0) 
													{// already exists, just select it
														$('option[@value='+current_topic+']').attr('selected','selected')
													} else {
																// add to cookie and select box
																var select = readCookie('ed_user_topics');
																if (select == null) select = current_topic;
																else select += " " + current_topic;
																select = $.trim(select);
																createCookie('ed_user_topics', select, 21);
																$("#topic_select").append($('<option value="' + current_topic +'" selected="selected">' + current_topic + '</option>'));
															}
															
													$(this).blur();
													$("#ed_tagsbox").focus();
												}
											}
								);
								
		$("#typeSearchBox").focus(function(e){ $(this).val("");} )
	 					   .blur(function(e) 
	 					   			{
										 if (current_topic)
										 	$(this).val(current_topic);
										 else
										 	$(this).val(emptystr);
								 	}
								 )
							.val(emptystr);						 
	}

	//Shows the login window for Connotea
	function load_loggin_window() 
	{
		$('#__modular__popup__').show('slow');
	}

	// replace connotea autocomplete with my own
	function _autocompletetags() 
	{
	  var tagsBox = document.getElementById('tagsbox');
	  var tagstring = tagsBox.value;
	  lastCaretPos = getCaretPosition(tagsBox);
	  clearsuggestions();
	  
	  if (tagstring.length == 0) 
		    return;
	}

	 /* 
	  *		freebase suggest binding -
	  *		-Sets up the ed_tagsbox to display freebase tag suggestions during typeahead
	  *		-Sets up the actions of when a tag suggestion is selected.  Such as which array to save the guid information to
	  *			and where to display it
	  */
	function bind_suggest(topic) 
	{
		var t = topic || get_default_type();
	   	var edbox = 'ed_tagsbox';
	    var box_settings = '{\'#' + edbox + '\' : { type:  \'' + t + '\'}}';
	
	  	var settings = eval( "(" + box_settings + ")");  
	    for (var id in settings) 
	    {
			var ac_param = get_ac_param();
			
			$.extend( ac_param, settings[id] );
			var freebaseSuggestParams = get_freebase_suggest_params();
				freebaseSuggestParams.ac_param = ac_param;
			
			$(id).freebaseSuggest(freebaseSuggestParams)
				 .unbind("suggest")
				 .bind("suggest", function(e, data) 
				 					{
										if (e.target.id == 'ed_tagsbox') 
										{
											if (typeof(addtag) != 'undefined') 
											{
												//removes default tag if one is being added
												var ed_default_tag = $("#ed_tag_default");
												if( ed_default_tag != null)
													$(ed_default_tag).parent().parent().remove();
											
						                    	//replace commas with periods.
						                    	var txt = data.name
						                    		txt = txt.replace(/,/g,".");
						                    	
						                    	// add to table new Semantic Tag row
						                    	var tr = $('<tr bgColor="#80c0ff"><td align="center"><span style="cursor:pointer;" title="Click me to remove!" name="ed_tag_text"></span></td></tr>');
						                    	
						                    	//put in text of the Semantic Tag and give it the 'click to remove function
						                    		tr.find("span[@name=ed_tag_text]").text(txt);
						                    		tr.find("span[@name=ed_tag_text]")
						                    			.click(function()
						                    					{
						                    						var tBody = $(this).parent().parent().parent(); //get tbody
						                    						$(this).parent().parent().remove();
						                    			
									                    			//check to see if there is only one row (header) - 
									                    			if(tBody[0].rows.length == 1)
									                    			{
										                    			//and put back in default tag
								  										var tr_default = '<tr><td align="center"><span  id="ed_tag_default" name="ed_tag_default"></span></td></tr>'
								 										tr_default = $(tr_default);
								 										tr_default.find("span[@name=ed_tag_default]").text('Semantic Tags will appear here');
																		$("#suggest_table").append(tr_default);
									                    			}
						                    					}
						                    				 );
							                   //Append new row to table 		
							                    	$("#suggest_table").append(tr);
						                       //set timeout to revert row color
						                    		window.setTimeout('revert_color()', 750);
						                       //clear ed tagsbox	
						                    		$('#ed_tagsbox').val('');
						                       //add in new tag information to __guid__ array	
							                    	if (data.guid && data.guid.charAt(0) == '#') 
							                    	{
							                    		__guid__[escape(txt)] = {'guid':data.guid.substr(1), 'types': data.type, 'alias':data.alias};
													} else {
							                    				__guid__[escape(txt)] = {'guid':data.guid, 'types': data.type,  'alias':data.alias};
							                    			}
						                    } else {
								                    	txt = txt.replace(/,/g,".");
								                    	$(data.input).val(txt);
								                    	this.c.caret_last(data.input);
						                    		}
					                    }
									}
							).bind("suggest-submit", function(e, data) {});	
	    }   
	}


	/*
	 *	 freebase suggest unbinding
	 */
	function unbind_suggest() 
	{
	    var settings = {'#ed_tagsbox' : {}};
	    for (var id in settings) 
	    {
			$(id).freebaseSuggest().unbind()	
	    }
	}

 	/*
  	 *	action run when the user saves their tags
  	 *	creates the xml representation of this tagging event
  	 */ 
	function save_tags_action(guidArray) 
	{
		var isPrivate = $("input[@type=radio][@name=private][@checked]").val() == 1;
		//IF PRIVATE SETTINGS ARE SET FOR CONNOTEA - do the following	
		if (isPrivate) {
			if (!__D_E_B_U_G__) {
				$('span[@name=ed_tag_text]').each(function() {
					var word = $(this).text();
					
					if (typeof(addtag) != 'undefined') {
						var txt = word;
						txt = txt.replace(/,/g,".");
						addtag(txt, true)
					}
				});
			}
			return null;
		}
	
		//START MAKING SEMANTIC TAGS XML	
			var tags = new Array();
			var box = $('#tagsbox').val();
			var agent = readCookie('ED_USERNAME');
			var connotea_agent = 'http://www.connotea.org/user/' + agent;
			var resource = null;
			
				resource = $("input[@name=uri]").not($("input[@type=hidden][@name=uri][@value^=http://www.connotea.org/edit]")).val();
			
			//check if uri is valid
				 var regexp = /^[A-Za-z]+:(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/; 
		   		 if(!regexp.test(resource)) 
		   		 	{
		   		 		alert('Please enter a valid URI'); 
		   		 		return null;
		   		 	}
		   		 		
			var start = 0
			var end = 0;
			resource.match(/.*&uri=([a-zA-Z0-9%\.]*){1}&?.*/);
			if (RegExp.$1)
				resource = unescape(RegExp.$1);
			
			var inQuotes = false;
			var word = "";
		
			for (var i = 0; i < box.length; i++) {
				if ((box.charAt(i) == " " || box.charAt(i) == ",") && !inQuotes ) {
					if ($.trim(word) != '')
						tags.push($.trim(word));
					word = "";
					continue;
				}
				if ((box.charAt(i) == '"') && !inQuotes ) {
					inQuotes = true;
					word// += '"';
				} else if ((box.charAt(i) == '"') && inQuotes){
					if (word.replace(/^\s+|\s+$/g, '') != '')
						tags.push($.trim(word));
					word = "";
					inQuotes = false;
				} else {
					word += box.charAt(i);
				}
			}
			word = $.trim(word);
			if (word != "" && word != "\"") {
				tags.push(word);
			}
		
		// add the ED tags to tags for processing
		$('span[@name=ed_tag_text]').each(function() {
			var word = $(this).text();
			
			var push_word = word;
			if (word.indexOf(' ') > 0) {
				push_word = "\"" + word + "\"";
			}
			tags.push($.trim(push_word))
			
			//if there's an apostrophe, backslash, plus sign - take it out - Filter!
						word = FilterOutReservedChars(word);
						$(this).text(word);	
		});
		
		var xml = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>';
		xml +='<tagging version="2.0"><agent>'+ agent + '</agent><connotea_agent>'+connotea_agent+'</connotea_agent><date>'+new Date().toString()+'</date><resource>'+escape(resource).replace(/&quot;/g,"\"").replace(/&apos;/g,"\'").replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&amp;/g,"&")+'</resource><tag-list>'
				
		for (var i in tags) {
			var text = 'http://www.connotea.org/'+agent+'/tag/' 
				+ escape(tags[i].substring(tags[i].charAt(0) == '"' || tags[i].charAt(0) == "'" ? 1 : 0, tags[i].charAt(tags[i].length-1) == '"' || tags[i].charAt(tags[i].length-1) == "'" ? tags[i].length-1 : tags[i].length ));
			var name = tags[i].substring(tags[i].charAt(0) == '"' || tags[i].charAt(0) == "'" ? 1 : 0,tags[i].charAt(tags[i].length-1) == '"' || tags[i].charAt(tags[i].length-1) == "'" ? tags[i].length-1 : tags[i].length )
			if (guidArray[escape(name)] ) {
				if( guidArray[escape(name)].guid) {
			
					if ( guidArray[escape(name)].types ) {
						var guid = guidArray[escape(name)].guid;
						xml += '<tag ontology="http://www.freebase.com" name="'+name+'" uri="http://www.freebase.com/view/guid/' + guid + '">';
						
						//Freebase-Types
						xml += '<Freebase-Type-List>'
					    for (var j in guidArray[escape(name)].types) {
					    	xml += '<FType id="'+ guidArray[escape(name)].types[j].id + '" name="' + guidArray[escape(name)].types[j].name + '"/>'
					    }
					    xml += '</Freebase-Type-List>'
					    
					    //Add in Alias to xml
						    if (guidArray[escape(name)].alias ){
							xml +=  '<Alias-List>'
							for (var y in guidArray[escape(name)].alias) {
						    	xml += '<Alias name="' + guidArray[escape(name)].alias[y] + '"/>'
							}
							xml += '</Alias-List>'
						    }
					    
						xml += '</tag>'
					}
				}
			} else {
				xml += '<tag ontology="http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/" name="'+name+'" uri="http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/Nothing">'+ text + '</tag>';
			}
		}
		xml += '</tag-list></tagging>'
		if (__D_E_B_U_G__) {
			alert("Nothing submitted because you are in debug mode ...");
			$("#commentbox").val(xml)
		} else {
			// TODO call addTag on all ED tags
			$('span[@name=ed_tag_text]').each(function() {
				var word = $(this).text();
				if (typeof(addtag) != 'undefined') {
					var txt = word;
					txt = txt.replace(/,/g,".");
					addtag(txt, true)
				}
			});
		}
		return xml;
	}


	//Called by Window.timeout function to return row back to white
	function revert_color()
	{
		//Get the last row and take away color	
		var val = $('#suggest_table').find('tbody tr:last').css("background", '#CCCCCC');
	}

	/*
	 * Function to take out Reserved Characters that Connotea does not like.
	 */
	function FilterOutReservedChars(word)
	{
				//filter out backslashes, plus signs and apostrophes
				var reappended_word = '';
				//plus sign and back slash - join together with space
				reappended_word = word.split(/[+]/).join(" ");
				//apostrophe - join with nothing
				reappended_word = reappended_word.split(/[']/).join("");
					return reappended_word;
	}

	
/**********************************************************************************************************************
 *	SUBMIT TAGGING  FUNCTIONS
 *
 *********************************************************************************************************************/	

	/**Calls the SPARQLTaggingServlet here with xml**/
	function SPARQLTagging(){
		var xml = __tagging_xml__; 
		
		if (!__D_E_B_U_G__ && xml != null) 
		{
		//submits the XML to SPARQLTaggingServlet witin the _iframeSPARQL_
			submitTagging('SPARQLTaggingServlet', xml, SPARQL_EP_params); 
				$('#type_chooser_td').empty();
		}
		return false;
	}

	/**
	 *	createTaggingForm is used as opposed to passing the tagging_xml as a url parameter because IE 
	 *	restricts url parameter to a size of 2083 chars (Less than what is required to pass tagging_xml as a parameter.  
	 *	Hidden Input Parameters however, have no limit (that I've encountered)
	 */
	function submitTagging(tagging_uri, tagging_xml, SEP_params)
	{
		startSubmitTaggingStatus();
		//Ajax options for ajax Form submission for submitting Tagging to SPARQL Endpoint
			var EDTaggingOptions = 
			{
				type: 		"POST",
				url:		__URL__ + tagging_uri,
				dataType:	"jsonp",
				cache:		false,
				success: 	handleTaggingResponse,
				error:		handleError
			}
			
			var formId = 'form4' + tagging_uri;
		
			//make form on the fly - 
			var tagging_form = $('<form></form>');	
				$(tagging_form).attr('id', formId); //id
				$(tagging_form).attr('name', formId); //name
			
			//make hidden input on the fly - for tagging XML 
			var hid_tag_xml = $('<input type="hidden"></input>');
				$(hid_tag_xml).attr('name', 'tagging'); //give it a name
				$(hid_tag_xml).attr('value', tagging_xml); //set value to the tagging xml
				$(hid_tag_xml).appendTo(tagging_form);
			
			//Send the parameters for the SPARQL Endpoint here if SEP_params exists
			if(typeof(SEP_params) != 'undefined'){
				/**SPARQL ENDPOINT URL**/
				var SEPurl = SEP_params.SPARQL_EP_url;
					var hid_paramSEPurl = $('<input type="hidden"></input>');
						$(hid_paramSEPurl).attr('name', 'SPARQL_EP'); //give it a name
						$(hid_paramSEPurl).attr('value', SEPurl);
						$(hid_paramSEPurl).appendTo(tagging_form);
				/**SPARQL ENDPOINT PARAMETER NAME OF query**/		
				var SEPqueryparam = SEP_params.SPARQL_EP_query_param_name;
					var hid_paramSEPquery = $('<input type="hidden"></input>');
						$(hid_paramSEPquery).attr('name', 'query_param_name'); //give it a name
						$(hid_paramSEPquery).attr('value', SEPqueryparam);
						$(hid_paramSEPquery).appendTo(tagging_form);
				/**SPARQL ENDPOINT MISC parameters**/		
				for (var i in SEP_params.SPARQL_EP_misc_params) {
					//alert('key is: ' + i + ', value is: ' + SEP_params[i]);
					var hid_param = $('<input type="hidden"></input>');
						$(hid_param).attr('name', i); //give it a name
						$(hid_param).attr('value', SEP_params.SPARQL_EP_misc_params[i]); //set value to the tagging xml
						$(hid_param).appendTo(tagging_form);
				}
			}
			//Put hidden input: 'hid_tag_xml aka: 'tagging'' into the tagging_form
			//Put tagging_form in body
			$(tagging_form).appendTo('body');
			$(tagging_form).ajaxForm(EDTaggingOptions);
			
			//submit form
			$(tagging_form).submit();
			$(tagging_form).remove(); //clean up
	}

	/**
	 *		Calling submitTagging may result here.	 
	 *		Handles the response from the ajax form submit to the Tagging Servlet
	 *		Response 200 indicates the tagging was submitted successfully
	 */
	function handleTaggingResponse(data)
	{	
		if(data.response.code == 200)
		{
			setProgressBarValue(100);
			//do it this way (non-jquery way) - will ignore jquery submit bind and not loop
			//TODO: replace later with an unbind followed by submit
			var form = document.getElementById('form_add'); 
						form.submit();	
		}else{
				var message = 'from handleTaggingResponse';
				handleError(data, message);
			 }
	}

	/**
	 *		Calling submitTagging may result here.	 
	 *		Handles the response from the ajax form submit to the Tagging Servlet if it's an error
	 *		Only ever really an error when the above - handleTagginResponse - does a submit before 
	 *		Ajax can finish on the second saveTagging
	 */
	function handleError(data, message)
	{
		var form = document.getElementById('form_add'); //do it this way (non-jquery way) - will ignore jquery submit bind and not loop
					form.submit();	
	}	

