<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
//String username = request.getAttribute("EDES_USERNAME").toString();
//String userid = request.getAttribute("EDES_USERHASH").toString();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'complete.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<script type="text/javascript" src="./jquery/jquery.js"></script>
	<script type="text/javascript" src="./jquery/jquery.form.js"></script>
	<script type="text/javascript" src="./jquery/jquery.freebase.suggest.js"></script>

	<script type="text/javascript" src="./js/EDcookie.js"></script>	
	<script type="text/javascript" src="./js/edlayout.js"></script> 
	<script type="text/javascript" src="./js/ed_properties_patched.js"></script>
			
	<script type="text/javascript">
		var _E_D_U_S_E_R_N_A_M_E_ = readCookie('EDES_USERNAME'); 
		
		$(document).ready(function()
		{	
			$('div.loggedinas').append('<p>Logged in as: <em>' + _E_D_U_S_E_R_N_A_M_E_ + '</em></p>');
			$('div.login-buttons').append('<p><a class="log" href="javascript:logout();">Log out of E.D.</a></p>');
			
			
									
		}	
		);
		
		function logout()
		{
			$.ajax({
			   type: 	"POST",
			   url: 	"describer",
			   data: 	"logout=true",
			   dataType:"text",
			   cache: 	false,
			   success: function(data, textStatus)
			   			{
					   		 window.close();
					    },
			   error: 	function(XMLHttpRequest, textStatus, errorThrown) 
			   			{
			   	 			alert('failed to logout');
			   			}
	 		});
			
		}
			
								
	</script>
	<link href="./css/global.css" rel="stylesheet" type="text/css" />
	<link href="./jquery/css/freebase-controls.css" rel="stylesheet" type="text/css" />

  </head>
  
  <body>
  		<div id="banner-wrap">
			<div id="banner">
				<div style="float: right; text-align: right;">
			  	<div class="loggedinas"></div>	
			  		<div class="login-buttons"></div>	
		 		</div>
			</div>
  		</div>
  		
  		
		<br>
		<br>
			
		  <button id="logoutbutton" class="buttonctl" value="Logout" onclick="javascript:window.close();" >Close</button>
	    <br>
	    <br>
	    <div id="ed-footer-wrap">
	    	<div id="ed-footer"></div>
	    </div>
  </body>
</html>
