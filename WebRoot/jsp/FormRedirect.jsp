<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String OPEndpoint = request.getAttribute("OPEndpoint").toString();
		//contains all the parameters to be hidden for OP
		
		Map params = (Map)request.getAttribute("parameterMap");
		
		//Get the key set of the parameters and make an iterator for it
		Set param_keys = params.keySet();
		Iterator i = param_keys.iterator();	
		
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<script type="text/javascript" src="./jquery/jquery.js"></script>
    <script>
    	//hide the continue button
		function hideButton(){
    		$('#continue_button').hide();
		}
    </script>
    <title>Form Redirection</title>

  </head>
  
  
  <body onload="hideButton(); document.forms['openid-form-redirection'].submit();">
  <img alt="" src="ed/images/mozilla_blu.gif"> &nbsp;&nbsp; Redirecting...</img>
	<form name="openid-form-redirection" action="<%=OPEndpoint%>" method="GET" accept-charset="utf-8">
	    
	<%
	//iterate through the parameters
	
		while(i.hasNext())
		{
			//Get the key and value of the current parameter
			Object key = i.next();
			Object val = params.get(key);
				out.println("<br /><br />");
				//out.println(key.toString() + "<br />" + val.toString() + "<br />");			
				out.println("<input type=\"hidden\" name=" + key.toString() + " value=" + val.toString() + " />");
			
		}
	 %>
	 
	<button type="submit" id="continue_button">Continue...</button>
	</form>
  </body>
</html>
