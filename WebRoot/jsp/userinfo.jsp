<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>

<%@ page import="org.icapture.ED.EDAuthenticator" %>
<%@ page import="org.icapture.web.HttpUtil" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//get User Name
String ED_HASH = HttpUtil.getCookieValue(request.getCookies(), "EDES_HASH", null);
		String userid = ED_HASH.split(",")[0];
		String username = EDAuthenticator.doesUserExist(userid);
		username = username != null ? username : "";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>User Information</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="Userinfo">
	<script type="text/javascript" src="./jquery/jquery.js"></script>
	<script type="text/javascript" src="./jquery/jquery.form.js"></script>
	
	<script type="text/javascript" src="./js/ed_properties_patched.js"></script>
	<script type="text/javascript" src="./js/edlayout.js"></script>
	<script type="text/javascript" src="./js/EDcookie.js"></script>
	<script type="text/javascript" src="./js/sparqlquery.js"></script>
	<script type="text/javascript">
    /**************************************************************
     * Dec 2008
     * @author Paul Lu
     * 
     ***************************************************************/
    	var QUERY_SPARQL_ENDPOINT = './SPARQLQueryServlet';
    	var RESULT_RETURN_FORMAT = 'application/sparql-results+json';
    	var DEFAULT_GRAPH_URI = 'EDtagging';
    
    	var timeout = null;
		var lastKeyPressCode = null;
	   	var NAME_EXISTS = true;
	   	
	   	//Called onload - creates the queryform to see if a user nickname already exists
	   	function Setup()
	   	{	
	   	 
	   		make_and_append_queryForm(QUERY_SPARQL_ENDPOINT, RESULT_RETURN_FORMAT, DEFAULT_GRAPH_URI);	
	   		$('#userinfoNickname').keydown(function(e) 
	   										{	
	   											lastKeyPressCode = e.keyCode;
	   											timeout = setTimeout(function(){checkUserName();}, 400);
	   											
	   										}
	   									   );		
	   									   
	   		$('#userinfoform').submit(
									function()
									{
										return !NAME_EXISTS;
									}
								);
								
			$('#userinfo_submit').bind('click', function(e,data) 
											{
												
												return !NAME_EXISTS;
											}
								);								   		
	   	}

		//Sends a query out to see if the nickname exists	
		function checkUserName()
		{
			// ignore if the following keys are pressed: [del] [shift] [capslock]
			if( lastKeyPressCode == 46 || (lastKeyPressCode > 8 && lastKeyPressCode < 32) || 
			    lastKeyPressCode == 38 || lastKeyPressCode == 40 || lastKeyPressCode == 9 || lastKeyPressCode == 13)
			{
				return;
			}
			
			var nickname = $('#userinfoNickname').val();
			nickname = $.trim(nickname);
			if(nickname == '')
			{
				$('#name_available').text('');
				return;
			}
			
			var nicknamequery = makeNickNameQuery(nickname);
			sendQuery(nicknamequery);
		}
		
		//Handles the response back from the ajaxform submit indicating if the nickname is available or not
		function handleQueryResponse(data)
	    {
	    	var len = data.results.bindings.length;
	    	if(len == 0)
	    	{	NAME_EXISTS = false;
				$('#name_available').text('NAME IS AVAILABLE');
				$('#name_available').css('color', 'green');
	    	}else{
		    	NAME_EXISTS = true;
	    		$('#name_available').text('NAME IS NOT AVAILABLE');
	    		$('#name_available').css('color', 'red');
	    		
	    	}
	    	
	   	}
	    	
	    //Handles if there was an error during the ajaxform submit	
	   	function handleQueryError(data)
	   	{
	   		$('#name_available').text('ERROR CHECKING NAME');
	   		$('#name_available').css('color', 'red');
	   		
	   	}
	   	
	   	//Make the checkName query
	   	function makeNickNameQuery(name)
	   	{
	   		var nicknameQuery = '';
	   		nicknameQuery += 'prefix tag: <http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/> \n';
			nicknameQuery += 'SELECT * WHERE \n'; 
			nicknameQuery += '{ \n';
			nicknameQuery += '?s tag:name ?name \n';
			nicknameQuery += 'FILTER regex(str(?name), "^' + name + '$") \n';
			nicknameQuery += '} ';
			
			return nicknameQuery;
	   	}


	</script>	
	<link href="./css/global.css" rel="stylesheet" type="text/css" />
	
  </head>
  
  <body onload="Setup();">


<div id="ed-main-blue" class="ed-main-blue">

<div id="edlightbox" class="edlightbox"><center>
	Before we get started, please tell us a little bit about yourself.
	
	<form name="userinfoform" id="userinfoform" method="post" action="userinfo">
<br><br>
		<input type="hidden" name="userinfoName" id="userinfoName"></input>
		<!-- javascript: Make sure something was checked -->
	<br>
		Nickname: <input type="textbox" value="<%=username%>" name="userinfoNickname" id="userinfoNickname" size="40"></input>
	<br><br>
		<span id="name_available" name="name_available"></span>
	<br><br>	
		<input type="submit" value="SUBMIT" name="userinfo_submit" id="userinfo_submit"></input>	
	</form>	
	</center>
</div>

<hr/>
<div id="ed-footer-wrap"><div id="ed-footer">
</div></div>

</div>		

  </body>
</html>
