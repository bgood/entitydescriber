<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="org.icapture.ED.EDAuthenticator" %>
<%@ page import="org.icapture.web.HttpUtil" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
//String URI_2b_described = request.getParameter("uri"); 
//String URI_title = request.getParameter("title");	
String username = request.getAttribute("EDES_USERNAME").toString();
String userid = request.getAttribute("EDES_USERHASH").toString();
String uri = request.getParameter("uri") != null ? request.getParameter("uri") : "";
String title = request.getParameter("title") != null ? request.getParameter("title") : "";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>Entity Describer</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<script type="text/javascript" src="./jquery/jquery.js"></script>
	<script type="text/javascript" src="./jquery/jquery.form.js"></script>
	<script type="text/javascript" src="./jquery/jquery.freebase.suggest.js"></script>
	<script type="text/javascript">
			var _E_D_U_S_E_R_N_A_M_E_="<%=username%>"
			var _E_D_U_S_E_R_I_D_="<%=userid%>"
			var _U_R_I_="<%=uri%>"
			var _T_I_T_L_E_="<%=title%>"
	</script>
	<script type="text/javascript" src="./js/EDcookie.js"></script>	
	<script type="text/javascript" src="./js/EDdescriber.js"></script>		
	  <script type="text/javascript" src="./js/edlayout.js"></script> 
		<script type="text/javascript" src="./js/ed_properties_patched.js"></script>
	
	<link href="./css/global.css" rel="stylesheet" type="text/css" />
	<link href="./jquery/css/freebase-controls.css" rel="stylesheet" type="text/css" />
	
  </head>
  
  <body>
		<div id="banner-wrap">  
	  		<div id="banner">
				<div style="float: right; text-align: right;">
			  		<div class="loggedinas"></div>
			  		<div class="login-buttons"></div>	
	  			</div>
	  			<div class="popuptitle"></div>
	  		</div>

	  	</div>	
	    <div id="content">
	    	<div id="popupcontent">
				<form id="form_add" name="add" enctype="application/x-www-form-urlencoded" action="describer" method="post">
					<!-- title table -->
						<table class="identifiedTable">
							<tr>
								<td class="addformlabelcell">Identified: </td>
								<td class="identified"><span class="internet"></span></td>
							</tr>
						</table>
						
					<!-- tag table -->	
						<table class="taggingTable">
							<tr valign='top'>
								<td><span>Filter by Type: </span></td>
								<td><div id="topic_choose_div"></div></td>
							</tr>
							<tr>
								<td class="addformlabelcell">Selected Tags: </td>
								<td class="addformdatacell"><div id="semantic_tags_div"></div></td>
							</tr>
							<tr>
								<td class="addformlabelcell" style="cursor: pointer;"> Semantic Tags: </td>
								<td class="addformdatacell"><div id="ed_semantic_autocomplete_div"></div></td>
							</tr>
							<tr>
								<td class="addformlabelcell"> Personal Tags: </td>
								<td class="addformdatacell"><div id="ed_personal_autocomplete_div"></div></td>
							</tr>
							<tr>
								<td class="" valign="bottom">Selected Tags: </td>
								<td class="addformdatacell"><div id="personal_tags_div"></div></td>
							</tr>
							<tr>
								<td>
								</td>
								<td>
									<button id="addbutton" class="buttonctl" value="Add to my library" style="float:right;">Add to my library</button>
								</td>
							</tr>
						</table>
				</form>	    		
	    	</div>
	    </div>
	    <br><br>
	    <div id="debug"></div>
	    <br>
	    <div id="ed-footer-wrap">
	    	<div id="ed-footer"></div>
	    </div>
  </body>
</html>
