package org.icapture.ED.connotea;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpState;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.icapture.ED.config.EdConfig;
import org.icapture.web.HttpUtil;

/**
 * Confirms that a user is logged in and valid and brings up the bookmark page 
 * of Entity Describer with uri and title replaced by textboxes for manual entry of the uri and title.
 * @author Paul Lu
 *
 */
public class AddToEDManualURI extends HttpServlet {

    static final long serialVersionUID = 1L;

    private String username = null;

    private String password = null;

    private static Logger logger = Logger.getLogger(AddToEDManualURI.class);

	
	
	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
			doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{

		if (username == null || password == null) {
		    username = EdConfig.ED_CONNOTEA_USERNAME;
		    password = EdConfig.ED_CONNOTEA_PASSWORD;
		}
		/**
		 * here we check for the ED_USERNAME cookie. if it doesnt exist, then
		 * redirect to the login page
		 */
		Cookie[] cookieArray = request.getCookies();
		String val = HttpUtil.getCookieValue(cookieArray, "ED_USERNAME", null);
		
		if (request.getCookies() == null
			|| HttpUtil.getCookieValue(request.getCookies(), "ED_USERNAME",
				null) == null) {
		    StringBuffer sb = new StringBuffer();
		    sb.append("?");
		    Enumeration<String> names = request.getParameterNames();
		    boolean hasParams = false;
		    while (names.hasMoreElements()) {
			String name = names.nextElement();
			sb.append(name
				+ "="
				+ URLEncoder
					.encode(request.getParameter(name), "UTF-8")
				+ "&");
			hasParams = true;
		    }
		    response.sendRedirect("ed/login.html" + (hasParams ? sb.toString() : ""));
		    return;
		}

		/**
		 * here the username exists. make sure that we are given uri, title
		 * 
		 */
		String uri = "";
		String title = "";
		String user = HttpUtil.getCookieValue(request.getCookies(), "ED_USERNAME", null);
		
		// get the page
		String page = getPage(uri, title, username, password);

		// replace the logout function with our own
		page = page.replaceAll("http://www.connotea.org/logout","javascript:logout();");
		page = page.replaceAll("http://www.connotea.org/library", "http://www.connotea.org/user/"+user);
		page = page.replaceAll("target=\"_blank\">Log out", ">Log out of E.D.");
		//replace the Look Up button

		page = page.replaceAll("<input type=\"submit\" name=\"button\" value=\"Look Up\" class=\"buttonctl\" id=\"addbutton\" />", "");
		// add our footer
		page = page.replaceAll("\"footer\"","\"ed-footer\"");
		page = page.replaceAll("\"footer-wrap\"","\"ed-footer-wrap\"");
		
		page = page.replaceAll("&copy; 2005-2008 Nature Publishing Group","");
		
		//strip connotea specific syle and page info
		page = page.replaceAll("<head profile=\"http://www.nature.com/common/xmdp-profiles/xoxo.html\">", "");
		page = page.replaceAll("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"http://www.nature.com/common/style/restrict_width/930px.css\" />","");
		page = page.replaceAll("<link rel=\"stylesheet\" type=\"text/css\" media=\"screen\" href=\"http://www.nature.com/common/style/print.css\" />","");
		
		// add our style sheet
		page = page.replaceAll("http://www.connotea.org/global.css",
			"css/global.css");
		// replace the title and add ed_connotea.js to the add page
		page = page
			.replaceAll(
				"<title>Connotea: addpopup</title>",
				"<title>Connotea +E.D.: addpopup</title>\n<!-- for FreeBase connection -->\n"
					+ "<link type=\"text/css\" rel=\"stylesheet\" href=\"jquery/css/freebase-controls.css\" />\n"
					+ "<script type =\"text/javascript\" src=\"js/EDcookie.js\"></script>\n"
					+ "<script type=\"text/javascript\" >var __E__D__U__S__E__R__='"+user+"'</script>\n"
					+ "<script type=\"text/javascript\" src=\"jquery/jquery.js\"></script>\n"
					+ "<script type=\"text/javascript\" src=\"jquery/jquery.freebase.suggest.js\"></script>\n" 
					+ "<script type=\"text/javascript\" src=\"jquery/jquery.form.js\"></script>\n"
					+ "<script type=\"text/javascript\" src=\"js/ed_properties_patched.js?"+System.currentTimeMillis()+"\"></script>\n"
					+ "<script type=\"text/javascript\" src=\"js/edlayout.js\"></script>\n"
					+ "<script type=\"text/javascript\" src=\"js/ed_connotea.js\"></script>\n"
					+ "<script type=\"text/javascript\" src=\"js/EDprogressBar.js\"></script>\n");

		page = page.replaceAll("http://www.connotea.org/autocomplete.js", "js/connotea_autocomplete.js");
		page = page.replaceAll("document\\.forms\\.add\\.tags\\.focus\\(\\);", "");
		//Give the form an id
		page = page.replaceAll("name=\"add\"", "name=\"add\" id=\"form_add\"");
		
		//add on an example span under Bookmark URL:
		page = page.replaceAll("Bookmark URL:", "Bookmark URL: <span class=\"optional\">(eg. http://www.connotea.org)</span>   ");
		// Set to expire far in the past.
		response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
		// Set standard HTTP/1.1 no-cache headers.
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		// Set IE extended HTTP/1.1 no-cache headers (use addHeader).
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");

		// Set standard HTTP/1.0 no-cache header.
		response.setHeader("Pragma", "no-cache");
		response.setContentType("text/html");
		response.getWriter().print(page);
	}
	
    private String getPage(String uri, String title, String user, String pass)
    throws HttpException, IOException {
		StringBuffer sb = new StringBuffer();
		String strURL = "http://www.connotea.org/addpopup?continue=confirm&uri="
			+ URLEncoder.encode(uri, "UTF-8")
			+ "&title="
			+ URLEncoder.encode(title, "UTF-8");
		
		HttpClient client = new HttpClient();
		client.getHostConfiguration().setHost("www.connotea.org", 80, "http");
		client.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
		
		HttpState initialState = new HttpState();
		client.setState(initialState);
		
		PostMethod authpost = new PostMethod("/login");
		authpost.addRequestHeader(new Header("User-Agent",
			"Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"));
		authpost.setRequestBody(new NameValuePair[] {
			new NameValuePair("username", user),
			new NameValuePair("password", pass),
			new NameValuePair("button", "Login"),
			new NameValuePair("dest", strURL) });
		client.executeMethod(authpost);
		authpost.releaseConnection();
		
		// Usually a successful form-based login results in a redicrect to
		
		int statuscode = authpost.getStatusCode();
		if ((statuscode == HttpStatus.SC_MOVED_TEMPORARILY)
			|| (statuscode == HttpStatus.SC_MOVED_PERMANENTLY)
			|| (statuscode == HttpStatus.SC_SEE_OTHER)
			|| (statuscode == HttpStatus.SC_TEMPORARY_REDIRECT)) {
		    Header header = authpost.getResponseHeader("location");
		    if (header != null) {
			String newuri = header.getValue();
			if ((newuri == null) || (newuri.equals(""))) {
			    newuri = "/";
			}
			logger.info("Redirect target: " + newuri);
			GetMethod redirect = new GetMethod(newuri);
		
			client.executeMethod(redirect);
			sb.append(redirect.getResponseBodyAsString());
			// release any connection resources used by the method
			redirect.releaseConnection();
		    } else {
			logger.info("Invalid redirect");
		    }
		}
		
		return sb.toString();
	}

}
