package org.icapture.ED.connotea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class for Servlet: JsonUserTags
 * Used to contact Connotea to get a User's existing tags
 * 
 */
public class JsonUserTags extends javax.servlet.http.HttpServlet implements
	javax.servlet.Servlet {
    static final long serialVersionUID = 1L;

    /*
     * (non-Java-doc)
     * 
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public JsonUserTags() {
	super();
    }

    /*
     * (non-Java-doc)
     * 
     * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request,
     *      HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
    	String username = request.getParameter("username");

		response.setHeader("Pragma", "no-cache");
		response.addHeader("Cache-Control", "must-revalidate");
		response.addHeader("Cache-Control", "no-cache");
		response.addHeader("Cache-Control", "no-store");
		response.setDateHeader("Expires", 0);
		response.setContentType("text/plain");

		PrintWriter pw = response.getWriter();
		String newline = System.getProperty("line.separator");
			if (username == null) 
			{
			    pw.print("{}");
			    return;
			}

		ArrayList<String> tags = new ArrayList<String>();
		tags = getUserTags(username);
		pw.write("{");
		boolean writeComma = false;
	
			for (String ns : tags) 
			{
			    if (writeComma) 
			    {
			    	pw.write("," + newline);
			    } else{
			    		writeComma = true;
			    	  }
			    pw.write("\"" + ns.trim() + "\" : 0");
			}

		pw.write("}");
	
		return;

    }

    /**
     * Gets the tags from a user has used from connotea
     * @param username
     * @return tags - An ArrayList of Strings representing the tags a user used in connotea
     */
    private ArrayList<String> getUserTags(String username) 
    {
		ArrayList<String> tags = new ArrayList<String>();
		String url = "http://www.connotea.org/txt/tags/user/"+username;
		try {
			    URL u = new URL(url);
			    BufferedReader br = new BufferedReader(new InputStreamReader(u.openStream()));
			    String line = null;
			    while ((line=br.readLine()) != null) 
			    {
			    	tags.add(line.trim());
			    }
		} catch (Exception e) {
		    
		}
	
		return tags;
    }

    /*
     * (non-Java-doc)
     * 
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request,
     *      HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }
}