package org.icapture.ED;


/**
 * Keeps default values for the DataStore (biomoby.elmonline.ca/sparql) and 
 * contains methods for setting new values for both DataStore
 * and setting those values in ED on deployment by calling the process method
 * in the context loader
 * 
 * @author Paul_Lu
 *
 */
public class TaggingDataStore {

   /*SPARQLENDPOINT DEFAULT VALUES FOR ED: */
    private String SPARQL_EP_DATASTORE_url = "http://biomoby.elmonline.ca/sparql";

    private String SPARQL_EP_DATASTORE_query_param_name = "query";
    
    private String SPARQL_EP_DATASTORE_parameters = null;
    
    /*SPARQLENDPOINT VARIABLE NAMES FOR ED: */
    public static String SPARQL_EP_DATASTORE_URL = "SPARQL_EP_DATASTORE_url";
    public static String SPARQL_EP_DATASTORE_QUERY_PARAM_NAME = "SPARQL_EP_DATASTORE_query_param_name";
    public static String SPARQL_EP_DATASTORE_PARAMETERS = "SPARQL_EP_DATASTORE_parameters";
    
    
    /*ED SPARQLENDPOINT GETTERS & SETTERS */

    /**
     * @return name of the query paramter used by the SPARQLEndpoint
     */
    public String getSPARQL_EP_DATASTORE_url() 
    {
    	return SPARQL_EP_DATASTORE_url;
    }
    
    /**
     * @param SPARQL_EP_DATASTORE_url the url of the SPARQL Endpoint to send queries to
     */    
    public void setSPARQL_EP_DATASTORE_url(String SPARQL_EP_DATASTORE_url) 
    {
    	this.SPARQL_EP_DATASTORE_url = SPARQL_EP_DATASTORE_url;
    }
    
    /**
     * @return the name of the query paramter used by the SPARQLEndpoint
     */
    public String getSPARQL_EP_DATASTORE_query_param_name() 
    {
    	return SPARQL_EP_DATASTORE_query_param_name;
    }
    
    /**
     * @param SPARQL_EP_DATASTORE_query_param_name name of the query paramter used by the SPARQLEndpoint
     */    
    public void setSPARQL_EP_DATASTORE_query_param_name(String SPARQL_EP_DATASTORE_query_param_name) 
    {
    	this.SPARQL_EP_DATASTORE_query_param_name = SPARQL_EP_DATASTORE_query_param_name;
    }
    
    /**
     * @return A String reresentation of the JSONObject containing the default parameters for the SPARQL Endpoint
     */
    public String getSPARQL_EP_DATASTORE_parameters()
    {
    	return SPARQL_EP_DATASTORE_parameters;
    }
    
    /**
     * @param SPARQL_EP_DATASTORE_parameters A String representation of the JSONObject 
     * containing the default parameters for the SPARQL Endpoint
     */
    public void setSPARQL_EP_DATASTORE_parameters(String SPARQL_EP_DATASTORE_parameters)
    {
    	this.SPARQL_EP_DATASTORE_parameters = SPARQL_EP_DATASTORE_parameters;
    }
   
    /**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
