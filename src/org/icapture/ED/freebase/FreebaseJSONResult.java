package org.icapture.ED.freebase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Object used to create JSON for a Result that conforms to the JSON format of the
 * Entity Describer Data Source API.
 * @author Paul Lu
 *
 */
public class FreebaseJSONResult {

	private JSONObject FreebaseResult;
	private HashMap TypeMap;
	private ArrayList aliases;

	/**
	 * Construct and load a FreebaseJSONResult Object with values
	 * 
	 * @param TypeMap -
	 * 					A HashMap of the Results types with key=id and value=name
	 * @param aliases -
	 * 					ArrayList of Strings representing Alternate names 
	 * @param articleID -
	 * 					The id of the blurb
	 * @param imageID -
	 * 					The id of the thumbnail
	 * @param guid -
	 * 					The guid
	 * @param name - 
	 * 					Name of the resulting term
	 * @param id -
	 * 					The id
	 * @param relevance_score -
	 * 					The relevance score from the search
	 */
	public FreebaseJSONResult(HashMap TypeMap, ArrayList<String> aliases, String articleID, String imageID, String guid, String name, String id, double relevance_score)
	{
		//Instantiate Root Object
		FreebaseResult = new JSONObject();
		
		if(TypeMap != null) //instantiate TypeMap
		{
			this.TypeMap = TypeMap;
		}else{
			this.TypeMap = new HashMap();
		}
		
		if(aliases != null) //instantiate aliases
		{
			this.aliases = aliases;
		}else{
			this.aliases = new ArrayList();
		}
		
			//Set values for Root JSONObject
			try {
					FreebaseResult.put("article", JSONObject.NULL);
					FreebaseResult.put("image", JSONObject.NULL);
					if(articleID != null)
					{
						JSONObject articleObject = new JSONObject("{\"id\": \"" + articleID + "\"}");
						FreebaseResult.put("article", articleObject);
					}
					if(imageID !=null)	
					{
						JSONObject imageObject = new JSONObject("{\"id\": \"" + imageID + "\"}");
						FreebaseResult.put("image", imageObject);
					}
						FreebaseResult.put("name", name);
						FreebaseResult.put("id", id);
						FreebaseResult.put("relevance:score", relevance_score);
						FreebaseResult.put("guid", guid);
						
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	
	/**
	 * Constructs an empty FreebaseJSONResult Object
	 */
	public FreebaseJSONResult()
	{
		//master Object
		FreebaseResult = new JSONObject();
		
			this.TypeMap = new HashMap();
			this.aliases = new ArrayList();
		
					try {
							FreebaseResult.put("article", JSONObject.NULL);
							FreebaseResult.put("image", JSONObject.NULL);
							FreebaseResult.put("name", "");
							FreebaseResult.put("id", "");
							FreebaseResult.put("relevance:score", 0);
							FreebaseResult.put("guid", "");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
	}
	
	/**
	 * Set the TypeMap
	 * @param TypeMap
	 */
	public void setTypeMap(HashMap<String, String> TypeMap)
	{
		this.TypeMap = TypeMap; 
	}
	
	/**
	 * Get the TypeMap
	 * @return Types
	 */
	public HashMap<String, String> getTypeMap()
	{
		return this.TypeMap;
	}
	
	/**
	 * add in a type
	 * @param id - 
	 * 				id of the type
	 * @param name - 
	 * 				name of the type
	 */
	public void addType(String id, String name)
	{
		this.TypeMap.put(id, name);
	}
	
	/**
	 * remove a type
	 * @param id - 
	 * 				id of the type
	 */
	public void removeType(String id)
	{
		this.TypeMap.remove(id);
	}
	
	/**
	 * Set the Aliases
	 * @param aliases
	 */
	public void setAliases(ArrayList<String> aliases)
	{
		this.aliases = aliases;
	}
	
	/**
	 * Add an Alias
	 * @param alias
	 */
	public void addAlias(String alias)
	{
		this.aliases.add(alias);
	}
	
	/**
	 * Remove an Alias
	 * @param alias
	 */
	public void removeAlias(String alias)
	{
		this.aliases.remove(alias);
	}
	
	/**
	 * Get the Aliases as an ArrayList
	 * @return ArrayList of aliases
	 */
	public ArrayList<String> getAliases()
	{
		return aliases;
	}

	/**
	 * Set the ArticleID
	 * @param articleID
	 * @throws JSONException
	 */
	public void setArticleID(String articleID) throws JSONException
	{
		if(articleID == null)
		{
			this.FreebaseResult.put("article", JSONObject.NULL);
		}else{
			JSONObject articleObject = new JSONObject("{\"id\": \"" + articleID + "\"}");
				this.FreebaseResult.put("article", articleObject);
		}
	}
	
	/**
	 * Get the ArticleID
	 * @return the ArticleID
	 * @throws JSONException
	 */
	public String getArticleID() throws JSONException
	{
		String articleID = this.FreebaseResult.getJSONObject("article").getString("id");
		return articleID;
	}
	
	/**
	 * Set the ImageID
	 * @param imageID
	 * @throws JSONException
	 */
	public void setimageID(String imageID) throws JSONException
	{
		if(imageID == null)
		{
			this.FreebaseResult.put("image", JSONObject.NULL);
		}else{
		JSONObject imageObject = new JSONObject("{\"id\": \"" + imageID + "\"}");
			this.FreebaseResult.put("image", imageObject);
		}
	}
	
	/**
	 * Get the ImageID
	 * @return the ImageID
	 * @throws JSONException
	 */
	public String getImageID() throws JSONException
	{
		String imageID = this.FreebaseResult.getJSONObject("article").getString("id");
		return imageID;
	}
	
	/**
	 * set the guid
	 * @param guid
	 * @throws JSONException
	 */
	public void setGuid(String guid) throws JSONException
	{
			this.FreebaseResult.put("guid", guid);
	}
	
	/**
	 * get the guid
	 * @return the guid
	 * @throws JSONException
	 */
	public String getGuid() throws JSONException
	{
		String guid =  this.FreebaseResult.getString("guid");
		return guid;
	}
	
	/**
	 * set the id
	 * @param id
	 * @throws JSONException
	 */
	public void setid(String id) throws JSONException
	{
			this.FreebaseResult.put("id", id);
	}
	
	/**
	 * get the id
	 * @return id
	 * @throws JSONException
	 */
	public String getid() throws JSONException
	{
		String id =  this.FreebaseResult.getString("id");
		return id;
	}
	
	/**
	 * set the name
	 * @param name
	 * @throws JSONException
	 */
	public void setName(String name) throws JSONException
	{
			this.FreebaseResult.put("name", name);
	}
	
	/**
	 * get the name
	 * @return name
	 * @throws JSONException
	 */
	public String getName() throws JSONException
	{
		String name  =  this.FreebaseResult.getString("name");
		return name;
	}
	
	/**
	 * set the relevance score
	 * @param relevance_score
	 * @throws JSONException
	 */
	public void setRelevanceScore(double relevance_score) throws JSONException
	{
			this.FreebaseResult.put("relevance:score", relevance_score);
	}
	
	/**
	 * Get the relevance score as a double
	 * @return relevance score
	 * @throws JSONException
	 */
	public Double getRelevanceScore() throws JSONException
	{
		Double relevance_score =  this.FreebaseResult.getDouble("relevance:score");
		return relevance_score;
	}
	
	/**
	 * Sets the Ontology
	 * @param ontology
	 * @throws JSONException
	 */
	public void setOntology(String ontology) throws JSONException 
	{
		this.FreebaseResult.put("ontology", ontology);
	}
	
	/**
	 * Gets the Ontology
	 * @return ontology
	 * @throws JSONException
	 */
	public String getOntology() throws JSONException 
	{
		return this.FreebaseResult.getString("ontology");
	}
	/**
	 * Put together all the components to make the complete FreebaseResult JSONObject
	 * that conforms to the Entity Describer API for data source using the inputs
	 * @param aliases
	 * @param typeMap
	 * @return JSONObject 
	 * @throws JSONException
	 */
	private JSONObject makefreebaseResultObject(ArrayList aliases, HashMap<String, String> typeMap) throws JSONException
	{

		//Create aliases JSONArray
			JSONArray alias = new JSONArray(aliases);
		
		//Create Types JSONArray
			JSONArray types = new JSONArray();
			Set<String> typeKeySet = typeMap.keySet();
				
				for(Iterator<String> it = typeKeySet.iterator(); it.hasNext();)
				{
					//create type object
					String typeid = it.next();
					
						JSONObject type = new JSONObject();
							type.put("name", typeMap.get(typeid));
							type.put("id", typeid);
					//add type object
					types.put(type);
				}
		
		FreebaseResult.put("alias", alias);
		FreebaseResult.put("type", types);
			
		return FreebaseResult;
	}
	
	/**
	 * Get the FreebaseJSONResult as a JSONObject that conforms to Entity Describer API for a data source's
	 * Result
	 * @return JSONObject 
	 */
	public JSONObject toJSON()
	{
		JSONObject fbResult = null;
		try {
			fbResult =  makefreebaseResultObject(this.aliases, this.TypeMap);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fbResult;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FreebaseJSONResult fbResult = new FreebaseJSONResult();
		JSONObject resultObject = fbResult.toJSON();
		
		try {
			System.out.println(resultObject.toString(5));
			
			System.out.println("ENTER VALUES: ");
			fbResult.setArticleID("/guid/9202a8c04000641f80000000000180e0");
			fbResult.setimageID(null);
			fbResult.setid("/en/fox");
			fbResult.setGuid("#9202a8c04000641f80000000000180d7");
			fbResult.setRelevanceScore(54.9008026123);
			fbResult.setName("fox");
			fbResult.addAlias("Fox");
			fbResult.addAlias("RedFox");
			fbResult.addType("/biology/organism_classification", "Organism Classification");
			fbResult.addType("/fictional_universe/character_species", "Character Species");
			resultObject = fbResult.toJSON();
			System.out.println(resultObject.toString(5));
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}

