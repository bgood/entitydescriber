package org.icapture.ED.freebase;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Organizes parameters of a JSONObject that represents Search Results
 * Conforms to the format of the Freebase / Entity Describer API for how the parameters are 
 * supposed to be organized
 * @author Paul Lu
 *
 */
public class FreebaseJSONSearchResponse {

	private JSONObject fbResponseObject;
	private JSONObject queryObject;
	private JSONArray typesArray;
	private JSONArray messages;
	private JSONArray results;
	
	private URLDecoder decoder = new URLDecoder();

	/* CONSTANTS */
	public static final String STATUS_OK = "200 OK";
	public static final String STATUS_BAD_REQUEST = "400 Bad request";
	public static final String CODE_OK = "/api/status/ok";
	public static final String CODE_BAD_REQUEST = "/api/status/error/textsearch";
	public static final String CODE_MISSING_FIELD = "/api/status/error/fields/missing";
	public static final String EMPTY_RESULT_MESSAGE = "No search results found"; 
	
	/**
	 * Constructor for making a JSONObject representation of a Freebase search response with values
	 * from the input 
	 * @param status -
	 * 					the http Status eg. "200 OK"
	 * @param start -	
	 * 					Which result to begin showing results
	 * @param prefix -
	 * 					String used to search for terms
	 * @param limit -
	 * 					Max number of results to show
	 * @param escape - 
	 * 					Format you wish to have the text escaped
	 * @param query -
	 * 					String used to search for terms
	 * @param types -
	 * 					ArrayList of Type ids  eg "/common/Topic"
	 * @param strict -
	 * 					How to match over multiple types
	 * @param code -
	 * 					represents status eg. "/api/status/ok"
	 * @param results -
	 * 					ArrayList of either ALL FreebaseJSONResult or JSONObjects resulting from FreebaseJSONResult.toJSON
	 * 					
	 */
	public FreebaseJSONSearchResponse(String status, int start, String prefix, int limit, String escape, String query, ArrayList<String> types, String strict, String code, ArrayList results) {

		//instantiate the Objects
		fbResponseObject = new JSONObject();
			queryObject = new JSONObject();
			messages = new JSONArray();

			if(types != null) //set types
			{
				typesArray = new JSONArray(types);
			}else{
				typesArray = new JSONArray();
			}
			
			if(results != null && results.size() != 0)
			{
				if(results.get(0) instanceof FreebaseJSONResult) //if Type FreebaseJSONResult
				{
					this.results = new JSONArray();
					for(Iterator it = results.iterator(); it.hasNext();)
					{
						FreebaseJSONResult fbResult = (FreebaseJSONResult) it.next();
						this.results.put(fbResult.toJSON());
					}
				}else if(results.get(0) instanceof JSONObject){ //if type JSONObject
					this.results = new JSONArray(results);
				}
			}else{
				this.results = new JSONArray();
			}
			
		try {
			//make query JSONObject
				queryObject.put("start", start)
							.put("limit", limit)
							.put("prefix", prefix)
							.put("query", query)
							.put("escape", escape);
			//make ResponseObject	
				fbResponseObject.put("status", status)
								.put("strict", strict)
								.put("code", code);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Constructs an empty JSONSearchResponse Object
	 */
	public FreebaseJSONSearchResponse()
	{
		fbResponseObject = new JSONObject();
			queryObject = new JSONObject();
			typesArray = new JSONArray();
			messages = new JSONArray();

		try {
			//make query JSONObject
				queryObject.put("start", 0)
							.put("limit", 0)
							.put("prefix", "null")
							.put("query", "null")
							.put("escape", "");
			//make ResponseObject	
				fbResponseObject.put("status", "")
								.put("strict", "")
								.put("code", "");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Constructs a FreebaseJSONSearchResponse which is an Error Response
	 * @param status -
	 * 					HttpStatus of the Response eg. '400 Bad Request'
	 * @param code - 
	 * 					API code for status
	 * @param info -
	 * 					info
	 * @param message -
	 * 					Message explaining cause of error
	 * @param msgCode -
	 * 					Message Code
	 */
	public FreebaseJSONSearchResponse(String status, String code, JSONObject info, String message, String msgCode)
	{
		fbResponseObject = new JSONObject();
			messages = new JSONArray();
		JSONObject msgObject = new JSONObject();
		try {
			msgObject.put("message", message)
						.put("code", msgCode)
						.put("info", info);
			
			messages.put(msgObject);
			
			fbResponseObject.put("status", status)
								.put("code", code)
								.put("info", info);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Constructs and initialize FreebaseJSONSearchResponse Object with the query string containing the parameters
	 * of the search
	 * 
	 * @param urlQueryString -
	 * 							QueryString from the URL
	 */
	public FreebaseJSONSearchResponse(String urlQueryString)
	{
		fbResponseObject = new JSONObject();
			this.queryObject = new JSONObject();
			typesArray = new JSONArray();
			messages = new JSONArray();

			//initialize queryObject
			try {
					this.queryObject.put("start", 0)
								.put("limit", 0)
								.put("prefix", "")
								.put("query", "")
								.put("escape", "");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		//Break down the queryString into its keys and values and insert them into FreebaseJSONSearchResponse Object
			
		if(urlQueryString.contains("?"))
			urlQueryString = urlQueryString.substring(1);
		
		String[] queryParameters = urlQueryString.split("&");
		for(int i = 0 ; i < queryParameters.length ; i++)
		{
			String param = queryParameters[i];
			String key = param.substring(0, param.lastIndexOf("="));
			String value = param.substring(param.lastIndexOf("=") + 1, param.length());
				   try {
					value = decoder.decode(value, "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
			
				   if(key.compareTo("callback") == 0) //do not add callback
					   continue;
			
					try {
							if(key.compareToIgnoreCase("strict") == 0)
							{
									this.fbResponseObject.put(key, value);
									continue;	
							}
							
							if(key.compareTo("type") == 0)
							{
								typesArray.put(value);
								continue;
							}
									this.queryObject.put(key, value);
							
				} catch (JSONException e) {
					e.printStackTrace();
				}
		}
		
	}

	/**
	 * Add a Message to the FreebaseJSONSearchResponse
	 * @param message
	 * @param info
	 * @param msgCode
	 * @throws JSONException
	 */
	public void addMessage(String message, JSONObject info, String msgCode) throws JSONException
	{
	
		JSONObject msgObject = new JSONObject("{\"message\": " + JSONObject.quote(message) + "}");
		msgObject.put("code", msgCode);
		msgObject.put("info", info);
		
		messages.put(msgObject);	
	}

	/**
	 * Add a type that the search was done over
	 * @param type
	 * 				id of the type
	 */
	public void addType(String type)
	{
		this.typesArray.put(type);
	}
	
	/**
	 * Set the ArrayList representing types that were searched over
	 * @param types
	 */
	public void setType(ArrayList<String> types)
	{
		this.typesArray = new JSONArray(types);
	}
	
	/**
	 * Get the FreebaseJSONSearchResponse's types as a JSONArray
	 * @return JSONArray of the the Types
	 */
	public JSONArray getTypes()
	{
		return this.typesArray;
	}
	
	/**
	 * Add a JSONObject representation of FreebaseJSONResult to the FreebaseJSONSearchResponse
	 * @param result 
	 */
	public void addResult(JSONObject result)
	{
		this.results.put(result);
	}
	
	/**
	 * Get the FreebaseJSONSearchResponse as a JSONArray
	 * @return JSONArray of the results
	 */
	public JSONArray getResults()
	{
		return this.results;
	}
	
	/**
	 * Add a FreebaseJSONResult result to the FreebaseJSONSearchResponse 
	 * @param fbResult
	 */
	public void addResult(FreebaseJSONResult fbResult)
	{
		this.results.put(fbResult.toJSON());
	}
	
	/**
	 * Set the FreebaseJSONSearchResponse results with an ArrayList of JSONObjects representing FreebaseJSONResults
	 * @param JSONfbResults
	 */
	public void setResult(ArrayList<JSONObject> JSONfbResults)
	{
		this.results = new JSONArray(JSONfbResults);
	}
	
	/**
	 * set the FreebaseJSONSearchResponse results with an ArrayList of FreebaseJSONResults
	 * @param JSONfbResults
	 */
	public void setResultfromArrayListofFreebaseJSONResult(ArrayList<FreebaseJSONResult> JSONfbResults)
	{
		this.results = new JSONArray();
		for(Iterator<FreebaseJSONResult> i = JSONfbResults.iterator() ; i.hasNext();)	
			this.results.put(i.next().toJSON());
	}
	
	/**
	 * set the FreebaseJSONSearchResponse results with a Collection of FreebaseJSONResults
	 * @param resultsCollection
	 */
	public void setResult(Collection<FreebaseJSONResult> resultsCollection) {
		this.results = new JSONArray();
		for(Iterator<FreebaseJSONResult> i = resultsCollection.iterator() ; i.hasNext();)	
			this.results.put(i.next().toJSON());
	}
	
	/**
	 * Set the status of FreebaseJSONSearchResponse
	 * @param status
	 * @throws JSONException
	 */
	public void setStatus(String status) throws JSONException
	{
			fbResponseObject.put("status", status);
	}
	
	/**
	 * Get the status of FreebaseJSONSearchResponse
	 * @return status
	 * @throws JSONException
	 */
	public String getStatus() throws JSONException
	{
		return  fbResponseObject.getString("status");
	}
	
	/**
	 * Set the code of FreebaseJSONSearchResponse
	 * @param code
	 * @throws JSONException
	 */
	public void setCode(String code) throws JSONException
	{
			fbResponseObject.put("code", code);
	}
	
	/**
	 * get the code of FreebaseJSONSearchResponse
	 * @return code
	 * @throws JSONException
	 */
	public String getCode() throws JSONException
	{
		return  fbResponseObject.getString("code");
	}
	
	/**
	 * set the strict of FreebaseJSONSearchResponse
	 * @param strict
	 * @throws JSONException
	 */
	public void setStrict(String strict) throws JSONException
	{
			fbResponseObject.put("strict", strict);
	}
	
	/**
	 * get the strict of FreebaseJSONSearchResponse
	 * @return strict
	 * @throws JSONException
	 */
	public String getStrict() throws JSONException
	{
		return  fbResponseObject.getString("strict");
	}
	
	/**
	 * set the start of FreebaseJSONSearchResponse
	 * @param start
	 * @throws JSONException
	 */
	public void setStart(int start) throws JSONException
	{
		this.queryObject.put("start", start);
	}
	
	/**
	 * get the start of FreebaseJSONSearchResponse
	 * @return start
	 * @throws JSONException
	 */
	public int getStart() throws JSONException
	{
		return this.queryObject.getInt("start");
	}
	
	/**
	 * set the limit of FreebaseJSONSearchResponse
	 * @param limit
	 * @throws JSONException
	 */
	public void setLimit(int limit) throws JSONException
	{
		this.queryObject.put("limit", limit);
	}
	
	/**
	 * get the limit of FreebaseJSONSearchResponse
	 * @return limit
	 * @throws JSONException
	 */
	public int getLimit() throws JSONException
	{
		return this.queryObject.getInt("limit");
	}
	
	/**
	 * set the Prefix of FreebaseJSONSearchResponse
	 * @param prefix
	 * @throws JSONException
	 */
	public void setPrefix(String prefix) throws JSONException
	{
		this.queryObject.put("prefix", prefix);
	}
	
	/**
	 * get the prefix of FreebaseJSONSearchResponse
	 * @return prefix
	 * @throws JSONException
	 */
	public String getPrefix() throws JSONException
	{
		return this.queryObject.getString("prefix");
	}
	
	/**
	 * set the query of FreebaseJSONSearchResponse
	 * @param query
	 * @throws JSONException
	 */
	public void setQuery(String query) throws JSONException
	{
		this.queryObject.put("query", query);
	}
	
	/**
	 * get the query of FreebaseJSONSearchResponse
	 * @return query
	 * @throws JSONException
	 */
	public String getQuery() throws JSONException
	{
		return this.queryObject.getString("query");
	}
	
	/**
	 * set the escape of FreebaseJSONSearchResponse
	 * @param escape
	 * @throws JSONException
	 */
	public void setEscape(String escape) throws JSONException
	{
		this.queryObject.put("escape", escape);
	}
	
	/**
	 * get the escape of FreebaseJSONSearchResponse
	 * @return escape
	 * @throws JSONException
	 */
	public String getEscape() throws JSONException
	{
		return this.queryObject.getString("escape");
	}
	
	/**
	 * Return FreebaseJSONSearchResponse as a JSONObject that conforms to the
	 * Entity Describer API for data sources
	 * @return JSONObject
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException
	{
		//check status
		String status = this.fbResponseObject.getString("status");
		
		//IS ERROR
		if(status.compareTo(this.STATUS_OK) != 0)
		{
			JSONObject ErrorResponseObject = new JSONObject();
			ErrorResponseObject.put("status", status)
								.put("code", fbResponseObject.getString("code"))
								.put("messages", this.messages);
			
			return ErrorResponseObject;
		}else{
		
			//IS EMPTY RESULTS
			if(this.results.length() == 0)
			{
				JSONObject EmptyResultResponseObject = new JSONObject();
				
				if(this.messages.length() == 0)
					this.messages.put(new JSONObject("{\"message\": \"" + this.EMPTY_RESULT_MESSAGE + "\"}"));
				
				EmptyResultResponseObject.put("status", this.fbResponseObject.getString("status"))
											.put("code", this.fbResponseObject.getString("code"))
											.put("messages", this.messages)
											.put("result", this.results);
				
				return EmptyResultResponseObject;
			}else{
			//IS NORMAL RESULTS
				this.queryObject.put("type", this.typesArray);
				JSONObject qO = queryObject;
				this.fbResponseObject.put("query", this.queryObject)
										.put("result", this.results);
				
				return this.fbResponseObject;
			}
		}
}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList Results = new ArrayList();
		FreebaseJSONResult result = new FreebaseJSONResult(null, null, "/guid/9202a8c04000641f80000000000180e0", "/wikipedia/images/en_id/9365524", "#9202a8c04000641f80000000000180d7", "Fox", "/en/fox", 54.9008026123);
		result.addType("/common/topic", "Topic");
		result.addType("/biology/organism_classification", "Organism Classification");
		result.addAlias("Fox");
		result.addAlias("PolarFox");
		Results.add(result);
		
		FreebaseJSONSearchResponse fbResponse = new FreebaseJSONSearchResponse(FreebaseJSONSearchResponse.STATUS_OK, 0, "loc", 20, "html", "", null, "true", FreebaseJSONSearchResponse.CODE_OK, Results);

		try {
			
			JSONObject JSONSearchResponse =  fbResponse.toJSON();
			
			System.out.println(JSONSearchResponse.toString(5));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}



}
