package org.icapture.ED.freebase;

import java.net.URLDecoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Object used to create JSON for a blurb that conforms to the JSON format of the
 * Entity Describer Data Source API.
 * @author Paul Lu
 *
 */
public class FreebaseJSONBlurbResponse {

	private URLDecoder decoder = new URLDecoder();
	private JSONObject fbResult = new JSONObject();
	private JSONObject fbResponseObject = new JSONObject();
	private JSONArray messageInfos = new JSONArray();
	
	/* CONSTANTS */ 
	public static final String STATUS_OK = "200 OK";
	public static final String STATUS_NOT_FOUND = "404 Not Found";
	public static final String CODE_OK = "/api/status/ok";
	public static final String CODE_ERROR_CONTENT = "/api/status/error/content";
	public static final String CODE_ERROR_ID_NOT_FOUND = "/api/status/error/load/id_not_found";
	public static final String CODE_MISSING_FIELD = "/api/status/error/fields/missing";
	public static final String EMPTY_RESULT_MESSAGE = "No search results found";
	public static final String ERROR_RETRIEVING_MESSAGE = "Error retrieving content";
	

	/**
	 * Default Constructor used to create an empty FreebaseJSONBlurbResponse
	 */
	public FreebaseJSONBlurbResponse() {
		try {
			this.fbResult.put("body", JSONObject.NULL)
							.put("media_type", JSONObject.NULL)
							.put("text_encoding", JSONObject.NULL);
			
			this.fbResponseObject.put("status", JSONObject.NULL)
									.put("code", JSONObject.NULL);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor used to create a FreebaseJSONBlurbResponse with the input values
	 * @param status
	 * @param code
	 * @param body
	 * @param media_type
	 * @param text_encoding
	 */
	public FreebaseJSONBlurbResponse(String status, String code, String body, String media_type, String text_encoding) {
		// TODO Auto-generated constructor stub
		try {
			this.fbResult.put("body", body)
			.put("media_type", media_type)
			.put("text_encoding", text_encoding);

			this.fbResponseObject.put("status", status)
								.put("code", code);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Add a message to the Freebase JSONBlurblResponse
	 * @param value
	 * @param message
	 * @param code
	 */
	public void addMessage(String value, String message, String code)
	{
		JSONObject msgObject = new JSONObject();
		JSONObject infoObject = new JSONObject();
		try {
			infoObject.put("value", value);

			msgObject.put("message", message)
						.put("code", code)
						.put("info", infoObject);
						
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		messageInfos.put(msgObject);
	}
	
	/**
	 * Set the status of FreebaseJSONSearchResponse
	 * @param status
	 * @throws JSONException
	 */
	public void setStatus(String status) throws JSONException
	{
			fbResponseObject.put("status", status);
	}
	
	/**
	 * Get the status of FreebaseJSONSearchResponse
	 * @return status
	 * @throws JSONException
	 */
	public String getStatus() throws JSONException
	{
		return  fbResponseObject.getString("status");
	}
	
	/**
	 * Set the code of FreebaseJSONSearchResponse
	 * @param code
	 * @throws JSONException
	 */
	public void setCode(String code) throws JSONException
	{
			fbResponseObject.put("code", code);
	}
	
	/**
	 * get the code of FreebaseJSONSearchResponse
	 * @return code
	 * @throws JSONException
	 */
	public String getCode() throws JSONException
	{
		return  fbResponseObject.getString("code");
	}
	
	/**
	 * sets the MediaType of the FreebaseJSONSearchResponse
	 * @param media_type
	 * @throws JSONException
	 */
	public void setMediaType(String media_type) throws JSONException
	{
		fbResult.put("media_type", media_type);
	}
	
	/**
	 * gets the MediaType of the FreebaseJSONSearchResponse
	 * @return media_type
	 * @throws JSONException
	 */
	public String getMediaType() throws JSONException
	{
		return  fbResult.getString("media_type");
	}	
	
	/**
	 * sets the TextEncoding of the FreebaseJSONSearchResponse
	 * @param text_encoding
	 * @throws JSONException
	 */
	public void setTextEncoding(String text_encoding) throws JSONException
	{
		fbResult.put("media_type", text_encoding);
	}
	
	/**
	 * gets the TextEncoding of the FreebaseJSONSearchResponse
	 * @return text_encoding
	 * @throws JSONException
	 */
	public String getTextEncoding() throws JSONException
	{
		return  fbResult.getString("text_encoding");
	}
	
	/**
	 * sets the body of the FreebaseJSONSearchResponse
	 * @param body
	 * @throws JSONException
	 */
	public void setBody(String body) throws JSONException
	{
		fbResult.put("body", body);
	}
	
	/**
	 * gets the body of the FreebaseJSONSearchResponse
	 * @return body
	 * @throws JSONException
	 */
	public String getBody() throws JSONException
	{
		return  fbResult.getString("body");
	}
	
	/**
	 * Returns a JSONObject conforming to the EntityDescriber data source API 
	 * with the values in the FreebaseJSONSearchResponse object
	 * @return JSONObject of the FreebaseJSONSearchResponse 
	 * @throws JSONException
	 */
	public JSONObject toJSON() throws JSONException
	{
		if((fbResponseObject.getString("status")).compareTo(this.STATUS_OK) == 0)
		{
			this.fbResponseObject.put("result", fbResult);
			return this.fbResponseObject;
		}else{
			fbResponseObject.put("messages", messageInfos);
			return this.fbResponseObject;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
