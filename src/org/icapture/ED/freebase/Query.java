package org.icapture.ED.freebase;

import java.util.ArrayList;
import java.util.List;

import org.icapture.web.HttpUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Used to send queries to Freebase
 * @author Ben Good
 *
 */
public class Query {

	static String callback="callback";

	/**
	 * Searches Freebase
	 * @param searchstring
	 * @param types
	 * @param limit
	 * @return String representation of the JSON formatted search results returned by Freebase 
	 * @throws JSONException
	 */
	public static String queryFreebaseViaSearchService(String searchstring, List<String>types, int limit) 
	throws JSONException
	{
		String response = "";
		//ex http://www.freebase.com/api/service/search?type=/film/actor&type=/music/artist&strict=any&prefix=te		
		String basesearch = "http://www.freebase.com/api/service/search";
		//?type=/film/actor&type=/music/artist&strict=any&prefix=te";
		if(types!=null&&types.size()>0){
			String typestring = "";
			typestring="?type=";
			boolean first = true;
			for(String t : types){
				if(first){
					typestring+=t;
					first = false;
				}else{
					typestring+="&type="+t;
				}
			}
			basesearch+=typestring;
		}
		basesearch+="&limit="+limit;
		basesearch+="&strict=true&prefix="+searchstring;
		JSONObject json = httpToFreebase(basesearch);
		response = json.toString(5);
		return response;
	}

	/**
	 * Searches Freebase 
	 * @param searchstring
	 * @param types
	 * @param limit
	 * @return JSONObject of the search results returned by Freebase 
	 * @throws JSONException
	 */
	public static JSONObject queryFreebaseViaSearchServiceGetJSONObject(String searchstring, List<String>types, int limit) 
	throws JSONException
	{
	//ex http://www.freebase.com/api/service/search?type=/film/actor&type=/music/artist&strict=any&prefix=te		
		String basesearch = "http://www.freebase.com/api/service/search";
		//?type=/film/actor&type=/music/artist&strict=any&prefix=te";
		if(types!=null&&types.size()>0){
			String typestring = "";
			typestring="?type=";
			boolean first = true;
			for(String t : types){
				if(first){
					typestring+=t;
					first = false;
				}else{
					typestring+="&type="+t;
				}
			}
			basesearch+=typestring;
			basesearch+="&";
		}else{
			basesearch+="?";
		}
		basesearch+="limit="+limit;
		basesearch+="&strict=true&prefix="+searchstring;
		JSONObject json = httpToFreebase(basesearch);
		return json;
	}

	/**
	 * Get the blurb from freebase corresponding to the guid 
	 * @param guid
	 * @param maxlength
	 * @return JSONObject of the blurb
	 */
	public static JSONObject queryFreebaseViaBlurbServiceGetJSONObject(String guid, int maxlength) 
	{
		//ex http://www.freebase.com/api/service/search?type=/film/actor&type=/music/artist&strict=any&prefix=te		
		String baseblurb = "http://www.freebase.com/api/trans/blurb/guid/";
		baseblurb+=guid;
		baseblurb+="?callback=" + callback + "&maxlength="+maxlength;
		JSONObject json = httpToFreebase(baseblurb);
		
		return json;
	}

	/**
	 * Does a post to Freebase and gets a response containning search results as JSON
	 * @param posturl
	 * @return JSONObject
	 */
	public static JSONObject httpToFreebase(String posturl)
	{
		String test = HttpUtil.getDataFromURL(posturl);
		if(test.indexOf(callback) != -1)
			test = test.substring(callback.length() + 1, test.lastIndexOf(")"));
		
				JSONObject completeresponse = null;
		try {
			completeresponse = new JSONObject(new JSONTokener(test));			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return completeresponse;
	}
	
	/**
	 * Does a post to Freebase and gets a response containning search results as String
	 * @param posturl
	 * @return String 
	 */
	public static String httpToFreebaseGetString(String posturl)
	{
		String test = HttpUtil.getDataFromURL(posturl);
		return test;
	}
	
	/**
	 * @param args
	 * @throws JSONException 
	 */
	public static void main(String[] args) throws JSONException {
		// TODO Auto-generated method stub

		String q = "te";
		List<String> types = new ArrayList<String>();
		types.add("/film/actor"); types.add("/music/artist");
		String r = queryFreebaseViaSearchService(q,types, 5);
		System.out.println("response\n"+r);
		
		queryFreebaseViaBlurbServiceGetJSONObject("9202a8c04000641f8000000000115198", 200);
	}
}
