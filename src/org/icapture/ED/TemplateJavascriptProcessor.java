package org.icapture.ED;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import org.icapture.ED.config.EdConfig;
/**
 * Tool for generating a javascript file from a template file and filling in the blank properties with 
 * properties from data_source.properties and data_store.properties
 * @author Paul Lu
 *
 */
public class TemplateJavascriptProcessor {

	TaggingDataSource datasource = null;
	TaggingDataStore datastore = null;
	
	public TemplateJavascriptProcessor(TaggingDataSource datasource, TaggingDataStore datastore)
	{
		this.datasource = datasource;
		this.datastore = datastore;
	}
	
    /**
     * Reads in the template file ed_properties.js.template - replaces
     * all the DataSource and SPARQLEndpoint specific variables within 
     * ed_properties.js with the values in the datasource.properties file
     * to create a new ed_properties_patched.js that works with the new 
     * DataSource and/or SPARQLEndpoint
     *  
     * @param template
     * 					-js file to apply the DataSource and SPARQLEndpoint changes to
     * @param outfile
     * 					-name of the new file after DataSource and SPARQLEndpoint modifications
     */
    public void process(String template, String outfile) 
    {
		// read in the template file, replace text, write new file
		StringBuffer sb = new StringBuffer();
		String line = null;
		String newline = System.getProperty("line.separator");
		
		try {
		    FileInputStream fis = new FileInputStream(template);
		    BufferedReader reader = new BufferedReader(new InputStreamReader(
			    fis));
		    while ((line = reader.readLine()) != null) {
			
		    /*REPLACE DATASOURCE PARAMETERS*/
		    line = line.replaceAll("@" + datasource.AC_PARAM + "@", datasource.getAc_param());
			line = line.replaceAll("@" + datasource.AC_PATH + "@", datasource.getAc_path());
			line = line.replaceAll("@" + datasource.AC_QSTR + "@", datasource.getAc_qstr());
			line = line.replaceAll("@" + datasource.BLURB_PARAM + "@", datasource.getBlurb_param());
			line = line.replaceAll("@" + datasource.BLURB_PATH + "@", datasource.getBlurb_path());
			line = line.replaceAll("@" + datasource.SERVICE_URL + "@", datasource.getService_url());
			line = line.replaceAll("@" + datasource.THUMBNAIL_PATH + "@", datasource.getThumbnail_path());
			line = line.replaceAll("@" + datasource.DEFAULT_TYPE + "@", datasource.getDefault_type());
			
			line = line.replaceAll("@URL_ROOT@", EdConfig.getED_root_url());
			
			/*REPLACE SPARQLENDPOINT DATASTORE PARAMETERS*/
			line = line.replaceAll("@" + datastore.SPARQL_EP_DATASTORE_URL + "@", datastore.getSPARQL_EP_DATASTORE_url());
			line = line.replaceAll("@" + datastore.SPARQL_EP_DATASTORE_QUERY_PARAM_NAME + "@", datastore.getSPARQL_EP_DATASTORE_query_param_name());
			line = line.replaceAll("@" + datastore.SPARQL_EP_DATASTORE_PARAMETERS + "@", datastore.getSPARQL_EP_DATASTORE_parameters());
		
			sb.append(line + newline);
		    }
		    reader.close();
		    BufferedWriter out = new BufferedWriter(new FileWriter(outfile));
		    out.write(sb.toString());
		    out.close();
		} catch (Throwable e) {
			System.err.println(e);
			e.printStackTrace();
		    System.err.println("*** exception ***");
		}
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	}

}
