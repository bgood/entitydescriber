package org.icapture.ED;

import java.sql.SQLException;
import java.util.Iterator;

import org.icapture.ontology.OntUtil;

import com.hp.hpl.jena.db.DBConnection;
import com.hp.hpl.jena.db.IDBConnection;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ModelMaker;

/**
 * This class manages the ontology entities in our local jena tagging database.
 * It is used by its children (the manager classes, UserManager, TagManager, and OntologyManager)
 * to read and write from the database.
 * 
 * It is built around the SemanticTagging ontology available here
 * http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/
 * which is largely based on one of the original tag ontology from
 * http://www.holygoat.co.uk/owl/redwood/0.1/tags/
 * 
 * @author Benjamin Good
 *
 */
public class SemanticTagging {
	public static String base = "http://bioinfo.icapture.ubc.ca/Resources/";
	public static String ns = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/";
	public static String ont_model = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging";

	private static String M_DB_URL = "jdbc:mysql://localhost/SemanticTagging2?autoReconnect=true"; // database URL

	private static String M_DB_USER = "connotea";     // database user id
	private static String M_DB_PASSWD = "tagger";   // database user password
	private static String M_DB = "MySQL";  // database engine
	public OntModelSpec spec;
	private IDBConnection connection;
	private Model the_db_model;
	public OntModel ct;

	public OntClass UserOntologyView;
	public OntClass User;
	public OntClass Ontology;
	public OntClass TaggingAct;
	public OntClass Tag;
	public OntClass SPARQLEndpoint;
	public OntClass FreebaseType;
	public OntClass Alias;

	public OntResource HiveUser;
	public ObjectProperty associatedTag;
	public ObjectProperty taggedBy;
	public ObjectProperty taggedResource;
	public ObjectProperty equivalentTag;
	public ObjectProperty inferredTag;
	public ObjectProperty hasUser;
	public ObjectProperty hasOntology;
	public ObjectProperty hasSPARQLEndpoint;
	public ObjectProperty hasFreebaseType;
	public ObjectProperty hasAlias;

	public DatatypeProperty isActive;
	public DatatypeProperty isChecked;
	public DatatypeProperty hasOntoName;
	public DatatypeProperty hasOntoColor;
	public DatatypeProperty hasOntoAbbreviation;
	public DatatypeProperty hasOntoDescription;
	public DatatypeProperty	localURI;
	public DatatypeProperty taggedOn;
	public DatatypeProperty termFile;
	public DatatypeProperty hasBaseRequest;
	public DatatypeProperty taggedWithEDVersion;

	public SemanticTagging(String host){
		M_DB_URL = "jdbc:mysql://"+host+"/SemanticTagging2?autoReconnect=true"; // database URL

	}
	public SemanticTagging(){}

	public static void main(String args[]){
		SemanticTagging c = new SemanticTagging();
		c.init();
		for(Iterator it = c.ct.listClasses(); it.hasNext();){
			System.out.println(it.next());
		}
		c.closeConnection();
	}

	public void initDbConnection(){
		/** establish connection to existing db **/
		spec = new OntModelSpec(OntModelSpec.OWL_MEM);
		String M_DBDRIVER_CLASS = "com.mysql.jdbc.Driver";  // database driver
//		load the driver class
		try {
			Class.forName(M_DBDRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		create a database connection
		connection = new DBConnection(M_DB_URL, M_DB_USER, M_DB_PASSWD, M_DB);
//		create a model maker with the given connection parameters
		ModelMaker maker = ModelFactory.createModelRDBMaker(connection);	
//		open a previously created model
		the_db_model = maker.openModel("SemanticTagging");
//		get an OntModel for it
		ct = ModelFactory.createOntologyModel(spec, the_db_model);

	}

	public void bindClasses(){
		//read in base ontology		
		ct.read("http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/");

		UserOntologyView = ct.getOntClass(ns+"UserOntologyView");
		User = ct.getOntClass(ns+"User");
		Ontology = ct.getOntClass(ns+"Ontology");
		hasUser = ct.getObjectProperty(ns+"hasUser");
		hasOntology = ct.getObjectProperty(ns+"hasOntology");
		hasSPARQLEndpoint = ct.getObjectProperty(ns+"hasSPARQLEndpoint");
		SPARQLEndpoint = ct.getOntClass(ns+"SPARQLEndpoint");
		hasBaseRequest = ct.getDatatypeProperty(ns+"hasBaseRequest");
		taggedWithEDVersion = ct.getDatatypeProperty(ns+"taggedWithEDVersion");

		isActive = ct.getDatatypeProperty(ns+"isActive");
		isChecked = ct.getDatatypeProperty(ns+"isChecked");
		hasOntoName = ct.getDatatypeProperty(ns+"hasOntoName");
		hasOntoColor = ct.getDatatypeProperty(ns+"hasOntoColor");
		hasOntoAbbreviation = ct.getDatatypeProperty(ns+"hasOntoAbbreviation");
		hasOntoDescription = ct.getDatatypeProperty(ns+"hasOntoDescription");
		localURI = ct.getDatatypeProperty(ns+"localURI");
		termFile = ct.getDatatypeProperty(ns+"termFile");

		TaggingAct = ct.getOntClass(ns+"Tagging");
		Tag = ct.getOntClass(ns+"Tag");
		taggedOn = ct.getDatatypeProperty(ns+"taggedOn");
		associatedTag = ct.getObjectProperty(ns+"associatedTag");
		taggedBy = ct.getObjectProperty(ns+"taggedBy");
		taggedResource = ct.getObjectProperty(ns+"taggedResource");
		equivalentTag = ct.getObjectProperty(ns+"equivalentTag");
		inferredTag = ct.getObjectProperty(ns+"inferredTag");

		FreebaseType = ct.getOntClass(ns+"FreebaseType");
		hasFreebaseType = ct.getObjectProperty(ns+"hasFreebaseType");

	}

	public void init(String localfile){
		ct = OntUtil.getEmptyOwlModel();
		bindClasses();
		ct.read(localfile);
	}

	public void init(){
		//default init assumes db connection
		initDbConnection();
		bindClasses();		
	}

	public void closeConnection(){
		ct.close();//the ontmodel
		if(the_db_model!=null){
			the_db_model.close(); //the underlying rdf model
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //the mysql connection
		}
	}

}
