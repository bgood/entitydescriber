package org.icapture.ED.dataAccess;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet wrapper for SPARQLEndPointAccess.  Prevents SPARQL injection
 * @author Paul Lu
 *
 */
public class SPARQLQueryServlet extends HttpServlet {

	protected static MessageDigest md = null;
	private static Hex HexEncoder = new Hex();

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * It takes the parameters of the request and response, processes it and sends it to 
	 * the SPARQL Endpoint.  
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//set up the PrintWriter
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		
		//Get the parameters
		String query = request.getParameter("query");
		String callback = request.getParameter("callback");
		String default_graph = request.getParameter("default-graph-uri");
		String format = request.getParameter("format");
		
		//Create a New JSONObject used to send the response back
		JSONObject JSONresp = new JSONObject();
		
		//Handle special statements like insert or delete and send back an error
		if(query.toLowerCase().contains("insert") || query.toLowerCase().contains("delete") || query.toLowerCase().contains("modify"))
		{
			StringBuffer exceptionResp = new StringBuffer();
			exceptionResp.append("Insert, Delete and Modify are Not Permitted");
			try 
			{
				JSONresp.put("resp", exceptionResp.toString());			
				out.println(callback + "(" + JSONresp.toString(3) + ")");
				out.flush();
				out.close();

			} catch (JSONException e) 
				{
					e.printStackTrace();
				}
			
			return;
		}
		
		//Put the Parameters into a JSONObject
		JSONObject SEPparams = new JSONObject();
		try 
		{
				SEPparams.put("callback", callback);
				SEPparams.put("format", format);
				SEPparams.put("query", query);
				
				//Get rid of default graph if the query contains graph in it
				if(query.toLowerCase().contains("graph"))
				{
					SEPparams.put("default-graph-uri", "");
				}else{
						SEPparams.put("default-graph-uri", default_graph);
					 }
		} catch (JSONException e1) {
		
			e1.printStackTrace();
		}

		//Send the query
		ResponseInfo respInfo = SPARQLEndPointAccess.sendQueryOrUpdate(SEPparams, SPARQLEndPointAccess.getSPARQLEndPoint_url());
		
		//If the response format is json, return appropriate json format
		if(format.toLowerCase().contains("json"))
		{
			String respJSONString = respInfo.getResponseAnswer();
			try {
				JSONObject respJSONObject = new JSONObject(respJSONString);
				out.println(callback + "(" + respJSONObject.toString(3) + ")");
			} catch (JSONException e) {
				e.printStackTrace();
			} 
		}else{
			//Otherwise treat it as HTML and process it as such
				String respHTML = respInfo.getResponseAnswer();
		
				try 
				{
					respHTML = respHTML.replace("border=\"1\"", "");
					JSONresp.put("resp", respHTML);
					
					out.println(callback + "(" + JSONresp.toString(3) + ")");
			
				} catch (JSONException e) 
					{
						e.printStackTrace();
					}			
				
				out.flush();
				out.close();
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
