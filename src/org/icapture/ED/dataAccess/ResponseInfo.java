package org.icapture.ED.dataAccess;


/**
 * Structure used to hold a HttpResponse Code and a String representation of 
 * the HttpResponse
 * @author Paul Lu
 *
 */
public class ResponseInfo {

	private int ResponseCode;
	private String ResponseAnswer;
	
	/**
	 * Contructor - Takes Response Code and Response
	 * @param RCode Response Code
	 * @param RAnswer The content of the response
	 */
	public ResponseInfo(int RCode, String RAnswer)
	{
		this.ResponseCode = RCode;
		this.ResponseAnswer = RAnswer;
	}
	
	/**
	 * Assigns ResponseCode -1
	 * And Response Answer with empty tags HEAD TITLE and BODY 
	 */
	public ResponseInfo()
	{
		this.ResponseCode = -1;
		this.ResponseAnswer = "<Answer><HEAD><TITLE></TITLE></HEAD><BODY>0</BODY></Answer>";
	}
	/**
	 * Gets the ResponseCode
	 * @return the ResponseCode
	 */
	public int getResponseCode()
	{
		return ResponseCode;
	}
	/**
	 * Gets the Response's Answer
	 * @return the contents of the Response
	 */
	public String getResponseAnswer()
	{
		return ResponseAnswer;
	}
	
	/**
	 * Sets the Response Code
	 * @param RCode
	 */
	public void setResponseCode(int RCode)
	{
		this.ResponseCode = RCode;
		
	}

	
	/**
	 * Sets the Response Answer
	 * @param RAnswer
	 */
	public void setResponseAnswer(String RAnswer)
	{
		this.ResponseAnswer = RAnswer;
	}

	/**
	 * Returns just the Body of the Answer
	 * @return The body of the answer (assuming the answer is html)
	 */
	public String returnBody()
	{
		//regular expression - Take just what's inbetween the body tags <BODY></BODY>
		String[] Answerparts = ResponseAnswer.split("</*BODY>");
		if(Answerparts.length == 3){
		return Answerparts[1];
		}else{
			return Answerparts[0];
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ResponseInfo respAnswer = new ResponseInfo();
		String middle = respAnswer.returnBody();
		
		System.out.println(middle);
		
	}

}
