package org.icapture.ED.dataAccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.icapture.ED.TaggingDataSource;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This static class is used to send queries and updates to a SPARQL Endpoint.
 * @author Paul Lu
 *
 */
public class SPARQLEndPointAccess {

	/* CONSTANTS used in queries and updates */
	public static String ns = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/";
	public static String prefix_rdf = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
	public static String prefix_tag = "prefix tag: <http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/>"; 
	public static String prefix_rdfs = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
	public static String prefix_xsd = "prefix xsd: <http://www.w3.org/2001/XMLSchema#>";	
	public static String Nothing_uri = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/Nothing";
	protected static final long serialVersionUID = 1L;
	protected static Logger logger = Logger.getLogger(SPARQLEndPointAccess.class);
	protected static String query_param_name = "query";

	
	/**
	 * Sends what is in the ParameterMap to the SparqlEndpoint url
	 * 
	 * @param ParamMap
	 * 			- Parameter Map containing the query and other parameters for the SPARQLEndpoint
	 * 
	 * @param SparqlEndpoint
	 * 			- The url of the Sparql Endpoint 
	 * 
	 * @return ResponseInfo object containing the SPARQLEndpoint's response
	 */
    public static ResponseInfo sendQueryOrUpdate(Map<String,String> ParamMap, String SparqlEndpoint)
    {
        int RespCode = -1;
        
        ResponseInfo respInfo = null;
        
    	//BUILD URL PARAMETER STRING
        String data = buildParameterDataString(ParamMap);
        
        //Send the Query or Update
        respInfo = send(SparqlEndpoint, data);
        
        //If error
        if(respInfo.getResponseCode() != HttpStatus.SC_OK)
        {
        	System.out.println("CAUSED BY STATEMENT: " + ParamMap.get(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name()) + "\n");
        	System.out.println(ParamMap.toString());
        }
        
        return respInfo;
    }
    
    /**
     * Sends what is in the JSONObject to the SparqlEndpoint url
     * @param Parameters
	 * 			- Parameter JSONObject containing the query and other parameters
	 * 
	 * @param SparqlEndpoint
	 * 			- The url of the Sparql Endpoint
	 * 
     * @return ResponseInfo object containing the SPARQLEndpoint's response
     */
    public static ResponseInfo sendQueryOrUpdate(JSONObject Parameters, String SparqlEndpoint){
        int RespCode = -1;
        
        ResponseInfo respInfo = null;
        
    	//BUILD URL PARAMETER STRING
        String data = null;
		try 
		{
			data = buildParameterDataString(Parameters);
		        //Send the Query or Update
        respInfo = send(SparqlEndpoint, data);
        
	        //If error
	        if(respInfo.getResponseCode() != HttpStatus.SC_OK)
	        {
	        	System.out.println("CAUSED BY STATEMENT: " + Parameters.getString(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name()) + "\n");
	        	System.out.println(Parameters.toString(5));
	        }
        
		} catch (JSONException e) {
			e.printStackTrace();
		}
        
        return respInfo;
    }
    
    /**
     * Establishes a url connection with the SPARQL Endpoint and sends it the data.
     * When response is received it is processed into a ResponseInfo Object and returned
     * 
     * @param SparqlEndpoint url
     * @param data - url paramString for the SPARQLEndpoint 
     * @return ResponseInfo object containing the SPARQLEndpoint's response
     */
    protected static ResponseInfo send(String SparqlEndpoint, String data)
    {
        ResponseInfo respInfo = new ResponseInfo();
        Integer RespCode = -1;
	    //ESTABLISH HTTP URL CONNECTION WITH SPARQL ENDPOINT	
        try {
        	URL url = new URL(SparqlEndpoint);            
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setDoOutput(true);
            
            //TURN OFF CACHING HERE
            if(conn.getUseCaches() != false)
            {
            	conn.setRequestProperty("Cache-Control", "no-cache");
            	conn.setRequestProperty("Pragma", "no-cache");
            }
            
            OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            
            //POST THE PARAMETERS TO THE SPARQL ENDPOINT
            writer.write(data);
            writer.flush();

            //GET THE RESPONSE CODE
            RespCode = conn.getResponseCode();
            respInfo.setResponseCode(RespCode);
        	
	            // print Response Code
	            System.out.println("----------");
	            System.out.println("Sent to: " + SparqlEndpoint + " ResponseCode received: " + conn.getResponseCode());
	            System.out.println("----------");
        	
	          //IF SOMETHING BAD HAPPENS
		            if(RespCode != HttpStatus.SC_OK) 
		            {
		            	String code = "" + RespCode;
		            	
		            	//Record any error information that might be coming back
		            	StringBuffer errorBuffer = new StringBuffer();
		            	
		            	BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		            		String line;
			            	while((line = reader.readLine()) != null)
			            	{
			            		errorBuffer.append(line);
			            	}
			            	String ResponseInfo = "\n SPARQLEndpointAccess: sendQueryOrUpdate \n\n RESPONSE: \n\n" + errorBuffer.toString() + "\n"; 
			            	//Set the responseInfo with the error
			            	respInfo.setResponseAnswer(ResponseInfo);
		            	//close everything and return the responseInfo
			            writer.close();
		            	reader.close();
		            	conn.disconnect();
		            	return respInfo;
		            }
            
            // Get the response
            StringBuffer answer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
	            while ((line = reader.readLine()) != null) 
	            {                
		            	answer.append(line);
	            }
            respInfo.setResponseAnswer(answer.toString());
            writer.close();
            reader.close();
            conn.disconnect();

        } catch (MalformedURLException ex) {
			
			logger.error("MalformedURL: \n" + SparqlEndpoint);
           	ex.printStackTrace();
            
        } catch (IOException ex) {
		
			logger.error("IO ERROR: \n" + ex.getMessage());
            ex.printStackTrace();
        }
       
        return respInfo;
    }
    
    /**
     * Uses Default Parameters and sends the query to the SPARQLEndPoint
     * @param query SPARQL query
     * @return ResponseInfo object containing the SPARQLEndpoint's response
     * @throws JSONException 
     */
    public static ResponseInfo sendQueryOrUpdate(String query)
    {
    	
		JSONObject JSON_SEP_params = null;
		try {
			JSON_SEP_params = new JSONObject(TaggingDataSource.getSPARQL_EP_DATASOURCE_parameters());
			JSON_SEP_params.put("format", "text/html");
			JSON_SEP_params.put(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name(), query);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	
		ResponseInfo respInfo = SPARQLEndPointAccess.sendQueryOrUpdate(JSON_SEP_params, TaggingDataSource.getSPARQL_EP_DATASOURCE_url());
    	
		return respInfo;
    }

    /**
     * Returns a HashMap of the default SPARQL Endpoint parameters
     * @return SPARQL Endpoint Parameters
     */
    public static HashMap<String, String> getCopyOfSEP_ParametersAsHashMap()
    {
    	JSONObject JSON_SEP_params = null;
    	HashMap<String,String> paramMap = new HashMap<String,String>(); 
    	try{
			JSON_SEP_params = new JSONObject(TaggingDataSource.getSPARQL_EP_DATASOURCE_parameters());
			
			for(Iterator<String> i = JSON_SEP_params.keys(); i.hasNext();)
			{
				paramMap.put(i.toString(), JSON_SEP_params.getString(i.toString()));
			}
			
			paramMap.put(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name(), "");
    	} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return paramMap;
    }
    
    /**
     * Returns a JSONObject containing the default SPARQL Endpoint parameters
     * @return SPARQL Endpoint Parameters
     */
    public static JSONObject getCopyOfSEP_ParametersAsJSONObject()
    {
    	JSONObject JSON_SEP_params = null;
    	try{
			JSON_SEP_params = new JSONObject(TaggingDataSource.getSPARQL_EP_DATASOURCE_parameters());
			
			JSON_SEP_params.put(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name(), "");
    	} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	return JSON_SEP_params;
    }
    
    /**
     * Returns the SPARQL Endpoint URL
     * @return a url
     */
    public static String getSPARQLEndPoint_url()
    {
    	return TaggingDataSource.getSPARQL_EP_DATASOURCE_url();
    }
    
    /**
     * Enter your SPARQL query and get back the results returned by the SPARQL Endpoint as a jsonobject 
     * @param query - SPARQL query
     * @return JSONObject of the SPARQL Endpoint results
     */
	public static JSONObject sendQueryGetJSON(String query)
	{
		
		//Create JSON Object with default parameters
		JSONObject JSON_SEP_params = null;
		try 
		{
			JSON_SEP_params = new JSONObject(TaggingDataSource.getSPARQL_EP_DATASOURCE_parameters());
			JSON_SEP_params.put(TaggingDataSource.getSPARQL_EP_DATASOURCE_query_param_name(), query);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		//Get the JSON 
		ResponseInfo respInfo = SPARQLEndPointAccess.sendQueryOrUpdate(JSON_SEP_params, TaggingDataSource.getSPARQL_EP_DATASOURCE_url());
    	
		//Check For Error
		if(respInfo == null || respInfo.getResponseCode() != HttpStatus.SC_OK)
		{
			logger.error("ERROR from SPARQL END POINT: " + respInfo.getResponseAnswer());
			return null;
		}
		
		//Get just the JSONObject results
		JSONObject jsonResults = null;

		try 
		{
			jsonResults = new JSONObject(respInfo.getResponseAnswer());
		} catch (JSONException e) {
			e.printStackTrace();
		}
			
		return jsonResults; 
	}
    	
    
    /**
     * Builds the parameter String for a url from a Map
     * @param ParamMap
     * @return URL Query String (Parameter String)
     */	
    private static String buildParameterDataString(Map<String, String> ParamMap)
    {
    	//BUILD URL PARAMETER STRING
    	StringBuffer data = new StringBuffer();
    	
	    	for(Iterator<String> ikey = ParamMap.keySet().iterator(); ikey.hasNext();)
	    	{
	    		String key = ikey.next();
	    		String value = ParamMap.get(key);
	    		try {
					data.append(URLEncoder.encode(key, "UTF-8") 
								+ "=" 
								+ URLEncoder.encode(value, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();	
					logger.error("SPARQLEndPointAccess: sendQueryOrUpdate: Error encoding parameter string: \n" + data.toString());
				}
	    		
	    		if(ikey.hasNext())
	    		{
	    			data.append("&");
	    		}
	    	}
	    return data.toString();	
    }

    /**
     * Builds a parameter String for a url from a JSONObject
     * @param Parameters
     * @return URL Query String (Parameter String)
     * @throws JSONException
     */
    private static String buildParameterDataString(JSONObject Parameters) throws JSONException
    {
    	//BUILD URL PARAMETER STRING
    	
    	StringBuffer data = new StringBuffer();
    	
	    	for(Iterator<String> ikey = Parameters.keys(); ikey.hasNext();)
	    	{
	    		String key = ikey.next();
	    		String value = Parameters.getString(key);
	    		try {
					data.append(URLEncoder.encode(key, "UTF-8") 
								+ "=" 
								+ URLEncoder.encode(value, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					logger.error("SPARQLEndPointAccess: sendQueryOrUpdate: Error encoding parameter string: \n" + data.toString());	
				}
	    		
	    		if(ikey.hasNext())
	    		{
	    			data.append("&");
	    		}
	    	}
	    return data.toString();	
    }
    
    
    /**
     * Enter your SPARQL query and get back an EDResultSet
     * @param query - SPARQL query 
     * @return EDResultSet Object of the results from the SPARQLEndpoint
     */
	public static EDResultSet sendQueryGetEDResultSet(String query)
	{
		
		JSONObject JSONResults = sendQueryGetJSON(query);
		if(JSONResults == null)
		{
			return null;
		}
			EDResultSet EDRES = new EDResultSet(JSONResults);
			
			return EDRES; 
	}

    
	public static void main(String[] args) {

	}

}
