package org.icapture.ED.dataAccess;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;

/**
 * A ResultSet sepcific to ED built from the results (JSON format) returned by Virtuoso SPARQL Endpoint
 * 
 * @author Paul Lu
 *
 */
public class EDResultSet {
	
	private ArrayList EDResults = new ArrayList();
	private String[] vars = null;
	private Iterator iter = null;
	private JSONObject currResult = null;

	/**
	 * Constructor - Makes an EDResultSet from String representation of JSON Object results 
	 * returned by a Virtuoso SPARQL Endpoint
	 * @param JSON_input String representing results in JSON format
	 */
	public EDResultSet(String JSON_input)
	{
		try {
			
			JSONObject ResultSet = new JSONObject(JSON_input); //JSONObject from String
			// JSON HEAD
			JSONObject Head = ResultSet.getJSONObject("head");
			// JSON VARIABLES - ie Column Names
			JSONArray JSON_vars = Head.getJSONArray("vars");
			vars = JSONArraytoStringArray(JSON_vars);
			//GET THE RESULTS
			JSONObject Results = ResultSet.getJSONObject("results");
			//GET THE RESULTS BINDINGS - Array of actual results (ie rows)
			JSONArray bindings = Results.getJSONArray("bindings");
			
			//put each binding into EDResults		
			for(int i=0 ; i < bindings.length() ; i++)
			{
				EDResults.add(bindings.getJSONObject(i));			
			}
			
			iter = EDResults.iterator();
			
		} catch (JSONException e) {
			System.err.print("ERROR IN EDResultSet Constructor \n" );
			e.printStackTrace();
			
		}
	
	}
	
	/**
	 * Constructor - Makes an EDResultSet from a JSON Object
	 * @param ResultSet - JSONObject of the results returned by Virtuoso SPARQL Endpoint
	 */
	public EDResultSet(JSONObject ResultSet)
	{
		try {
			// JSON HEAD
			JSONObject Head = ResultSet.getJSONObject("head");
			// JSON VARIABLES - ie Column Names
			JSONArray JSON_vars = Head.getJSONArray("vars");
			vars = JSONArraytoStringArray(JSON_vars);
			//GET THE RESULTS
			JSONObject Results = ResultSet.getJSONObject("results");
			//GET THE RESULTS BINDINGS - Array of actual results (ie rows)
			JSONArray bindings = Results.getJSONArray("bindings");
			
			//put each binding into EDResults		
			for(int i=0 ; i < bindings.length() ; i++)
			{
				EDResults.add(bindings.getJSONObject(i));			
			}
			
			iter = EDResults.iterator();
			
		} catch (JSONException e) {
			System.err.print("ERROR IN EDResultSet Constructor \n" );
			e.printStackTrace();		
		}
	}
	
	/**
	 * Takes a JSONArray and (assuming it's composed of Strings), returns the contents
	 * of the JSONArray as a normal String Array
	 * @param JSON_vars
	 * @return String Array 
	 * @throws JSONException
	 */
	private String[] JSONArraytoStringArray(JSONArray JSON_vars) throws JSONException
	{
		String[] temp_varArray = new String[JSON_vars.length()];
		for(int i=0 ; i < JSON_vars.length() ; i++)
		{
			temp_varArray[i] = JSON_vars.getString(i);
		}
		
		return temp_varArray;
	}
	
	/**
	 * Returns the Names of the (Parameters) Variables of the ResultSet
	 * ie. Column Names
	 * @return String[] of the parameter names of the Results
	 */
	public String[] getParameterNames()
	{
		return vars;	
	}
	
	/**
	 * Returns length of EDResultSet
	 * @return number of Results in ResultSet
	 */
	public int Length()
	{
		return EDResults.size();
	}
	
	/**
	 * Returns true if there is another object in the list
	 * @return true if there is another object in the list, false otherwise
	 */
	public boolean hasNext()
	{
		return iter.hasNext();
	}
	
	/**
	 * Moves the iterator up by 1 
	 */	public void next()
	{
		currResult = (JSONObject)iter.next();	
	}
	
	/**
	 * Gets the value associated with the input ParameterName for the current object
	 * as an Integer
	 * @param ParamName
	 * @return Integer value associated with the ParamName
	 * @throws JSONException
	 */
	public Integer getInt(String ParamName) throws JSONException
	{
		if(currResult != null)
		{
			JSONObject curr = currResult.getJSONObject(ParamName);
			return curr.getInt("value");
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the value associated with the input ParameterName for the current object
	 * as a String
	 * @param ParamName
	 * @return String value associated with the ParamName
	 * @throws JSONException
	 */
	public String getString(String ParamName) throws JSONException
	{
		if(currResult != null)
		{
			JSONObject curr = currResult.getJSONObject(ParamName);
			return curr.getString("value");
		}else{
			return null;
		}
	}
	
	/**
	 * Get the Type associated with the input ParameterName for the current object
	 * as a String
	 * @param ParamName
	 * @return Type 
	 * @throws JSONException
	 */
	public String getType(String ParamName) throws JSONException
	{
		if(currResult != null)
		{
			JSONObject curr = currResult.getJSONObject(ParamName);
			return curr.getString("type");
		}else{
			return null;
		}
	}
	
	/**
	 * Gets the DataType associated with the input ParamName for the current object
	 * as a String
	 * @param ParamName
	 * @return DataType
	 */
	public String getDataType(String ParamName)
	{
		if(currResult != null)
		{
			JSONObject curr;
			try {
				curr = currResult.getJSONObject(ParamName);
				String dtype = curr.getString("datatype");
				return dtype;
			} catch (JSONException e) {
				
				return null;
			}
			
		}else{
			return null;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

		    
}
