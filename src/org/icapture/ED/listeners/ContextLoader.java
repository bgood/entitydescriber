package org.icapture.ED.listeners;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.icapture.ED.TaggingDataSource;
import org.icapture.ED.TaggingDataStore;
import org.icapture.ED.TemplateJavascriptProcessor;
import org.icapture.ED.config.EdConfig;

/**
 * Sets the context of how ED is going to be used after being deployed ie. the
 * Data Source, the SPARQLEndpoint, etc.
 * 
 * @author Ed Kawas
 * 
 */
public class ContextLoader implements ServletContextListener {

    public static String PATCHED_FILE_PATH = "";
    public static TaggingDataStore dataStore = null;
    public static TaggingDataSource dataSource = null;

    /**
     * Destroys the patched ed_connotea.js file upon destruction of context
     * @param event - ServletContextEvent
     */
    public void contextDestroyed(ServletContextEvent event) {
	try {
	    String path = PATCHED_FILE_PATH;
	    if (path != null && !path.trim().equals("")) {
		File f = new File(path);
		if (f.isFile())
		    f.delete();
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Called when the web application is deployed and sets the context of how
     * ED is being used based on the values in the datasource.properties file
     * 
     * @param event - ServletContextEvent
     */
    public void contextInitialized(ServletContextEvent event) {
	ServletContext servletContext = event.getServletContext();
	TaggingDataSource dataSource = new TaggingDataSource();
	TaggingDataStore dataStore = new TaggingDataStore();

	try {

	    Properties properties = EdConfig.DATA_SOURCES_PROPERTIES;

	    // start setting datasource file... default is what is in TaggingDataSource
	    dataSource
		    .setService_url(properties.getProperty(
			    TaggingDataSource.SERVICE_URL, dataSource
				    .getService_url()));
	    dataSource.setAc_param(properties.getProperty(
		    TaggingDataSource.AC_PARAM, dataSource.getAc_param()));
	    dataSource.setAc_path(properties.getProperty(
		    TaggingDataSource.AC_PATH, dataSource.getAc_path()));
	    dataSource.setDefault_type(properties.getProperty(
		    TaggingDataSource.DEFAULT_TYPE, dataSource
			    .getDefault_type()));
	    dataSource.setAc_qstr(properties.getProperty(
		    TaggingDataSource.AC_QSTR, dataSource.getAc_qstr()));
	    dataSource
		    .setBlurb_param(properties.getProperty(
			    TaggingDataSource.BLURB_PARAM, dataSource
				    .getBlurb_param()));
	    dataSource.setBlurb_path(properties.getProperty(
		    TaggingDataSource.BLURB_PATH, dataSource.getBlurb_path()));
	    dataSource.setThumbnail_path(properties.getProperty(
		    TaggingDataSource.THUMBNAIL_PATH, dataSource
			    .getThumbnail_path()));

	    org.icapture.ED.TaggingDataSource.setSPARQL_EP_DATASOURCE_url(properties.getProperty(
	    		TaggingDataSource.SPARQL_EP_DATASOURCE_URL, dataSource.getSPARQL_EP_DATASOURCE_url()));

	    org.icapture.ED.TaggingDataSource.setSPARQL_EP_DATASOURCE_query_param_name(properties.getProperty(
	    		TaggingDataSource.SPARQL_EP_DATASOURCE_QUERY_PARAM_NAME, dataSource.getSPARQL_EP_DATASOURCE_query_param_name()));

	    org.icapture.ED.TaggingDataSource.setSPARQL_EP_DATASOURCE_parameters(properties.getProperty(
	    		TaggingDataSource.SPARQL_EP_DATASOURCE_PARAMETERS, dataSource.getSPARQL_EP_DATASOURCE_parameters()));


	    properties.list(System.out);
	    
	    properties = EdConfig.DATA_STORES_PROPERTIES;

	    dataStore
	    .setSPARQL_EP_DATASTORE_url(properties.getProperty(
	    		TaggingDataStore.SPARQL_EP_DATASTORE_URL, dataStore.getSPARQL_EP_DATASTORE_url()));

	    dataStore
	    .setSPARQL_EP_DATASTORE_query_param_name(properties.getProperty(
	    		TaggingDataStore.SPARQL_EP_DATASTORE_QUERY_PARAM_NAME, dataStore.getSPARQL_EP_DATASTORE_query_param_name()));

	    dataStore
	    .setSPARQL_EP_DATASTORE_parameters(properties.getProperty(
	    		TaggingDataStore.SPARQL_EP_DATASTORE_PARAMETERS, dataStore.getSPARQL_EP_DATASTORE_parameters()));

	    
	} catch (Exception ne) {
	    // using freebase and not custom data source
	    System.out
		    .println("No Properties loaded from Properties file - using default values ");
	}
	String templateFilePath = servletContext
		.getRealPath("/js/ed_properties.js.template");
	PATCHED_FILE_PATH = servletContext.getRealPath("/js/")
		+ System.getProperty("file.separator")
		+ "ed_properties_patched.js";
	TemplateJavascriptProcessor JavascriptProcessor = new TemplateJavascriptProcessor(dataSource, dataStore);
	JavascriptProcessor.process(templateFilePath, PATCHED_FILE_PATH);
	System.out.println(PATCHED_FILE_PATH);
    }

}
