package org.icapture.ED.tag;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Reads the session variables set by SPARQLTaggingServlet: tagsCompleted, tagsTotal
 * Calculates Progress from 1 to 10
 * Sends back JSON containing the above information with the keys: tagged, total, progress
 * @author Paul Lu
 *
 */
public class TaggingProgressServlet extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * Get the session attributes tagsCompleted and tagsTotal to calculate the 
	 * progress and send back json with that information
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		
        HttpSession session = request.getSession(true);	
        Integer tagsCompleted = (Integer) request.getSession().getAttribute("tagsCompleted");
        
        Integer tagsTotal = (Integer) request.getSession().getAttribute("tagsTotal");
        String callback = request.getParameter("callback");
        
        Float progressTen = null;
        Integer progTen = 0;
        if(tagsCompleted != null && tagsTotal != null)
        {
        //1-10 How far completed is it?
        progressTen = (tagsCompleted.floatValue()/tagsTotal.floatValue())*100;
        progTen = progressTen.intValue();
        }else{
        	tagsCompleted = 0;
        	tagsTotal = 0;
        }
        //Create JSON - fill with values and send back the data
        JSONObject jsonProgress = new JSONObject();
        System.out.println("PROGRESSBAR value calculated from session variables: " + progTen);
        try {
			jsonProgress.put("tagged", tagsCompleted);
			jsonProgress.put("total", tagsTotal);
			jsonProgress.put("progress", progTen);
			
	        out.print(callback + "(" + jsonProgress.toString(5) + ")");

        } catch (JSONException e) {
			e.printStackTrace();
		}
        
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * Calls the doGet method
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

			doGet(request, response);
	}

}
