package org.icapture.ED.tag;

/**
 * Specialized exception for Tagging class
 * @author Ben Good
 *
 */
public class TaggingException extends Exception {

	public TaggingException() {
		super();
	}
	public TaggingException(String s) {
		super(s);
	}
}
