package org.icapture.ED.tag;

/**
 * Structure for containing values of a Freebase Type object
 * @author Ben Good
 *
 */
public class Ftype {
	public String id;
	public String name;
	
}
