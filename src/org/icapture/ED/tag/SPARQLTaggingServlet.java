package org.icapture.ED.tag;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.commons.httpclient.HttpStatus;
import org.icapture.ED.dataAccess.ResponseInfo;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Acts as a servlet wrapper for SPARQLTagManager
 * @author Paul Lu
 *
 */
public class SPARQLTaggingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("SPARQLTaggingServlet");
	private String date;
	
	/**
	 * Instantiates a SPARQLTagManager to process the tagging object and submit an insert into 
	 * a SPARQL Endpoint.  Returns the response html and response code that the SPARQLTagManager 
	 * gets from the SPARQL Endpoint that the TagManager is inserting to.
	 * 
	 *  
	 * @param request -
	 * 					the request send by the client to the server
	 * @param response -
	 * 					the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//callback and semanticLink variables
		String callback = null;
		String resource = null;
		
		HashMap ParameterMap = new HashMap(); //Parameters for SPARQLEndpoint
		
		//Get Session and create Session Variable for Progress Bar - reset values to 0
			HttpSession sess = request.getSession();		
				sess.setAttribute("tagsCompleted", 0);
				sess.setAttribute("tagsTotal", 0);
		
		//put all the request parameters into the ParameterMap
		Enumeration<String> param_names = request.getParameterNames();
		    while (param_names.hasMoreElements()) 
		    {	
				String name = param_names.nextElement();
				ParameterMap.put(name, request.getParameter(name));
		    }
	    
		String tagging = (String) ParameterMap.get("tagging");
		ParameterMap.remove("tagging");
		
		//set callback parameter if it exists
			if(request.getParameter("callback") != null)
			{
				callback = (String) request.getParameter("callback");
				ParameterMap.remove("callback");
				ParameterMap.remove("_");
			}
		
		//set resource parameter if it exists
			if(request.getParameter("resource") != null)
			{		
				resource = (String) request.getParameter("resource");
				ParameterMap.remove("resource");
			}

		int ResponseCode = -1; //initialize ResponseCode
		String ResponseBody  = null;
		ResponseInfo respHTML = null;

		//TAGGING XML IS NULL		
		if (tagging == null) 
		{
			// error condition -
			logger.error("tagging = null");
			returnJSONresponse(response, callback, ResponseCode, resource);
			return;
		} else{
			
			//DO THE BUISNESS:   		
			try {
					Tagging event = new Tagging(tagging); //build a Tagging object
					sess.setAttribute("tagsTotal", event.tags.size() + 1); //set total tags (+1 for user insert information)
					
					 SPARQLTagManager StM = null;
					 /**Specific SPARQL Endpoint being used**/ 
					if(!ParameterMap.isEmpty() && ParameterMap.get("SPARQL_EP") != null)
					{
				        String SPARQLEndPoint = (String) ParameterMap.get("SPARQL_EP");
				       	ParameterMap.remove("SPARQL_EP");
				        
			        	String query_param_name = (String) ParameterMap.get("query_param_name");
			        	ParameterMap.remove("query_param_name");
				   
			        	StM = new SPARQLTagManager(ParameterMap, SPARQLEndPoint, query_param_name, sess);
					}else{
							StM = new SPARQLTagManager(sess); 		//Default SPARQL Endpoint being used
						 }
					
			        date = StM.getZuluTime(event.getTag_moment()); //get the date incase there's an error
			        
			        //ACTUAL submission of Tags	to the SPARQL End Point		        
			        respHTML = StM.addTagEvent(event, tagging); 

			        //IF BAD RESPONSE CODE			        
			        if(respHTML.getResponseCode() != HttpStatus.SC_OK) //error
			        {
			        	logger.error("error saving tagging SPARQLTaggingServlet - Bad Response Code \n" + respHTML.getResponseCode() + "\n TAGGING: \n" + tagging+"\n");
			        	returnJSONresponse(response, callback, respHTML.getResponseCode(), resource); //send back errorJSON
				        	
			        }
		        
			} catch (Exception e) {
				//EXCEPTION DURING SUBMISSION OF TAGS TO SPARQL END POINT				
				logger.error("error saving tagging SPARQLTaggingServlet \n"+tagging+"\n ERROR TRACE: \n" + e.getMessage() + e.getLocalizedMessage() + e.getStackTrace());
					String rcode = "-1";
					String extra_info = "";
						
						if(respHTML != null)
						{
							rcode = String.valueOf(respHTML.getResponseCode());
							extra_info = respHTML.getResponseAnswer();	
						}
			        	returnJSONresponse(response, callback, Integer.valueOf(rcode), resource);
										
				return;
			}
		}
		
		//IF THE RESPONSE IS NOT NULL
		if(respHTML != null)
		{
			ResponseCode = respHTML.getResponseCode();
			ResponseBody = respHTML.returnBody();
		}else
		{
			logger.error("No Response from SPARQLTagManager \n" + tagging);
			returnJSONresponse(response, callback, ResponseCode, resource);

			return;
		}
		//ALL GOOD - SEND 200 RESPONSE 		
		logger.info("Response Code returned by SPARQLTagManager: " + ResponseCode);
		returnJSONresponse(response, callback, ResponseCode, resource);

	}

	
	/**
	 * Turns the response into a JSON format and returns it to the caller
	 * @param response
	 * @param callback
	 * @param ResponseCode
	 * @param resource 
	 */
	public void returnJSONresponse(HttpServletResponse response, String callback, int ResponseCode, String resource) 
	{
		logger.info("RETURNING JSON WITH RESPONSE CODE: " + ResponseCode);
		response.setContentType("text/plain");
		PrintWriter out;
		try 
		{
			JSONObject returnResponse = new JSONObject();
			JSONObject resp = new JSONObject();
			try 
			{
				resp.put("code", ResponseCode);
				resp.put("resource", resource);
				returnResponse.put("response", resp);
				
					out = response.getWriter();
					out.println(callback +"(" + returnResponse.toString(5) + ")");
				out.flush();
				out.close();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Redirects to doGet
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
		
	}
}
