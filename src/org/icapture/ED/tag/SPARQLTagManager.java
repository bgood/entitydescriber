package org.icapture.ED.tag;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.icapture.ED.dataAccess.ResponseInfo;
import org.icapture.ED.dataAccess.SPARQLEndPointAccess;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

/**
 * SPARQL/SPARUL Tag Manager 
 * Used to convert a Tagging object to SPARQL insert statements and 
 * submits them to the assigned URL for submission into Database
 * 
 * Calling the constructor with no parameters assumes the SPARQLEndpoint is "http://biomoby.elmonline.ca/sparql"
 * with the Parameters that Endpoint uses
 * 
 * To specify a different SPARQLEndpoint, call the constructor with parameters.
 * This allows you to specify the URL of the SPARQLEndpoint and the Parameters which it expects.
 * 
 * @author Paul Lu
 *
 */
public class SPARQLTagManager{

	//used in combining statements
	protected String tagging_entry = null;
	protected String user_entry = null;
	protected ArrayList Tags = new ArrayList();
	protected String DTG = ""; //Date Time Group
	protected String tagging_xml = ""; // xml from which the Tagging object is derived
	
	//used in SendQueryOrUpdate
	protected HashMap ParameterMap = null;
	protected String query_param_name = "query";
	protected String SPARQLEndPoint = "";
	
	//for sending emails to developer email if there is an error
	protected HttpSession session = null;
	
	//PREFIXES
	protected String ns = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/";
	protected String prefix_rdf = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>";
	protected String prefix_tag = "prefix tag: <http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/>"; 
	protected String prefix_rdfs = "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>";
	protected String prefix_xsd = "prefix xsd: <http://www.w3.org/2001/XMLSchema#>";	
	protected String Nothing_uri = "http://bioinfo.icapture.ubc.ca/Resources/SemanticTagging/Nothing";
	
	//used for making hashcodes
	protected Integer taggingHash = null;
	protected static MessageDigest md = null;
	private static Hex HexEncoder = new Hex();
	private URLDecoder decoder = new URLDecoder();

	//logger stuff
	protected static final long serialVersionUID = 1L;
	protected static Logger logger = Logger.getLogger(SPARQLTagManager.class);
	
	
	/**
	 * Default Constructor
	 */
		public SPARQLTagManager(HttpSession session)
		{
			//Parameters other than the query that are required for the submission
			HashMap ParameterMap = new HashMap();
			ParameterMap.put("default-graph-uri", "sandbox");
			ParameterMap.put("should-sponge", "");
			ParameterMap.put("format", "text/html");
			ParameterMap.put("debug", "on");
			this.ParameterMap = ParameterMap;
			this.SPARQLEndPoint = "http://biomoby.elmonline.ca/sparql";
			this.query_param_name = "query";
			this.session = session;
			
		}
		
	/**
	 * Costructor 
	 * @param ParameterMap
	 * 				- Parameter Map containing all the necessary parameters for the SPARQL Endpoint and the query/update statment
	 * @param SPARQLEndpoint
	 * 				- URL of the SPARQL Endpoint
	 * @param query_param_name
	 * 				- The name (name value pair) of the query parameter 
	 * @param session
	 * 				- The session calling these methods (used for updating progress)
	 */	
		public SPARQLTagManager(HashMap ParameterMap, String SPARQLEndpoint, String query_param_name, HttpSession session)
		{
			this.ParameterMap = ParameterMap;
			this.query_param_name = query_param_name;
			this.SPARQLEndPoint = SPARQLEndpoint;
			this.session = session;			
		}
	
		/**
		 * Takes the Tagging object and forms the Parameters and submits them to the SPARQL/SPARUL Database
		 * Returns true if the submission is successful, false otherwise.
		 * @param tagging
		 * @return ResponseInfo object containing response code and response content
		 * @throws UnsupportedEncodingException 
		 * @throws NoSuchAlgorithmException 
		 */
		public ResponseInfo addTagEvent(Tagging tagging, String tagging_xml) throws UnsupportedEncodingException, NoSuchAlgorithmException
		{
			this.tagging_xml = tagging_xml;
			/**Make all the necessary Insert statments and put them in an array List - loop through and execute**/
			ArrayList insert_statements = new ArrayList();

			/**CREATE INSERT STATEMENT FOR TAGGING AND USER AND ADD TO INSERT STATEMENT ARRAY LIST**/					
			StringBuffer tag_user_stmt =  new StringBuffer();
			ResponseInfo resp = null;
			//create Tagging statement body
			String taggingInsert = createTaggingInsert(tagging);
			
			//create User statement body
			String userInsert = createUserInsert(tagging);
			
			//link together Tagging and User statement bodies for first insert Statement
			tag_user_stmt.append(taggingInsert 
									+ "\n" 
									+ userInsert 
									+ "\n");
				// add taggedBy statement
				tag_user_stmt.append(tagging_entry 
										+ " tag:taggedBy "
										+ user_entry);
			
			//make INSERT statement
				String tag_userInsert = completeInsert(tag_user_stmt.toString());
			
			//add Insert Statement to ArrayList of Insert Statements
				insert_statements.add(tag_userInsert);
				
			/**CREATE INSERT STATEMENTS FOR TAGS AND ADD TO INSERT STATEMENT ARRAY LIST**/			
			//get the tags
			List tag_list = tagging.getTags();
			int tag_num = tag_list.size();
			//calculate hash
				md = MessageDigest.getInstance("SHA");
			//Clear Tags ArrayList just in case
				Tags.clear();

			Calendar c = new GregorianCalendar();
			Date trialDate = new Date();
			c.setTime(trialDate);
			
			//Integer tagHash = null;
			for(Iterator i = tag_list.iterator(); i.hasNext();)
			{
				Tag t = (Tag)i.next();
			
				String typed_tag = t.tag;
				String public_tag = t.uri;
				String the_tag = "";
				String value_for_hashing ="";
				
				//make tag uid
				if(public_tag != null && public_tag.compareTo(SPARQLEndPointAccess.ns + "Nothing") != 0)
				{
					//create the value to be hashed to make tag uid
					value_for_hashing = public_tag + tagging.agent + taggingHash + getZuluTime(c) + System.currentTimeMillis();
					
				}else{
					value_for_hashing = typed_tag + tagging.agent + taggingHash + getZuluTime(c) + System.currentTimeMillis();
				}
				
				byte[] val_inBytes = value_for_hashing.getBytes(); //get bytes of value
				md.update(val_inBytes);
				String tagHash = String.valueOf(HexEncoder.encodeHex(md.digest()));
				t.tagUID = tagHash;
				the_tag = URLEncoder.encode(FilterOutReservedChars(typed_tag), "UTF-8"); //watch out for this statement
				String c_user = tagging.agent;
				
				String tag_entry = "<" + ns + tagHash + ">";
				
				StringBuffer tag_stmt = new StringBuffer();
				tag_stmt.append(createTagInsert(t, tagging) + "\n");
				tag_stmt.append(tagging_entry
								+ " tag:associatedTag "
								+ tag_entry);
				
				String tagInsert = completeInsert(tag_stmt.toString());
				
				//add Insert Statement to ArrayList of Insert Statements
				insert_statements.add(tagInsert);
			}
			
			
			session.setAttribute("tagsTotal", insert_statements.size());
			
			Iterator insertIter = insert_statements.iterator();
			int respCode = HttpStatus.SC_OK;
			
			String statement = null;

			//do all the inserting here
			while(insertIter.hasNext() && respCode == HttpStatus.SC_OK)
			{	
				statement = (String)insertIter.next();
				ParameterMap.remove(query_param_name); //remove existing query
				ParameterMap.put(query_param_name, statement);
				
				//send
				Integer progress = (Integer)session.getAttribute("tagsCompleted") + 1;
				System.out.println("PROGRESS: " + progress);
				session.setAttribute("tagsCompleted", progress);
					
				resp = SPARQLEndPointAccess.sendQueryOrUpdate(ParameterMap, SPARQLEndPoint);
				respCode = resp.getResponseCode();
			}
			
			//clean up session variables
			session.setAttribute("tagsCompleted", 0);
			session.setAttribute("tagsTotal", 0);
			
			if(respCode != HttpStatus.SC_OK)
			{
				System.err.println("RESPONSE CODE: " + respCode);
				System.err.println("Stopped at Statement: ");
				System.err.println(statement);
				logger.error("ERROR RESPONSE CODE: \n\n" + respCode);
				logger.error("ERROR CAUSED BY INSERT STATEMENT: \n\n" + statement);			
				return resp;
			}
			return resp;
		}

	/**
	 * creates A String that is the SPARUL insert statement body for 
	 * a Tagging resource
	 * 
	 * Prefixes not included 
	 * @param tagging 
	 * @return String - SPARQUL insert statement
	 * @throws UnsupportedEncodingException 
	 */
	private String createTaggingInsert(Tagging tagging) throws UnsupportedEncodingException
	{
		StringBuffer insert_stmt = new StringBuffer();
		//create hash
		
		String tagging_uri = tagging.agent + tagging.resource_tagged + tagging.tag_moment; //input for hash
		taggingHash = tagging_uri.hashCode();
		tagging_entry = "<" + ns + taggingHash + ">"; //<tagging>
		String tagging_type = "<" + ns + "Tagging>";
		
		//create Tagging type Tagging
		insert_stmt.append(tagging_entry
							+ " rdf:type "
							+ tagging_type +"; ");
				insert_stmt.append("\n");
		
		//taggedOn time 
				
				DTG = getZuluTime(tagging.tag_moment);
		String timetagged = "\"" + DTG + "\"";
		insert_stmt.append("tag:taggedOn " 
							+ timetagged + "^^xsd:dateTime; ");
		insert_stmt.append("\n");
		
		//taggedWithEDVersion version
		insert_stmt.append("tag:taggedWithEDVersion " 
							+ "\"" + String.valueOf(tagging.EDVersion) + "\"" + "^^xsd:float; ");
		insert_stmt.append("\n");
		
		//taggedResource resource
		String resource = "<" + tagging.resource_tagged + ">";
		insert_stmt.append("tag:taggedResource "
							+ resource);

		insert_stmt.append(" . ");
		return insert_stmt.toString();
	}
	
	/**
	 * creates A String that is the SPARUL insert statement body 
	 * for a User resource
	 * @param tagging
	 * @return String
	 * @throws UnsupportedEncodingException 
	 */
	private String createUserInsert(Tagging tagging) throws UnsupportedEncodingException
	{
			
		StringBuffer insert_stmt = new StringBuffer();
		//String connotea_agent = tagging.connotea_agent;
		/**CHANGE**/
		//String c_agent = URLEncoder.encode(tagging.agent, "UTF-8");
		String c_agent = tagging.agent;
		user_entry = "<" + ns + c_agent + ">";
		String user_type = "<" + ns + "User" + ">";
		
		//create User
		insert_stmt.append(user_entry
							+ " rdf:type "
							+ user_type + "; ");
		
		insert_stmt.append("\n");
		//append label
		String label = "\"" + c_agent + "\"";
		
		insert_stmt.append("rdfs:label "
							+ label + "@EN ");
		
		//append seeAlso
		if(tagging.connotea_agent != null)
		{
			String connotea_agent_uri = "<" + tagging.connotea_agent + ">";	

			insert_stmt.append("; \n");
			insert_stmt.append("rdfs:seeAlso "
					+ connotea_agent_uri + ". ");
		}else{
				insert_stmt.append(". \n");
			 }

		return insert_stmt.toString();
	}
	
	/**
	 * creates A String that is the SPARUL insert statement body for a Tag resource
	 * @param tag
	 * @return String
	 * @throws UnsupportedEncodingException 
	 */
	private String createTagInsert(Tag tag, Tagging tagging) throws UnsupportedEncodingException
	{
		StringBuffer insert_stmt = new StringBuffer();	
		//String typed_tag = tag.tag; //name of the tag
		String typed_tag = decoder.decode(tag.tag, "UTF-8");
		
		String public_tag = tag.uri; //topic
		String ontology = tag.ontology; //freebase ontology
		
		List<String> aliases = tag.alias;
		String tagUID = tag.tagUID;
		
		//ftypes
		String the_tag = "";
		
		//regex typed_tag here and make it 'the_tag'
		
		//has to be encoded in this one to eliminate spaces in the resulting uri it is used in
		the_tag = URLEncoder.encode(FilterOutReservedChars(typed_tag), "UTF-8");
		String c_user = tagging.agent;
		String tag_entry = "<" + ns + tagUID + ">";
		String tag_type = "<" + ns + "Tag" + ">";
		
		//Add tag_entry to array list for later
		Tags.add(tag_entry);
		
		//create Tag
		insert_stmt.append(tag_entry
							+ " rdf:type "
							+ tag_type );
		
		//SeeAlso URI
		if(tagging.connotea_agent != null)
		{
			insert_stmt.append("; \n");
			String connotea_tag_uri = "<http://www.connotea.org/data/user/" + c_user + "/tag/" + the_tag + ">";
			insert_stmt.append("rdfs:seeAlso "
								+ connotea_tag_uri);
		}
		
		//hasOntology
		if(ontology != null  && ontology.compareToIgnoreCase("") != 0)
		{
			insert_stmt.append ("; \n");
			String ont = "<" + ontology + ">";
			insert_stmt.append("tag:hasOntology "
							+ ont);
		}
		
		//Add Tag Label
		if(typed_tag != null && typed_tag.compareToIgnoreCase("") != 0)
		{
			insert_stmt.append("; \n");
			String tag_label = "\"" + typed_tag + "\"";
			insert_stmt.append("rdfs:label "
								+ tag_label + "@EN");
		}
		
		//Add Topic
		if(public_tag != null && public_tag.compareToIgnoreCase("") != 0)
		{
				insert_stmt.append(" . \n");
				String topic = "<" + public_tag + ">";
				//add Topic Label
				String label = "\"" + typed_tag + "\"";
				String label_literal = label +"@EN";
				
				if(public_tag.compareTo(Nothing_uri) != 0)
				{
					insert_stmt.append(topic
									+ " rdfs:label "
									+ label_literal);
				
					//attach aliases if the topic has some
					if(aliases != null)
					{
						Iterator<String> aIt = aliases.iterator(); 
							String alias = null;
							String alias_literal = null;
								
								while(aIt.hasNext()) //for each alias
								{
									insert_stmt.append(" . \n");
									alias = "\"" + aIt.next() + "\"";
									alias_literal = alias + "@EN";
									insert_stmt.append(topic + " rdfs:label " + alias_literal);
								}		
					}		
					
					insert_stmt.append(" . \n");
					
					if(tag.ftypes!= null)
					{
	
						for(Ftype ftype : tag.ftypes)
						{
							String FBtype_insert = createFreeBaseTypeInsert(ftype);
							//create  FreebaseType type FreebaseType
							insert_stmt.append(FBtype_insert
												+ " . " + "\n");
							
							//topic hasFreebaseType FreebaseType
							insert_stmt.append(topic 
												+ " tag:hasFreebaseType " 
												+ "<http://www.freebase.com/view"+ftype.id + "> ." + "\n");		
						}
					}
				}
		
		//isDefinedBy topic
		insert_stmt.append(tag_entry
							+ " rdfs:isDefinedBy "
							+ topic);
		}
		
		insert_stmt.append(" . ");
		return insert_stmt.toString();
	}
	
	/**
	 * Create the insert statement body for making a FreeBaseType 
	 * in case it doesn't already exist
	 * @param ftype
	 * @return String - Insert statement
	 */
	private String createFreeBaseTypeInsert(Ftype ftype)
	{
		StringBuffer insert_stmt = new StringBuffer();
		String freeBaseType = "<http://www.freebase.com/view"+ftype.id + ">";
		String type = "<" + ns + "FreebaseType" + ">";
		//create type
		insert_stmt.append(freeBaseType 
							+ " rdf:type " 
							+ type + "; \n");
		
		//add type label
		String label = "\"" + ftype.name + "\"";
		String label_literal = label +"@EN";
		insert_stmt.append("rdfs:label "
							+ label_literal);
		
		return insert_stmt.toString();
	}
	
	/**
	 * Adds on prefixes and wraps the statement in a SPARUL Insert statement
	 * @param insert
	 * @return A completed Insert statement
	 */
	private String completeInsert(String insert)
	{
		StringBuffer Insert = new StringBuffer();
		
		Insert.append(prefix_rdf + "\n" //add prefixes
							+ prefix_tag + "\n"
							+ prefix_rdfs + "\n"
							+ prefix_xsd + "\n"
							+ "INSERT{" + "\n" //add insert statement
							+ insert + "\n"
							+ "}");
		
		return Insert.toString();
	}
	
		
	/**
	 * Returns the current DTG of a Calendar object as a String. 
	 * format: yyyy-mm-ddThh:min:msZ
	 * @param cal
	 * @return String representing the Date Time Group
	 */
	public String getZuluTime(Calendar cal) 
	{
		TimeZone tz = TimeZone.getTimeZone("GMT");
		Date dt = cal.getTime();
		
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'000Z'");
		formatter.setTimeZone(tz);
		//FieldPosition fp = DateFormat.Field;
		String DTG = formatter.format(dt);
			return DTG;
	}

    /**
     * Uses regex to filter out the Connotea reserved characters
     * @param word
     * @return String with reserved characters filtered out
     */
    public String FilterOutReservedChars(String word)
    {
    	String reappended_word = "";
    	String[] word_split = word.split("[+/]");
    	reappended_word = word_split[0];
    	for(int i=1 ; i < word_split.length ; i++)
    	{
    		reappended_word = reappended_word + " " + word_split[i];
    	}
    	
    	String aposreappended_word = "";
    	String[] aposword_split = reappended_word.split("[']");
    	for(int i=0 ; i < aposword_split.length ; i++)
    	{
    		aposreappended_word = aposreappended_word + aposword_split[i];
    	}

    	
    	return aposreappended_word;
    }
	
	/**
	 * USED for Function Testing
	 * @param args
	 * @throws TaggingException 
	 */
	public static void main(String[] args) {
		
//		SPARQLTagManager STM = new SPARQLTagManager();
/**TESTING RESERVED WORD FILTER**/
//		String Parkinsons = "Parkinson's Disease";
//		String HS = "Hack/Slash";
//		String Springer = "Springer+Business";
		
//		System.out.println(STM.FilterOutReservedChars(Parkinsons));
//		System.out.println(STM.FilterOutReservedChars(HS));
//		System.out.println(STM.FilterOutReservedChars(Springer));
		// TODO Auto-generated method stub
//		
//		/**Read in xml for use of tag**/
//		StringBuffer xmlBuffer = new StringBuffer();
//		
//		//String apo_error = "http%3A//keet.wordpress.com/2008/07/09/bfo%u2019s-specific-and-generic-dependence-and-generalising-progress-in-essential-and-mandatory-parts/";
//		//URLDecoder decoderRing = new URLDecoder();
////		try {
////			decoderRing.decode(apo_error, "UTF-8");
////		} catch (UnsupportedEncodingException e2) {
////			System.err.println("ERROR DECODING: \n");
////			e2.printStackTrace();
////		}
//		try{
//		    // Open the file that is the first 
//		    // command line parameter
//		    FileInputStream fstream = new FileInputStream("/Users/Lu_P/workspace_ED/ED/apostrophe_error.xml");
//		    // Get the object of DataInputStream
//		    DataInputStream in = new DataInputStream(fstream);
//		        BufferedReader br = new BufferedReader(new InputStreamReader(in));
//		    String strLine;
//		    //Read File Line By Line
//		    while ((strLine = br.readLine()) != null)   {
//		      // Print the content on the console
//		      System.out.println (strLine);
//		      //append
//		      xmlBuffer.append(strLine + "\n");
//		      
//		    }
//		    //Close the input stream
//		    in.close();
//		    }catch (Exception e){//Catch exception if any
//		      System.err.println("Error: " + e.getMessage());
//		    }
//		
//		SPARQLTagManager STM = new SPARQLTagManager();
//		STM.setMODE(1);
//		
//		Tagging tagger = null;
//		try {
//			tagger = new Tagging(xmlBuffer.toString());
//		} catch (TaggingException e) {
//			System.out.println("Error in making Tag");
//			e.printStackTrace();
//		}
//		
//		String param = null;
//		try {
//			param = STM.createTaggingInsert(tagger);
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("createTaggingInsert:");
//		System.out.println(param + "\n");
//		
//		String user_param = null;
//		try {
//			user_param = STM.createUserInsert(tagger);
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		System.out.println("createUserInsert: ");
//		System.out.println(user_param + "\n");
//		
//		//test tag
//		List tag_list = tagger.getTags();
//		Iterator i = tag_list.iterator();
//		
//		while(i.hasNext())
//		{
//			Tag t = (Tag)i.next();
//			
//			System.out.println("createTagInsert:");
//			String tag_param = null;
//			try {
//				tag_param = STM.createTagInsert(t, tagger);
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			System.out.println( tag_param + "\n");
//			
//		}
//			System.out.println("\n");
//			try {
//				System.out.println("addTagEvent Response Code returned: " + STM.addTagEvent(tagger, xmlBuffer.toString()));
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		
	}

}
