package org.icapture.ED.tag;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.icapture.ontology.OntUtil;

/**
 * Object for containning Tagging Information
 * @author Benjamin Good
 *
 */
public class Tagging {
	String agent;

	String connotea_agent = null;

	List<Tag> tags;

	Calendar tag_moment;

	String resource_tagged;
	
	String taggingMode = null;
	
	float EDVersion;

	/**
	 * //TODO Implement this parser..
	 * 
	 * map from an xml representation to an instance of this object
	 * 
	 * <pre>
	 * <code>
	 * &lt;tagging&gt;
	 * 	&lt;agent&gt;http://www.connotea.org/user/bgood&lt;/agent&gt;
	 * 	&lt;connotea_agent&gt;bgood&lt;/connotea_agent&gt;
	 * 	&lt;Date&gt;Mon May 21 14:47:02 PDT 2007&lt;/Date&gt;
	 * 	&lt;Resource&gt;http://www.bla.com/taggable_thing.html&lt;/Resource&gt; 
	 * 	&lt;tag-list&gt;
	 * 		&lt;tag name=&quot;cell type 33:br&quot;&gt;http:purl.org/obo/brenda/brenda.owl#BTO_000007&lt;/tag&gt;
	 * 		&lt;tag name=&quot;important&quot;&gt;http://www.connotea.org/bgood/tag/important&lt;/tag&gt;
	 * 	&lt;/tag-list&gt;
	 * &lt;/tagging&gt;
	 * </code>
	 * </pre>
	 * @param xml_tagging
	 * @throws TaggingException 
	 * @throws TaggingException
	 */
	public Tagging(String xml_tagging) throws TaggingException  {
		Document doc = null;
		try {
			// Create a builder factory
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			factory.setValidating(false);
			// Create the builder and parse the file
			doc = factory.newDocumentBuilder().parse(
					new ByteArrayInputStream((xml_tagging.getBytes())));

		} catch (SAXException e) {
			throw new TaggingException(
					"There was a problem reading the input!\n"
							+ e.getLocalizedMessage());
		} catch (ParserConfigurationException e) {
			throw new TaggingException(
					"What the heck ... This shouldn't happen!\n"
							+ e.getLocalizedMessage());
		} catch (IOException e) {
			throw new TaggingException("An I/O error occurred!\n"
					+ e.getLocalizedMessage());
		}
		//t.tag = a_tag.getAttribute("name");
		NodeList taggings = doc.getElementsByTagName("tagging");
		if (taggings.getLength() >= 1) {
			Element a_tagging = (Element)taggings.item(0);
			String v = a_tagging.getAttribute("version");
			if(v!=null&&v.length()>0){
				try{
					this.setEDVersion(Float.parseFloat(v));
				}catch(NumberFormatException ne){
					System.out.println("Couldn't parse "+v);
					ne.printStackTrace();
				}
			}
			 
		}
		
		NodeList nodes = doc.getElementsByTagName("agent");
		if (nodes.getLength() >= 1) {
			this.agent = nodes.item(0).getTextContent();
			this.agent = this.agent == null ? "" : this.agent;
		}
		nodes = doc.getElementsByTagName("connotea_agent");
		if (nodes.getLength() >= 1) {
			this.connotea_agent = nodes.item(0).getTextContent();
			this.connotea_agent = this.connotea_agent == null ? null
					: this.connotea_agent;
		}

		nodes = doc.getElementsByTagName("taggingMode");
		if (nodes.getLength() >=1) {
			this.taggingMode = nodes.item(0).getTextContent();	
		}
		
		nodes = doc.getElementsByTagName("date");
		if (nodes.getLength() >= 1) {
			DateFormat formatter = new SimpleDateFormat();
			this.tag_moment = null;
			try {
				//HAVE -> Wed Aug 01 16:38:55 PDT 2007
				//NEED -> YYYY-MM-DD'T'hh:mm:ss:sss ex. 1999-05-31T13:20:00.000
				Date thed = (Date)formatter.parse(nodes.item(0).getTextContent());
				//String theds = thed.toString();
				Calendar c = new GregorianCalendar();
				c.setTime(thed);
				this.tag_moment = c;
			} catch (DOMException e) {
				
			} catch (ParseException e) {

			}
			this.tag_moment = this.tag_moment == null ? GregorianCalendar.getInstance()
					: this.tag_moment;
		}
		nodes = doc.getElementsByTagName("resource");
		if (nodes.getLength() >= 1) {
			try {
				String resource = nodes.item(0).getTextContent();
				resource = resource.replaceAll("%u2019", "%E2%80%99");//incase you get one of those CRAZY! apostrophe encodings
				//System.out.println(resource);
				this.resource_tagged = java.net.URLDecoder.decode(resource,"UTF-8");
				this.resource_tagged = this.resource_tagged.replace("|", "%7c"); //replace vertical bar/pipe | - SPARQL hates it
				this.resource_tagged = this.resource_tagged.replaceAll(" ", "+");//replace spaces in a url - SPARQL hates it
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.resource_tagged = this.resource_tagged == null ? "" : this.resource_tagged;
		}
		tags = new ArrayList<Tag>();
		nodes = doc.getElementsByTagName("tag");
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			Tag t = new Tag();
			if (node instanceof Element) {
				//tags.put(((Element)node).getAttribute("name"), ((Element)node).getTextContent().trim());
				Element a_tag = (Element)node;
				t.tag = a_tag.getAttribute("name");
				
				String ont = a_tag.getAttribute("ontology");
				if(ont.trim().compareTo("") != 0){	
					t.ontology = a_tag.getAttribute("ontology");
				}else{
					String tag_uri = a_tag.getAttribute("uri");
					if(tag_uri.contains("#"))
					{
						t.ontology = tag_uri.substring(0, tag_uri.lastIndexOf("#"));
					}
				}
				
				//t.uri = a_tag.getTextContent().trim();
				t.uri = a_tag.getAttribute("uri");
				
				
				Node ftypelist = node.getFirstChild();
				NodeList ftypes = ftypelist.getChildNodes();
				if(ftypes!=null&&ftypes.getLength()>0){
					List<Ftype> ftypess = new ArrayList<Ftype>();
					for(int f = 0; f< ftypes.getLength(); f++){
						Node ftype = ftypes.item(f);
						if(ftype instanceof Element){
							Ftype f_type = new Ftype();
							Element a_type = (Element)ftype;
							f_type.id = a_type.getAttribute("id");
							f_type.name = a_type.getAttribute("name");
							ftypess.add(f_type);
						}
					}
					t.ftypes = ftypess;
				}
				
				//Aliases
				Node aliasList = node.getLastChild();
				NodeList aliases = aliasList.getChildNodes();
				if(aliases != null && aliases.getLength() > 0){
					List<String> aList = new ArrayList<String>();
					for(int a = 0 ; a < aliases.getLength() ; a++)
					{
						Node aliasEle = aliases.item(a);
						if(aliasEle instanceof Element){
							String alias = new String();
							Element a_element = (Element)aliasEle;
							alias = a_element.getAttribute("name");
							aList.add(alias);
						}
					}
					t.alias = aList;
				}
				
				tags.add(t);
			}
		}
		
	/*	tags = new HashMap<String, String>();
		nodes = doc.getElementsByTagName("tag");
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			if (node instanceof Element) {
				tags.put(((Element)node).getAttribute("name"), ((Element)node).getTextContent().trim());
			}
		}
		*/
	}
	public static void main(String[] args) throws TaggingException {
//		HAVE -> Wed Aug 01 16:38:55 PDT 2007
		//NEED -> YYYY-MM-DD'T'hh:mm:ss:sss ex. 1999-05-31T13:20:00.000
		//										2007-08-18T00:52:25.718Z
//		String date_ex = "Fri July 18 14:47:02 PDT 2007";
//		DateFormat formatter = new SimpleDateFormat();
		/*
		//	Date d = (Date)formatter.parse(date_ex);			
			Calendar c = new GregorianCalendar();
		//	c.setTime(d);
			Model m = ModelFactory.createDefaultModel();
			Literal thedate = m.createTypedLiteral(c);
			System.out.println(thedate);

		
		
	//from the original ED
		String xml = "<tagging>\n" + 
				" 	<agent>http://www.connotea.org/user/bgood</agent>\n" + 
				" 	<connotea_agent>bgood</connotea_agent>\n" + 
				" 	<date>Fri July 18 14:47:02 PDT 2007</date>\n" + 
				" 	<resource>http://www.bla.com/taggable_thing_888.html</resource> \n" + 
				" 	<tag-list>\n" + 
				" 		<tag name=\"Physical Object\" ontology=\"http://www.berkeleybop.org/ontologies/obo-all/loggerhead_nesting/loggerhead_nesting.owl\">http://purl.org/obo/owl/EKB#EKB_0000008</tag>\n" + 
				"		<tag name=\"important\" ontology=\"\">http://www.connotea.org/bgood/tag/important</tag>\n" + 
				" 	</tag-list>\n" + 
				" </tagging>\n" + 
				"";
		

		*/
		
	//for ED2.0	
/*
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<tagging>" +
				"<agent>http://www.connotea.org/user/bgood</agent>" +
				"<connotea_agent>bgood</connotea_agent>" +
				"<date>Fri Mar 14 2008 15:50:03 GMT-0700 (PDT)</date>" +
				"<resource>http://bioinfo.icapture.ubc.ca/bgood/index.html</resource>" +
				"<tag-list>" +
				"<tag ontology=\"http://www.freebase.com\" name=\"Cat\" uri=\"http://www.freebase.com/view/guid/9202a8c04000641f800000000000f6fc\">"+
						"<Freebase-Type-List>" +
							"<FType id=\"/common/topic\" name=\"Topic\"/>" +
							"<FType id=\"/user/zivoron/default_domain/animals\" name=\"Animals\"/>" +
							"<FType id=\"/fictional_universe/character_species\" name=\"Character Species\"/>" +
							"<FType id=\"/biology/organism_classification\" name=\"Organism Classification\"/>" +
						"</Freebase-Type-List>" +
				"</tag>" +
				"<tag ontology=\"http://www.freebase.com\" name=\"Cata Mansikka-aho\" uri=\"http://www.freebase.com/view/guid/9202a8c04000641f8000000006d06a51\">" +
					"<Freebase-Type-List>" +
						"<FType id=\"/common/topic\" name=\"Topic\"/><FType id=\"/people/person\" name=\"Person\"/>" +
						"<FType id=\"/music/artist\" name=\"Musical Artist\"/>" +
					"</Freebase-Type-List>" +
				"</tag>" +
				"</tag-list>" +
				"</tagging>";
	

		*/
String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
		"<tagging version=\"2.0\">" +
		"<agent>http://www.connotea.org/user/bgood</agent>" +
		"<connotea_agent>bgood</connotea_agent>" +
		"<date>Fri Apr 25 2008 16:35:34 GMT-0700 (PDT)</date>" +
		"<resource>http%3A//www.entitydescriber.org/AdminDumperServlet</resource>" +
		"<tag-list>" +
		"<tag ontology=\"http://www.freebase.com\" name=\"MobySim cricket\" uri=\"http://www.freebase.com/view/guid/9202a8c04000641f800000000003c943\">" +
		"<Freebase-Type-List><FType id=\"/common/topic\" name=\"Topic\"/>" +
		"<FType id=\"/cricket/cricket_match_type\" name=\"Cricket Match Type\"/>" +
		"</Freebase-Type-List></tag></tag-list></tagging>";

/**
 
 <?xml version="1.0" encoding="UTF-8"?>
 <tagging version="2.0">
 <agent>http://www.connotea.org/user/bgood</agent>
 <connotea_agent>bgood</connotea_agent>
 <date>Sat Jun 07 2008 22:40:13 GMT-0700 (PDT)</date>
 <resource>http%3A//subversion.tigris.org/project_packages.html</resource>
 <tag-list>
 	<tag ontology="http://www.freebase.com" 
 		name="Subversion" 
 		uri="http://www.freebase.com/view/guid/9202a8c04000641f8000000000112f29">
 		<Freebase-Type-List><FType id="/common/topic" name="Topic"/>
 			<FType id="/computer/software" name="Software"/>
 			<FType id="/user/chriseppstein/default_domain/revision_control_system" name="Revision Control System"/>
 		</Freebase-Type-List>
 	</tag>
 	<tag ontology="http://www.freebase.com" name="Subversion" 
 		uri="http://www.freebase.com/view/guid/9202a8c04000641f8000000000112f29">
 		<Freebase-Type-List><FType id="/common/topic" name="Topic"/>
 			<FType id="/computer/software" name="Software"/>
 			<FType id="/user/chriseppstein/default_domain/revision_control_system" name="Revision Control System"/>
 		</Freebase-Type-List>
 	</tag>
 </tag-list>
 </tagging>

 
 */

//String pxml = "tagging=%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3Ctagging%20version%3D%222.0%22%3E%3Cagent%3Ehttp%3A//www.connotea.org/user/bgood%3C/agent%3E%3Cconnotea_agent%3Ebgood%3C/connotea_agent%3E%3Cdate%3ESat%20Jun%2007%202008%2022%3A40%3A13%20GMT-0700%20%28PDT%29%3C/date%3E%3Cresource%3Ehttp%253A//subversion.tigris.org/project_packages.html%3C/resource%3E%3Ctag-list%3E%3Ctag%20ontology%3D%22http%3A//www.freebase.com%22%20name%3D%22Subversion%22%20uri%3D%22http%3A//www.freebase.com/view/guid/9202a8c04000641f8000000000112f29%22%3E%3CFreebase-Type-List%3E%3CFType%20id%3D%22/common/topic%22%20name%3D%22Topic%22/%3E%3CFType%20id%3D%22/computer/software%22%20name%3D%22Software%22/%3E%3CFType%20id%3D%22/user/chriseppstein/default_domain/revision_control_system%22%20name%3D%22Revision%20Control%20System%22/%3E%3C/Freebase-Type-List%3E%3C/tag%3E%3Ctag%20ontology%3D%22http%3A//www.freebase.com%22%20name%3D%22Subversion%22%20uri%3D%22http%3A//www.freebase.com/view/guid/9202a8c04000641f8000000000112f29%22%3E%3CFreebase-Type-List%3E%3CFType%20id%3D%22/common/topic%22%20name%3D%22Topic%22/%3E%3CFType%20id%3D%22/computer/software%22%20name%3D%22Software%22/%3E%3CFType%20id%3D%22/user/chriseppstein/default_domain/revision_control_system%22%20name%3D%22Revision%20Control%20System%22/%3E%3C/Freebase-Type-List%3E%3C/tag%3E%3C/tag-list%3E%3C/tagging%3E";

//try {
//	xml = java.net.URLDecoder.decode(pxml,"UTF-8");
//} catch (UnsupportedEncodingException e) {
	// TODO Auto-generated catch block
//	e.printStackTrace();
//}
System.out.println(xml);

		Tagging t = new Tagging(xml);
		System.out.println("Agent: " + t.getAgent());
		System.out.println("Connotea agent:" + t.getConnotea_agent());
		System.out.println("Resource:" + t.getResource_tagged());
		System.out.println("Time Tagged:" + t.getTag_moment().toString());
		System.out.println("Tags:" + t.getTags());
		System.out.println("EDversion:" + t.getEDVersion());
/*		
		TagManager m = new TagManager();
		System.out.println(m.addTagEvent(t));
		m.init();
		OntUtil.writeAnyModel(m.ct, "CollectedData/test.rdf");
		m.closeConnection();
*/	
	}

	public Tagging() {
		// TODO Auto-generated constructor stub
	}
	
	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getConnotea_agent() {
		return connotea_agent;
	}

	public void setConnotea_agent(String connotea_agent) {
		this.connotea_agent = connotea_agent;
	}

	public String getResource_tagged() {
		return resource_tagged;
	}

	public void setResource_tagged(String resource_tagged) {
		this.resource_tagged = resource_tagged;
	}

	public Calendar getTag_moment() {
		return tag_moment;
	}

	public void setTag_moment(Calendar tag_moment) {
		this.tag_moment = tag_moment;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	
	public float getEDVersion() {
		return EDVersion;
	}
	public void setEDVersion(float version) {
		EDVersion = version;
	}
	
	public String getTaggingMode(){
		return taggingMode;
	}
	
	public void setTaggingMode(String taggingMode){
		this.taggingMode = taggingMode;
	}
	
}
