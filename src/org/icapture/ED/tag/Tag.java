package org.icapture.ED.tag;

import java.util.List;
/**
 * 
 */

/**
 * @author Benjamin Good
 *
 */
public class Tag {

	public String tag;
	public String uri;
	public String ontology;
	public List<Ftype> ftypes;
	public List<String> alias;
	public String tagUID;
	public String toString(){
		String s = "tag: "+tag+"\n uri: "+uri+"\n ontology: "+ ontology;
		
		if(ftypes!=null){
			for(Ftype f : ftypes){
				s = s+"\n f_id:"+f.id+" f_name"+f.name;
			}
			for(String a : alias){
				s = s+"\n alias: " + a;
			}
		}
		return s;
	}
}
