package org.icapture.ED;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.icapture.ED.config.EdConfig;
/**
 * Servlet Filter which adds the 'X-XRDS-Location' header to all servlets and pages 
 * @author paul lu
 *
 */
public class EDFilter implements Filter {
	private String XRDSLocation;
	private FilterConfig filterConfig;
	
	public void destroy() {
		
	}

	/**
	 * Method called to add the 'X-XRDS-Location' header
	 * 
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		resp.addHeader("X-XRDS-Location", this.XRDSLocation);

		chain.doFilter(request, response);

	}

	/**
	 * Called when this servlet is called.
	 * Sets the XRDSLocation variable to the url in EdConfig class
	 */
	public void init(FilterConfig fc) throws ServletException {
		this.filterConfig = fc;
		this.XRDSLocation = EdConfig.getED_yadis_url();
		
	}

}
