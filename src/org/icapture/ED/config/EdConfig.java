package org.icapture.ED.config;

import java.io.IOException;
import java.util.Properties;

/**
 * Keeps default values for Entity Describer's configuration and 
 * contains methods for setting new values for the Entity Describer's properties
 * @author Eddie Kawas
 * 
 */
public class EdConfig {

    /* the following properties can be found in our main properties file */
    public static String ED_CONNOTEA_PASSWORD = "";

    public static String ED_CONNOTEA_USERNAME = "";

    public static String TERM_XML_DIR = "";

    public static String TERM_JSON_DIR = "";

    public static String OWL_DIR = "";

    public static String CUSTOM_ONTOLOGY_DIRECTORY = "";

    public static String ED_ROOT_URL = ""; 
    
    public static String ED_YADIS_DOC_LOCATION = "";
    
    public static String ED_HASH_secret = "";
    
    /* the following Properties objects are contained in properties files */
    public static Properties DATA_SOURCES_PROPERTIES = new Properties();

    public static Properties DATA_STORES_PROPERTIES = new Properties();
    
    // load all of our properties from our main file
    static 
    {
		Properties p = new Properties();
		try {
		    p.load(EdConfig.class
			    .getResourceAsStream("/ED_PROPERTIES.properties"));
		    ED_CONNOTEA_PASSWORD = p.getProperty("ed.password", "");
		    ED_CONNOTEA_USERNAME = p.getProperty("ed.username", "");
		    TERM_XML_DIR = p.getProperty("term.xml.dir", "");
		    TERM_JSON_DIR = p.getProperty("term.json.dir", "");
		    OWL_DIR = p.getProperty("owl.dir", "");
		    CUSTOM_ONTOLOGY_DIRECTORY = p.getProperty("custom.ontology.directory", "");
		    ED_ROOT_URL = p.getProperty("ED_ROOT_URL", "");
		    ED_YADIS_DOC_LOCATION = p.getProperty("ED_YADIS_DOC_LOCATION", "");
		    
		    // get data_source.properties
		    if (!p.getProperty("file.custom.custom.datasource", "").equals("")) 
		    {
				try 
				{
				    DATA_SOURCES_PROPERTIES.load(EdConfig.class
					    .getResourceAsStream(p.getProperty(
						    "file.custom.custom.datasource", "")));
				} catch (IOException e) {
				    e.printStackTrace();
				}
		    }
		    
		    // get data_store.properties
		    if (!p.getProperty("file.custom.custom.datastore", "").equals("")) 
		    {
				try 
				{
				    DATA_STORES_PROPERTIES.load(EdConfig.class
					    .getResourceAsStream(p.getProperty(
						    "file.custom.custom.datastore", "")));
				} catch (IOException e) {
				    e.printStackTrace();
				}
			}
		    
		} catch (IOException e) {
		    e.printStackTrace();
		}

    }

    /**
     * @return the secret word used to generate the hash code for the secure cookie
     * for users logging in
     */
    public static String getED_HASH_secret(){
    	return ED_HASH_secret;
    }
    
    /**
     * 
     * @param ed_hash_secret the secret word used to generate the hash code for the secure cookie
     * for users logging in 
     */
    public static void setED_HASH_secret(String ed_hash_secret){
    	ED_HASH_secret = ed_hash_secret;
    }
   
    /**
     * 
     * @return The basepath of the web app.  eg. http://localhost:8080/EntityDescriber
     */
    public static String getED_root_url(){
    	return ED_ROOT_URL;
    }
    
    /**
     * 
     * @param root_url The basepath of the web app.  eg. http://localhost:8080/EntityDescriber
     */
    public static void setED_root_url(String root_url){
    	ED_ROOT_URL = root_url;
    }
    
    /**
     * 
     * @return The path to the YADIS document for openid 2.0 authentication relative to ED_ROOT_URL
     */
    public static String getED_yadis_doc_loc(){
    	return ED_YADIS_DOC_LOCATION;
    }
    
    /**
     * 
     * @param yadis_loc The path to the YADIS document for openid 2.0 authentication relative to ED_ROOT_URL
     */
    public static void setED_yadis_doc_loc(String yadis_loc){
    	ED_YADIS_DOC_LOCATION = yadis_loc;
    }
    
    /**
     * 
     * @return The url to the YADIS document used for openid 2.0 authentication
     */
    public static String getED_yadis_url(){
    	return ED_ROOT_URL + ED_YADIS_DOC_LOCATION;
    }
    
    public static void main(String[] args) {
	System.out.println(EdConfig.DATA_SOURCES_PROPERTIES);
	System.out.println(EdConfig.DATA_STORES_PROPERTIES);
    }

}
