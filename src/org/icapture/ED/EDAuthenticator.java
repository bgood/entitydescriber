package org.icapture.ED;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.icapture.ED.config.EdConfig;
import org.icapture.ED.dataAccess.SPARQLEndPointAccess;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * EDAuthenticator is primarily used to create, manage and destroy cookies used for 
 * user authentication. 
 * 
 * @author Paul Lu
 *
 */
public class EDAuthenticator {
	
	//All Message Digest for ED will be using the SHA algorithm
	/* Message digests are secure one-way hash functions that 
	 * take arbitrary-sized data and output a fixed-length hash value.
	 */
	private static MessageDigest md = null;
	//used for encoding the Hash value
	private static Hex HexEncoder = new Hex();
	private static String ed_hash_secret = EdConfig.getED_HASH_secret();
	private static Logger logger = Logger.getLogger(EDAuthenticator.class);

	
	/**
	 * 
	 * Re-calculates the Hash value from the given values in the secure cookie "EDES_HASH" and compares it to 
	 * make sure it is the same Hash value and therefore valid.  
	 *
	 * @param request The HttpServletRequest
	 * @return boolean True if the hashcookie is valid, false otherwise
	 */
	public static boolean isHashCookieValid(HttpServletRequest request)
	{
		try {
			//Get the cookie from the request
			Cookie[] cookies = request.getCookies();
			Cookie hash_cookie = getCookie(cookies, "EDES_HASH");
				if(hash_cookie == null)
				{
					return false;
				}
			
			String hash_vals = hash_cookie.getValue();
			
			//Split it up into their respective values: "openid,logintime,HASH" 
			String[] hash_split = hash_vals.split(",");
	
				if(hash_split.length != 3)
				{
					return false;
				}
			
			
			String hash = hash_split[2];
			//calculate hash
				md = MessageDigest.getInstance("SHA");
				
				String values = hash_split[0] + "," + hash_split[1] + "," + ed_hash_secret; //join openid and logintime and secret
			//convert to bytes
				byte[] val_inBytes = values.getBytes();
			//do update on bytes -> digest it -> encode (so that it is a Base64 String like the one from the cookie) 
				md.update(val_inBytes);	
				
				String calculated_Hash = String.valueOf(HexEncoder.encodeHex(md.digest()));
			//compare the cookie hash and the calculated Hash
				if(calculated_Hash.compareTo(hash) == 0)
				{
					return true;
				}
		} catch (NoSuchAlgorithmException e) {
			
			logger.error("Failure in ED_Authenticator method: isHashCookieValid: " + e.getLocalizedMessage());
			return false;
		}
		
		return false;
	}

	/**
	 * Checks to make sure the secure cookie "EDES_HASH" is valid and that it corresponds to the 
	 * provided username
	 * 
	 * @param request The HttpServletRequest
	 * @param username The username being authenticated
	 * @return boolean Returns true if "EDES_HASH" cookie corresponds to the username and "EDES_HASH" is valid
	 */
	public static boolean isHashCookieValid_username(HttpServletRequest request, String username)
	{
		try {
			//Get the cookie from the request
			Cookie[] cookies = request.getCookies();
			Cookie hash_cookie = getCookie(cookies, "EDES_HASH");
				if(hash_cookie == null)
				{
					return false;
				}
			
			String hash_vals = hash_cookie.getValue();
			
			//Split it up into their respective values: "openid,logintime,HASH" 
			String[] hash_split = hash_vals.split(",");
	
				if(hash_split.length != 3)
				{
					return false;
				}
			
			//Check username
				String hash_name = hash_split[0];
				if(username != hash_name)
				{
					return false;
				}
				
			String hash = hash_split[2];
			//calculate hash
				md = MessageDigest.getInstance("SHA");
				
				String values = hash_split[0] + "," + hash_split[1] + "," + ed_hash_secret; //join openid and logintime and secret
			//convert to bytes
				byte[] val_inBytes = values.getBytes();
			//do update on bytes -> digest it -> encode (so that it is a Base64 String like the one from the cookie) 
				md.update(val_inBytes);	
				
				String calculated_Hash = String.valueOf(HexEncoder.encodeHex(md.digest()));
			//compare the cookie hash and the calculated Hash
				if(calculated_Hash.compareTo(hash) == 0)
				{
					return true;
				}
		} catch (NoSuchAlgorithmException e) {
			
			logger.error("Failure in ED_Authenticator method: isHashCookieValid_username: " + e.getLocalizedMessage());
			return false;
		}
		
		return false;
	}
	
	/**
	 * Creates a new Secure Cookie called 'EDES_HASH' for the user. So long as the 'EDES_HASH' cookie is present 
	 * the user stays logged into Entity Describer 
	 * 
	 * @param EDusrnm  The username the user is logging in with 
	 * @param login_time The time the user logged in at expressed as a Long
	 * @return Cookie The "EDES_HASH" Cookie containing the values and hash used to authenticate the user.
	 */
	public static Cookie makeHashCookie(String EDusrnm, Long login_time)
	{
		Cookie edHashCookie = null;
		try {
			md = MessageDigest.getInstance("SHA");
			
				byte[] usrnm_inBytes = EDusrnm.getBytes();
				md.reset();
				md.update(usrnm_inBytes);
				String usrnm_HASH = String.valueOf(HexEncoder.encodeHex(md.digest()));
				
				String values = usrnm_HASH + "," + login_time.toString();
				
				String valuesPlusSecret = values  + "," + ed_hash_secret;
			
				//convert to bytes
					byte[] val_inBytes = valuesPlusSecret.getBytes();
						//do update->digest->encode
						md.reset();
						md.update(val_inBytes);				
						String HashString = String.valueOf(HexEncoder.encodeHex(md.digest()));
						
						//create new hash cookie
						String edhash_values = values + "," + HashString;
						edHashCookie = new Cookie("EDES_HASH", edhash_values);
		
		} catch (NoSuchAlgorithmException e) {
			
			logger.error("Failure in ED_Authenticator method: makeHashCookie: " + e.getLocalizedMessage());
			
			return null;
		}
		
		return edHashCookie;
	}
	
	/**
	 * Creates a Cookie with name: 'EDES_USERNAME' and value: Hash of the String param EDusrnm
	 * @param EDusrnm The username.
	 * @return Cookie Cookie named 'EDES_USERNAME'
	 */
	public static Cookie makeUserHashCookie(String EDusrnm)
	{
		Cookie edUsrHashCookie = null;
		
		try {
			md = MessageDigest.getInstance("SHA");

			byte[] usrnm_inBytes = EDusrnm.getBytes();
			md.reset();
			md.update(usrnm_inBytes);
			String usrnm_HASH = String.valueOf(HexEncoder.encodeHex(md.digest()));
			edUsrHashCookie = new Cookie("EDES_USERHASH", usrnm_HASH);
			
		} catch (NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		}
		return edUsrHashCookie;
		
	}
	
	/**
	 * Logs out of Entity Describer by invalidating the session and destroying the Cookies
	 *  
	 * @param request The HttpServletRequest  
	 * @param response The HttpServletResponse
	 * @return boolean true 
	 */
	public static boolean logout(HttpServletRequest request, HttpServletResponse response)
	{
		HttpSession curr_session = request.getSession(false);
		
		//destroy current session
		if(curr_session != null)
		{
			curr_session.invalidate();
		}
		//destroy cookie ED_USERNAME
		removeCookie(request, response, "ED_USERNAME");
		removeCookie(request, response, "ED_HASH");
		removeCookie(request, response, "EDES_USERNAME");
		removeCookie(request, response, "EDES_USERHASH");
		removeCookie(request, response, "EDES_HASH");
		removeCookie(request, response, "NEXTPAGE");

		return true;
	}
	
	/**
	 * 
	 * Gets the Cookie from the request that matches the name Parameter.
	 * Returns null if the Cookie does not exist.
	 * 
	 * @param cookies Cookie Array (You get this from calling HttpServletRequest method getCookies
	 * @param name Name of the Cookie you want
	 * @return The Cookie matching the name Parameter
	 */
	public static Cookie getCookie(Cookie[] cookies, String name)
	{
		if(cookies != null)
		{
			for (int i = 0; i < cookies.length; i++) 
			{
		        Cookie cookie = cookies[i];
		        if(cookie.getName().compareTo(name) == 0)
		        {	        
		        	return cookie;
		        }
			}
		}
		return null;
	}

	/**
	 * Destroys the Cookie with the name corresponding to the String param cookieName
	 * 
	 * @param request the HttpServletRequest
	 * @param response the HttpServletResponse
	 * @param cookieName Name of the Cookie you want destroyed
	 */
	public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String cookieName)
	{
		Cookie[] cookies = request.getCookies();
		Cookie cookie = getCookie(cookies, cookieName);
		if(cookie != null)
		{
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}
	}
	
	/**
	 * Converts a request's parameters into a string of form: ?key1=value1&key2=value2
	 * to be attached at the end of a URL
	 * @param request the HttpServletRequest
	 * @return String - URL paramString
	 */
	public static String make_param_string(HttpServletRequest request)
	{
		StringBuffer sb = new StringBuffer();
	    sb.append("?");
	    Enumeration<String> names = request.getParameterNames();
	    boolean hasParams = false;
	    
	    while (names.hasMoreElements()) {
		String name = names.nextElement();
		
		try {
			sb.append(name
				+ "="
				+ URLEncoder.encode(request.getParameter(name), "UTF-8")
				+ "&");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
		
		hasParams = true;
		
	    }
	    
	    if(hasParams){
	    	return sb.toString();
	    }else{
		    	return null;
	     }
	}
	
	
	/**
	 * Checks to see if the user exists by sending a query to the SPARQL Endpoint
	 * 
	 * @param user_hash 
	 * 				String - hash of the user's openid.  (openid minus the protocol http:// or https://)
	 * @return
	 * 				String - nickname of user, null otherwise
	 */
	public static String doesUserExist(String user_hash)
	{
		JSONArray jsonUserArray = null;

		//SPARQL query for user and name
		StringBuffer q = new StringBuffer();
			q.append(SPARQLEndPointAccess.prefix_rdf + "\n");  
			q.append(SPARQLEndPointAccess.prefix_tag + "\n");
			q.append("select Distinct * where { \n"); 
			q.append("?user rdf:type tag:User . \n");
			q.append("FILTER(?user = <" + SPARQLEndPointAccess.ns + user_hash + ">) \n");
			q.append("?user tag:name ?nickname .\n");
			q.append("}");		

		//Send query
			JSONObject jsonUserResult = null;
			jsonUserResult = SPARQLEndPointAccess.sendQueryGetJSON(q.toString());
		
			if(jsonUserResult == null)
				return null;
			
		//Process Result	
			try 
			{
				
				jsonUserArray = jsonUserResult.getJSONObject("results").getJSONArray("bindings");
				
				if(jsonUserArray.length() != 0 && jsonUserArray != null)
				{
					String nickname = jsonUserArray.getJSONObject(0).getJSONObject("nickname").getString("value");
					return nickname;
				}else{		
					return null;
				}
			} catch (JSONException e) {
					e.printStackTrace();
					return null;
			}
			
	}	
	
/**
 * @param args
 */
public static void main(String[] args) {
	// TODO Auto-generated method stub

	}

}
