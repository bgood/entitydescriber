package org.icapture.ED.ontology;

import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.icapture.ontology.OntUtil;
import org.icapture.web.HttpUtil;

import com.hp.hpl.jena.ontology.OntModel;

public class OntologyWriter {

	
	public static int parseAndStoreOntology(String url, String ont_loc, String term_loc) throws MalformedURLException, IOException{
		int response = 0;
		HttpURLConnection urlConnection = (HttpURLConnection)(new URL(url)).openConnection();
		urlConnection.setUseCaches(true);
		response = urlConnection.getResponseCode();
		if(response<400){
			int length = urlConnection.getContentLength();
			int max = HttpUtil.max_upload_file_size;
			//ok check length, ok, then write otherwise return too big response
			if(length<max){
				//if length ok, then send it to w3c for validation
//				int rdf_valid = Utils.hitW3CRDFValidator(url);
//				if(rdf_valid!=1){
//					return -2;
//				}
				//if good, then store it
				OntModel m = OntUtil.getOwlMemModel(url);
				//store it..
				OntUtil.writeAnyModel(m, ont_loc);
				//parse it and store the terms
				OntologyManager om = new OntologyManager();
				String terms = OntologyParser.getTermsAndDefsFromModel(m, "JSON","", "");
				FileWriter f = new FileWriter(term_loc);
				f.write(terms);
				f.close();
				m.close();
			}else{
				response = 413; // too big
			}
		}
		return response;
	}

}
