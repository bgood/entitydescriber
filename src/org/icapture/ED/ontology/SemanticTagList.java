/**
 * 
 */
package org.icapture.ED.ontology;

import java.util.HashMap;

/**
 * @author Benjamin Good
 *
 */
public class SemanticTagList {
	String uri;
	HashMap<String,String> uris;
	HashMap<String,String> tags;
	
	public SemanticTagList(){
		uri = "http://bioinfo.icapture.ubc.ca/Resources/";
		uris = new HashMap<String,String>();
		tags = new HashMap<String,String>();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}
	
	public HashMap<String, String> getTags() {
		return tags;
	}
	public void setTags(HashMap<String, String> tags) {
		this.tags = tags;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public HashMap<String, String> getUris() {
		return uris;
	}
	public void setUris(HashMap<String, String> uris) {
		this.uris = uris;
	}
	
}
