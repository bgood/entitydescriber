package org.icapture.ED.ontology;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import java.io.*;

import org.icapture.ED.SemanticTagging;
import org.icapture.ED.tag.Tag;
import org.icapture.ontology.OntUtil;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.RDFReader;

/**
 * @author Benjamin Good
 *
 */
public class OntologyManager extends SemanticTagging{
	private static final String TOKEN = "_|_";
	private static final String REGEX_TOKEN = "_\\|_";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	//	String ont_url0 = "http://purl.org/obo/owl/MESH"; 
		//http://purl.org/obo/owl/MESH#MESH_A.01.047
	//	String ont_url1 = "http://www.berkeleybop.org/ontologies/obo-all/fma_lite/fma_lite.owl"; 
	//	String ont_url2 = "http://www.berkeleybop.org/ontologies/obo-all/caro/caro.owl"; 
	//	String ont_url3 = "http://biomoby.org/RESOURCES/MOBY-S/Objects"; 
	//	String ont_url4 = "http://bioinfo.icapture.ubc.ca/bgood/ont/BioLinks.owl"; 
	//	String ont_url5 = "file:BioLinks/BioLinks.owl"; 
	
		String n = "obi";
		String ont_url = "http://arch.uwindsor.ca:8080/ED/ont/"+n;
		OntologyManager om = new OntologyManager();
		//parse it and store the terms
		String json = om.getTermsAndDefs(ont_url);
		FileWriter f;
		try {
			f = new FileWriter("/usr/local/tomcat/term_json/"+n+".json");
			f.write(json);
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getTermsAndDefs(String url){
		OntModel model = OntUtil.getEmptyOwlModel();
		RDFReader reader = model.getReader();
			
		try {
			reader.read(model, new URL(url).openStream(), null);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return "";

		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
		return OntologyParser.getTermsAndDefsFromModel(model, "JSON", "", "");
	}

	/**
	 * looks for a cached json term file and returns its contents
	 * @param ont_url
	 * @return
	 * @throws IOException 
	 */
	public String getCachedTermJSON(String ont_url) throws IOException{
		String json = null;
		init();
		OntResource requested_ont = ct.getOntResource(ont_url);
		String local_json = "";
		local_json = requested_ont.getProperty(termFile).getString();
		if(local_json!=null){
			BufferedReader read =  new BufferedReader(new FileReader(local_json));
			if(read==null){
				return json;
			}
			String line = read.readLine();
			json="";
			while(line!=null){
				json+=line;
				line = read.readLine();
			}
		}
		closeConnection();
		return json;
	}	
	
	/**
	 * looks for a cached xml term file and returns its contents
	 * @param ont_url
	 * @return
	 * @throws IOException 
	 */
	public String getCachedTermXML(String ont_url) throws IOException{
		String xml = null;
		init();
		OntResource requested_ont = ct.getOntResource(ont_url);
		String local_xml = "";
		local_xml = requested_ont.getProperty(termFile).getString();
		if(local_xml!=null){
			BufferedReader read =  new BufferedReader(new FileReader(local_xml));
			if(read==null){
				return xml;
			}
			String line = read.readLine();
			xml="";
			while(line!=null){
				xml+=line;
				line = read.readLine();
			}
		}
		closeConnection();
		return xml;
	}

/**
 * given an ontology url (its unique id), use a local version to get terms
 * @param ont_url
 * @return
 * @throws Exception
 */
public String getLocalTermsAndDefs(String ont_url) throws Exception {
	//look up stored local reference
	init();
	OntResource requested_ont = ct.getOntResource(ont_url);
	String local_ont_uri = "";
	String response = null;
	local_ont_uri = requested_ont.getProperty(localURI).getString();
	try{	
		//Model model = ModelFactory.createDefaultModel();
		OntModel model = OntUtil.getEmptyOwlModel();
		model.read("file:"+local_ont_uri);		
		response = OntologyParser.getTermsAndDefsFromModel(model, "JSON", "", "");
	}catch(Exception e){
		throw e;
	}
	closeConnection();
	return response;
}

/**
 * Set the local path for the given ontology
 * @param ont_url
 * @param new_loc
 */
public void updateLocalOntLoc(String ont_url, String new_loc) {
	init();
	OntResource requested_ont = ct.getOntResource(ont_url);
	requested_ont.setPropertyValue(localURI, ct.createTypedLiteral(new String(new_loc)));
	closeConnection();
}

/****************************************************************************
 * Queries below here
 * 
 */

/**
 * given a resolvable uri for an ontology (maybe on file system or web), 
 * 	load it, query its class uris and class labels with the query,
 *  return an xml string describing the classes as terms
 *  if expand, then also produce all of the children
 */
public Set<Tag> expandUri(String ont_url, String ont_uri){
	OntModel m = OntUtil.getEmptySimpleRDFSModel();
	m.read(ont_url);
	OntClass term = m.getOntClass(ont_uri);
	if(term==null){
		return null;
	}
	Set<Tag> tags = new HashSet<Tag>();
	Tag n = new Tag();
	n.uri = term.getURI();
	n.ontology = ont_url;
	n.tag = term.listLabels(null).next()+"";
	if(n.tag==""){
		n.tag= OntUtil.getStringName(term);
	}else if(n.tag.endsWith("@en")||n.tag.endsWith("@EN")){
		n.tag = n.tag.substring(0,n.tag.lastIndexOf('@'));
	}
	tags.add(n);
	
	for(Iterator<OntClass> sc = term.listSubClasses(); sc.hasNext();){
		OntClass sub = sc.next();
		n = new Tag();
		n.uri = sub.getURI();
		n.ontology = ont_url;
		n.tag = sub.listLabels(null).next()+"";
		if(n.tag==""){
			n.tag= OntUtil.getStringName(sub);
		}else if(n.tag.endsWith("@en")||n.tag.endsWith("@EN")){
			n.tag = n.tag.substring(0,n.tag.lastIndexOf('@'));
		}
		tags.add(n);
	} 
	return tags;
}

/**
 * given a resolvable uri for an ontology (maybe on file system or web), 
 * 	load it, query its class uris and class labels with the query,
 *  return an xml string describing the classes as terms
 *  if expand, then also produce all of the children
 */
public Set<Tag> ontologyQuery(String ont_url, String query, boolean expand){
	OntModel m = OntUtil.getEmptySimpleRDFSModel();
	m.read(ont_url);
	List<OntClass> classes = getClassList(query, m);
	if(classes==null){
		return null;
	}
	Set<Tag> tags = new HashSet<Tag>();
	for(Iterator<OntClass> c = classes.iterator(); c.hasNext(); ){
		OntClass term = c.next();
		Tag n = new Tag();
		n.uri = term.getURI();
		n.ontology = ont_url;
		n.tag = term.listLabels(null).next()+"";
		if(n.tag==""){
			n.tag= OntUtil.getStringName(term);
		}else if(n.tag.endsWith("@en")||n.tag.endsWith("@EN")){
			n.tag = n.tag.substring(0,n.tag.lastIndexOf('@'));
		}
		tags.add(n);
		if(expand){
			for(Iterator<OntClass> sc = term.listSubClasses(); sc.hasNext();){
				OntClass sub = sc.next();
				n = new Tag();
				n.uri = sub.getURI();
				n.ontology = ont_url;
				n.tag = sub.listLabels(null).next()+"";
				if(n.tag==""){
					n.tag= OntUtil.getStringName(sub);
				}else if(n.tag.endsWith("@en")||n.tag.endsWith("@EN")){
					n.tag = n.tag.substring(0,n.tag.lastIndexOf('@'));
				}
				tags.add(n);
			} 
		}
	}
	return tags;
}



/**
 * Return a list of classes from the input ontology
 * whose name (part after the # in the URI)or label matches the regexQ
 * Note the reasoning method for the ModelSpec matters here..
 * @param regexQ
 * @param inputOnt
 * @return
 */	
	public List<OntClass> getClassList(String regexQ, OntModel inputOnt){
		List<OntClass> classes = new ArrayList<OntClass>();

		String queryString = "" +
		" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> "+
		" prefix owl: <http://www.w3.org/2002/07/owl#> " +
		" prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
		" select distinct ?x" +
		" where {" +
		" {{ ?x rdf:type owl:Class } UNION { ?x rdf:type rdfs:Class}} " +
		"  " +
		"{ " +
		" FILTER regex(str(?x), '"+regexQ+"', 'i') " +
		"} " +
		"UNION {" +
		" ?x rdfs:label ?y . " + 
		" FILTER regex(str(?y), '"+regexQ+"', 'i') " +
		"} " +
		"}";
		
	//	" order by local:RandomFilter(?x) " +
	//	"limit 10";

	//	System.out.println(queryString);

	Query query = QueryFactory.create(queryString) ;
	QueryExecution qexec = QueryExecutionFactory.create(query, inputOnt) ;
	try {
		ResultSet results = qexec.execSelect() ;
		for ( ; results.hasNext() ; ){
			QuerySolution soln = results.nextSolution() ;      	
			RDFNode x = soln.get("x") ;
			if ( x.isResource())
			{
				OntClass c = inputOnt.getOntClass(x.toString());
				classes.add(c);
				//System.out.println("x is "+x.toString());
			}
		}
	} finally { qexec.close() ; }
		
		return classes;
	}


}
