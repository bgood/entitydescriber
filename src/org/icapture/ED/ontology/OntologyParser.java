/**
 * 
 */
package org.icapture.ED.ontology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.icapture.ontology.OntUtil;
import org.icapture.ontology.Structure;
import org.icapture.web.HttpUtil;
import org.stringtree.json.JSONWriter;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * Handles text processing associated with ontologies
 * @author Benjamin Good
 *
 */
public class OntologyParser {

	public static void main(String[] args){
		/*OntModel m = OntUtil.getOwlMemModel(
				"http://bioinfo.icapture.ubc.ca/Resources/mygrid_algorithm");
		String xml = getTermsAndDefsFromModel(m, "XML", "data");
		System.out.println(xml);
		*/
		/*OntModel m = OntUtil.getOwlMemModel("http://purl.org/obo/owl/GO");
		Map<String,OntModel> os = splitUpTaxonomy(m, 1);
		for(Entry<String,OntModel> o : os.entrySet()){
			System.out.println(o.getKey()+" "+o.getValue().size());
		}*/
	}

	/**
	 * 
	 * @param model
	 * @param format
	 * @param ontName
	 * @return
	 */
	public static String getTermsAndDefsFromModel(OntModel model, String format, String ontName, String ontURL) {	
		String onto_ns = "";
		//TODO get this done more elegantly using jena api
		//TODO handle multiple labels/synonyms per URI
		//this is a pretty ugly hack.. other methods were more elegant but this was fast so here it is..
		Map<String, String> results = new TreeMap<String, String>();
	//	NodeIterator subjects = model.listObjects();
		ResIterator subjects = model.listSubjects();
		int c = 0;
		while (subjects.hasNext()) {
			c++;
			RDFNode node = subjects.nextResource();//subjects.nextNode();
			if (node.isResource()) {
				Resource subject = (Resource) node;
				if (subject.hasProperty(RDF.type))
					if (subject.getProperty(RDF.type).getObject().toString()
							.endsWith("owl#Class")) {
						System.out.println(subject);
						String parent = null;
						String id = subject.getURI();
						if(id!=null){
							if(id.indexOf('#')>1){
								if(c<1001){
									onto_ns = id.substring(0,id.indexOf('#')+1);
									c = 1001;
								}
								id = id.substring(id.indexOf('#')+1);
							}else if(id.lastIndexOf('/')>1&&id.lastIndexOf('/')<id.length()){
								if(c<1001){
									onto_ns = id.substring(0,id.lastIndexOf('/')+1);		
									c = 1001;
								}
								id = id.substring(id.lastIndexOf('/')+1);
							}else{
								System.out.println("error decomposing "+subject.getURI());
								return null;
							}
							String def = getOboOrOtherDefinition((OntResource)subject.as(OntResource.class));
							if(def==null){
								def = "";
							}
							parent = OntUtil.getTagFromResource(subject);
							if (parent != null && parent.trim().length() != 0)
								results.put(parent, id+"||"+def);
						}
					}
			}
		}
		String output = "";
		if(format.equals("JSON")){
			output = termsAsJSON(results, onto_ns, ontName, ontURL);
		}else{
			output = termsAsXML(results, onto_ns, ontName, ontURL);
		}
		return output;
	}

	/**
	 * Renders an XML string containing terms and there definitions.
	 * Expects a Map of term-definition pairs, a namespace for the source ontology, and a name for the ontology
	 * @param results
	 * @param onto_ns
	 * @param ontName
	 * @return
	 */
	public static String termsAsXML(Map<String, String> results, String onto_ns, String ontName, String ontURL){

		String xml = "";
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;

		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();  
			Element root = (Element) document.createElement("terms"); 
			root.setAttribute("ontName", ontName);
			root.setAttribute("ontNs", onto_ns);
			if(ontURL != null)
				root.setAttribute("ontURL", ontURL);
			
			document.appendChild(root);

			for (Iterator<Entry<String, String>> pairs = results.entrySet().iterator(); pairs.hasNext();){
				Entry<String,String> pair = pairs.next();
				String term = (String) pair.getKey();
				String def = (String) pair.getValue();
				if (term.length() != 0){
					Element t = (Element) document.createElement("term");
					//t.setAttribute("name", term);
					t.setAttribute("uri", def.substring(0, def.indexOf("||")));
					root.appendChild(t);

					Element label = (Element) document.createElement("label");
					label.appendChild(document.createTextNode(term));
					Element description = (Element) document.createElement("description");
					description.appendChild(document.createTextNode(def.substring(def.indexOf("||")+2)));
					Element imageurl = (Element) document.createElement("imageurl");

					t.appendChild(label);
					t.appendChild(description);
					t.appendChild(imageurl);
				}
			}
			xml = HttpUtil.domToString(document);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	public static String termsAsJSON(Map<String, String> results, String onto_ns, String ontName, String ontURL){
		SemanticTagList tags = new SemanticTagList();
		tags.setUri(onto_ns);
		for (Iterator<Entry<String, String>> pairs = results.entrySet().iterator(); pairs.hasNext();){
			Entry pair = pairs.next();
			String term = (String) pair.getKey();
			String def = (String) pair.getValue();
			if (term.length() != 0){
				tags.tags.put(term, def.substring(def.indexOf("||")+2));
				tags.uris.put(term, def.substring(0, def.indexOf("||")));
			}
		}

		JSONWriter writer = new JSONWriter();
		return writer.write(tags);
	}

	/**
	 * Given an OntResource, check for an obo defintion property.  If found return it,
	 * else check for comments and return them
	 * @param c
	 * @return
	 */
	public static String getOboOrOtherDefinition(OntResource c){
		Model m = c.getModel();
		String def = null;
//		if OBO :(  hate this..
		String c_ns = c.getNameSpace();
		if(c_ns!=null&&c_ns.startsWith("http://purl.org/obo/owl/")){
			String obons = "http://www.geneontology.org/formats/oboInOwl#";
			Property oboDef = m.getProperty(obons+"hasDefinition");
			OntResource obodef = (OntResource) c.getPropertyValue(oboDef);
			if(obodef!=null){
				if (obodef.hasProperty(RDFS.label)) {
					def = obodef.getProperty(RDFS.label)
					.getString();
					if (def != null && def.indexOf("#") > 0)
						def = def.substring(def.indexOf("#") + 1);
				}
			}
			//OntProperty oboSyn = m.getOntProperty(obons+"hasRelatedSynonym");
			//ExtendedIterator obosyns = c.listPropertyValues(oboSyn);
			//	String obosyn = obosyns.toString();
			//	System.out.println("syn "+obosyn);
		}else{
			try{
				ExtendedIterator comments = (ExtendedIterator) c.listComments(null);
				if(comments!=null){
					List<String> comment = comments.toList();
					Iterator ci = comment.iterator();
					if(ci.hasNext()){
						def = ci.next().toString();
					}
				}
			}
			catch(Exception e){
				System.err.println(e);
			}
		}
		return def;
	}

	/**
	 * Given an ontology (presumably a large one so watch for memory problem here)
	 * break it apart and return a list of sub ontologies.  Depth refers to the level 
	 * at which to break the tree up.  
	 * 
	 * ex go has subclass cc, bp, and mf 
	 * if d = 1 , return subgraphs rooted at cc, bp, and mf (but include parents of root)
	 * 
	 * @param ont
	 * @param root_depth
	 * @return
	 */	
	public static Map<String,OntModel> splitUpTaxonomy(OntModel  ont, int root_depth){
		Map<String,OntModel> branches = new HashMap<String,OntModel>();
		Structure s = new Structure(ont, true);
		s.tagClassDepth();
		ExtendedIterator classes = ont.listClasses();
		while(classes.hasNext()){
			OntClass c = (OntClass)classes.next();
			if((!OntUtil.getStringName(c).equals("oboInOwl:Synonym"))
					&&(!OntUtil.getStringName(c).equals("oboInOwl:ObsoleteClass"))
					&&(!OntUtil.getStringName(c).equals("oboInOwl:DbXref"))
					&&(!OntUtil.getStringName(c).equals("oboInOwl:Subset"))
					&&(!OntUtil.getStringName(c).equals("oboInOwl:Definition"))
					&&(!OntUtil.getStringName(c).equals("oboInOwl:SynonymType"))){
				Literal dvallist = (Literal)c.getPropertyValue(s.evalOnt.getDatatypeProperty("http://default/eval.owl#depth"));
				StringTokenizer st = new StringTokenizer(dvallist.getString(),"|");
				while(st.hasMoreTokens()){
					int depth = Integer.parseInt(st.nextToken());
					if(depth==root_depth){
						//	System.out.println(OntUtil.getTagFromResource(c)+" "+dvallist);
						branches.put(OntUtil.getTagFromResource(c), OntUtil.makeSubOnt(c, ont));
					}
				}
			}
		}
		return branches;
	}

	/**
	 * 
	 * @param url
	 * @return

	public String getTermsAndDefsFromModelXML(OntModel model) {	

		//	StringBuffer sb = new StringBuffer();
		//	String newline = System.getProperty("line.separator");
		//	sb.append(xml_out);
		Map<String, String> results = new TreeMap<String, String>();

		NodeIterator subjects = model.listObjects();
		while (subjects.hasNext()) {
			RDFNode node = subjects.nextNode();
			if (node.isResource()) {
				Resource subject = (Resource) node;
				if (subject.hasProperty(RDF.type))
					if (subject.getProperty(RDF.type).getObject().toString()
							.endsWith("owl#Class")) {
						String parent = null;
						String uri = subject.getURI();
						String def = this.getOboDefinition((OntResource)subject.as(OntResource.class));
						if(def==null){
							def = "";
						}
						parent = OntUtil.getTagFromResource(subject);
	 */
	/*		if (subject.hasProperty(RDFS.label)) {
							parent = subject.getProperty(RDFS.label)
									.getString();
							if (parent != null && parent.indexOf("#") > 0)
								parent = parent
										.substring(parent.indexOf("#") + 1);
						}else{
							if(parent==null){
								parent = OntUtil.getAfterHashName(subject);
							}//parent = subject.getLocalName();
						} 
	 */
	/*						if (parent != null && parent.trim().length() != 0)
							results.put(parent, uri+"||"+def);
					}
			}
		}

		String xml = "";
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;

		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();  
			Element root = 
				(Element) document.createElement("terms"); 
			document.appendChild(root);

			for (Iterator<Entry<String, String>> pairs = results.entrySet().iterator(); pairs.hasNext();){
				Entry pair = pairs.next();
				String term = (String) pair.getKey();
				String def = (String) pair.getValue();
				if (term.length() != 0){
					Element t = (Element) document.createElement("term");
					t.setAttribute("name", term);
					t.setAttribute("uri", def.substring(0, def.indexOf("||")));
					t.appendChild(document.createTextNode(def.substring(def.indexOf("||")+2)));	
					root.appendChild(t);
					//	sb.append(newline+"<term name=\""+term+"\" uri=\""+def.substring(0, def.indexOf("||"))+"\">"+def.substring(def.indexOf("||")+2)+"</term>");
				}
			}
			xml = Utils.domToString(document);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//sb.append(newline+"</terms>");

		model.close();
		return xml;
	}	
	 */	

}
