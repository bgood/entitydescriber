package org.icapture.ED.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.icapture.ED.dataAccess.SPARQLEndPointAccess;
import org.icapture.ED.freebase.FreebaseJSONResult;
import org.icapture.ED.freebase.FreebaseJSONSearchResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to search the data source SPARQL Endpoint for a user's freetext tags
 * @author Paul Lu
 *
 */
public class UserFreeTextTagSearcher {

	/* CONSTANTS */
	private static final String __DEFAULT_SEARCH_EVERYTHING__ = "__SEARCH_EVERYTHING__";
	private static final String __SEMANTIC_TAGGING_URI_PREFIX__ = SPARQLEndPointAccess.ns;
	private static final String __SEMANTIC_TAGGING_URI_NOTHING__ = SPARQLEndPointAccess.Nothing_uri;
	private static final String __FREETEXT_PREFIX_ID__ = "/freetext/";
	private static final String __FREETEXT_PREFIX_NAME__ = "used_by_";
	private static final String __PREFIX__ = "prefix";
	
	/**
	 * Default constructor
	 */
	public UserFreeTextTagSearcher()
	{
		
	}
	
	/**
	 * Creates a FreebaseJSONSearchResponse JSONObject using the input parameters
	 * @param prefix
	 * @param dosearchEverything
	 * @param start
	 * @param limit
	 * @param types_values
	 * @return JSONObject containing search results
	 */
	public JSONObject searchFreeText(String prefix, boolean dosearchEverything, Integer start, Integer limit, String[] types_values)
	{
		ArrayList<String> usernameList = new ArrayList<String>();

		if(!dosearchEverything)
		{
			for(int i = 0 ; i < types_values.length; i++)
			{
				if(types_values[i].contains("/freetext/"))
					 usernameList.add(types_values[i].substring("/freetext/".length(), types_values[i].length()));				
			}
		}else{
			usernameList.add(__DEFAULT_SEARCH_EVERYTHING__); //add a name indicating to search everything
		}
		
		HashMap<String, JSONArray> UserJSONTermResultMap = new HashMap<String, JSONArray>();

		for(Iterator<String> iter = usernameList.iterator() ; iter.hasNext();)
		{
			String username = iter.next();
			JSONObject jsonCompleteResults = null;
			
				jsonCompleteResults = SPARQLEndPointAccess.sendQueryGetJSON(makeGetUserFreeTextTagsQuery(username, prefix, dosearchEverything));
			
			if(jsonCompleteResults != null)
			{
				JSONArray jsonResultArray;
				try {
					jsonResultArray = jsonCompleteResults.getJSONObject("results").getJSONArray("bindings");
					if(jsonResultArray.length() > 0)
						UserJSONTermResultMap.put(username, jsonResultArray);
				} catch (JSONException e) {
	
					e.printStackTrace();
				}
			}		
		}
	
		HashMap<String, FreebaseJSONResult> ResultsMap= new HashMap<String, FreebaseJSONResult>();
		Set keySet = UserJSONTermResultMap.keySet();
		int total = 0;
		int start_point = start;
		for(Iterator<String> iter = keySet.iterator(); iter.hasNext();)
		{
			
			try {
					String username = iter.next();
					JSONArray jsonResultArray = UserJSONTermResultMap.get(username);
					
					//check if total is less than start
					total = total + jsonResultArray.length();
					if(total < start)
					{
						start_point = start_point - jsonResultArray.length();
						continue;
					}
					
					for(int i=start_point; i < jsonResultArray.length(); i++)
					{
						JSONObject termResult = jsonResultArray.getJSONObject(i);
						String term = termResult.getJSONObject("taglabel").getString("value");
						String user = termResult.getJSONObject("user").getString("value");
						user = user.replaceAll(__SEMANTIC_TAGGING_URI_PREFIX__, ""); //take out prefix

						if(ResultsMap.containsKey(term)) //existing term
						{
							ResultsMap.get(term).addType(__FREETEXT_PREFIX_ID__ + user, __FREETEXT_PREFIX_NAME__ + user);
						}else{
									//new term
									FreebaseJSONResult fbResult = new FreebaseJSONResult();
									
									fbResult.setName(term);
									fbResult.setid(__SEMANTIC_TAGGING_URI_NOTHING__);
									fbResult.setGuid(__SEMANTIC_TAGGING_URI_NOTHING__);
									fbResult.addType(__FREETEXT_PREFIX_ID__ + user, __FREETEXT_PREFIX_NAME__ + user);
									fbResult.setOntology(__SEMANTIC_TAGGING_URI_PREFIX__);
									
									ResultsMap.put(term, fbResult);
									if(start_point > 0)
										start_point--;
							 }
						
						if(limit != null && ResultsMap.size() >= limit) //break if greater than limit
							break;
					}
					
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			if(limit != null && ResultsMap.size() >= limit) //break if greater than limit
				break;
		}

		JSONObject JSONResults = null;
		FreebaseJSONSearchResponse fbSearchResponse = new FreebaseJSONSearchResponse();
		try {
					fbSearchResponse.setStatus(fbSearchResponse.STATUS_OK);
					fbSearchResponse.setCode(fbSearchResponse.CODE_OK);

					Collection<FreebaseJSONResult> ResultsCollection = ResultsMap.values();
				    
					ArrayList<FreebaseJSONResult> ResultsArrayList = new ArrayList<FreebaseJSONResult>(ResultsCollection);
					
					Collections.sort(ResultsArrayList, new Comparator<FreebaseJSONResult>() {
						public int compare(FreebaseJSONResult o1, FreebaseJSONResult o2) {
						    int i = 0;
							try {
								i = o1.getName().compareTo(
								    o2.getName());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							return i;
						}
					    });
					fbSearchResponse.setResult(ResultsArrayList);
					JSONResults = fbSearchResponse.toJSON();

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		
		
		return  JSONResults;

	}
	/**
	 * Takes a word eg. sparql and returns a string [Ss][Pp][Aa][Rr][Qq][Ll]
	 * Work around for getting case insensitive regex queries to the SPARQL Endpoint
	 * @param word
	 * @return String 
	 */
	public String case_sen_workaround(String word)
	{
		StringBuffer sb = new StringBuffer();

		char[] chararray = word.toCharArray();
		for(int i=0 ; i < word.length(); i++)
		{
			char ch = chararray[i];
			Character character = new Character(ch);
			sb.append("[");
			if(character.isLetter(ch))
			{
				sb.append(character.toString().toUpperCase() + character.toString().toLowerCase());
			}else{
				sb.append(character.toString());
			}
			sb.append("]");
			          
		}
		
		return sb.toString();
	}
	
	/**
	 * Make a JSONObject 
	 * @return JSONObject
	 */
	public JSONObject makefreebaseAPIJSONResults()
	{
		JSONObject fbSearchResults = new JSONObject();
		return fbSearchResults;
	}

	/**
	 * Make Query for Getting Freetext tags that 
	 * @param user_uri
	 * @param prefix
	 * @param searchEverything
	 * @return String representing the Freetext tag query
	 */
	public String makeGetUserFreeTextTagsQuery(String user_uri, String prefix, boolean searchEverything)
	{	String cs_prefix = case_sen_workaround(prefix);
		StringBuffer sb = new StringBuffer();
		
		sb.append(SPARQLEndPointAccess.prefix_tag + "\n");
		sb.append(SPARQLEndPointAccess.prefix_rdfs + "\n");
		sb.append("	SELECT Distinct ?taglabel ?user \n");
		sb.append("	WHERE{ \n");
		if(!searchEverything)
		{		
			sb.append("	?tagging tag:taggedBy ");  
			sb.append("<"+ SPARQLEndPointAccess.ns + user_uri + "> . \n");
		}
		sb.append(" ?tagging tag:associatedTag ?tag . \n");
		sb.append(" ?tagging tag:taggedBy ?userID . \n");
		sb.append(" ?userID tag:name ?user . \n");
		sb.append(" ?tag rdfs:label ?taglabel . \n");	
		sb.append(" ?tag rdfs:isDefinedBy <" + SPARQLEndPointAccess.Nothing_uri + "> \n");
		sb.append(" FILTER regex(str(?taglabel), \"" + cs_prefix + "\") \n");		
		sb.append("}");	
	
		return sb.toString();
	}	
}
