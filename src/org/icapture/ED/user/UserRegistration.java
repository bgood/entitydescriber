package org.icapture.ED.user;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.icapture.ED.EDAuthenticator;
import org.icapture.ED.config.EdConfig;
import org.icapture.ED.dataAccess.ResponseInfo;
import org.icapture.ED.dataAccess.SPARQLEndPointAccess;
import org.icapture.web.HttpUtil;
import org.json.JSONException;

/**
 * A User Registration Servlet used to save new user information.
 * @author Paul Lu
 *
 */
public class UserRegistration extends HttpServlet 
{
	private String root_domain = EdConfig.getED_root_url();

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * Dispatches a new user to the userinfo jsp
	 *  
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String paramString = request.getQueryString() != null ? "?" + request.getQueryString() : "";

		//GAUNTLET: CHECK IF USER IS LOGGED IN
		if (!EDAuthenticator.isHashCookieValid(request))
		{
			response.sendRedirect(root_domain + "openid.html" + paramString);
			return;
		}
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/userinfo.jsp");
	 	dispatcher.forward(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * Takes the form from the userinfo jsp and saves the user information to 
	 * the SPARQL Endpoint before redirecting to the main describer page
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nickname = request.getParameter("userinfoNickname");
		String USR_HASH = HttpUtil.getCookieValue(request.getCookies(), "EDES_USERHASH", null);
		
		String usrURI = SPARQLEndPointAccess.ns + USR_HASH;
		String userInsert = makeUserInsertStatement(USR_HASH, nickname);
		
			ResponseInfo respInfo = SPARQLEndPointAccess.sendQueryOrUpdate(userInsert);
			
			if(respInfo.getResponseCode() == HttpStatus.SC_OK)
			{	System.out.println(nickname + " registered with " + usrURI);
				//create username cookie
				Cookie EDES_USERNAME = new Cookie("EDES_USERNAME", nickname);
				response.addCookie(EDES_USERNAME);
				String paramString = request.getQueryString() != null ? request.getQueryString() : "";
				response.sendRedirect(root_domain + "describer" + paramString);
			}	
			
	}

	/**
	 * Creates a sparql insert statement for a user
	 * @param userid The userid 
	 * @param nickname The nickname associated with the userid
	 * @return a sparql insert statement
	 */
	public String makeUserInsertStatement(String userid, String nickname)
	{
		
		String user_entry = "<" + SPARQLEndPointAccess.ns + userid + ">";
		StringBuffer sb = new StringBuffer();
		sb.append(SPARQLEndPointAccess.prefix_rdf + "\n");  
		sb.append(SPARQLEndPointAccess.prefix_tag + "\n");
		sb.append(SPARQLEndPointAccess.prefix_rdfs + "\n");
		sb.append("INSERT { \n");
		sb.append(user_entry + " rdf:type <" +SPARQLEndPointAccess.ns + "User> ; \n");
		
		if(nickname != null)
			sb.append("tag:name \"" + nickname + "\"@EN");

		sb.append("}");
		
		return sb.toString();
	}
}
