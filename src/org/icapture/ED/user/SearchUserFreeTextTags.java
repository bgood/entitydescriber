package org.icapture.ED.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.icapture.ED.dataAccess.SPARQLEndPointAccess;
import org.icapture.ED.freebase.FreebaseJSONResult;
import org.icapture.ED.freebase.FreebaseJSONSearchResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Servlet Wrapper for UserFreeTextTagSearcher
 * Handles no Types by searching everyone
 * Handles 0 to any Types
 * Handles start
 * Handles limit
 * TODO:
 * strict
 * @author Paul Lu
 *
 */
public class SearchUserFreeTextTags extends HttpServlet {

	private static final String __DEFAULT_SEARCH_EVERYTHING__ = "__SEARCH_EVERYTHING__";
	private static final String __SEMANTIC_TAGGING_URI_PREFIX__ = SPARQLEndPointAccess.ns;
	private static final String __SEMANTIC_TAGGING_URI_NOTHING__ = SPARQLEndPointAccess.Nothing_uri;
	private static final String __FREETEXT_PREFIX_ID__ = "/freetext/";
	private static final String __FREETEXT_PREFIX_NAME__ = "used_by_";
	private static final String __PREFIX__ = "prefix";
	
	private UserFreeTextTagSearcher FTagSearcher = new UserFreeTextTagSearcher();
	
	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * Processes the parameters given to it and formats the Search Results for 
	 * User Freetext Tags to a JSONObject conforming to the Entity Describer data 
	 * source API
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ArrayList<String> usernameList = new ArrayList<String>();
		
		//type will be of form: /freetext/user.name.blogspot.com/
		boolean searchEverything = false;
		String callback = request.getParameter("callback");
		String prefix = request.getParameter(__PREFIX__);
		String query = request.getParameter("query");		
		String escape = request.getParameter("escape");
		Integer limit = request.getParameter("limit") != null ? Integer.valueOf(request.getParameter("limit")) : 20;
		Integer start = request.getParameter("start") != null ? Integer.valueOf(request.getParameter("start")) : 0;
			
		String strict = request.getParameter("strict");
		String[] types_values = request.getParameterValues("type");

		if((prefix == null && query == null) || callback == null) //missing essential parameter - print error
		{
			ArrayList<String> messages = new ArrayList();
			if(prefix == null && query == null)
				messages.add("You must specify a query or a prefix");
			if(callback == null)
				messages.add("You must specify a callback");
			
			FreebaseJSONSearchResponse fbErrorResponse = new FreebaseJSONSearchResponse(FreebaseJSONSearchResponse.STATUS_BAD_REQUEST, 
																						FreebaseJSONSearchResponse.CODE_BAD_REQUEST, 
																						new JSONObject(), 
																						messages.get(0), 
																						FreebaseJSONSearchResponse.CODE_MISSING_FIELD);
			messages.remove(0);
			for(Iterator<String> i = messages.iterator(); i.hasNext();)
			{
				try {
					fbErrorResponse.addMessage(i.next(), new JSONObject(), FreebaseJSONSearchResponse.CODE_MISSING_FIELD);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			printFreebaseSearchResponse(response, fbErrorResponse, callback);
			return;
		}

		if(prefix == null) //set prefix to query if prefix is not present
		{
				prefix = query;	
		}

		
		if(types_values == null) //check types
			searchEverything = true;

		JSONObject fbSearchResponse = FTagSearcher.searchFreeText(prefix, searchEverything, start, limit, types_values);
		printFreebaseSearchResponse(response, fbSearchResponse, callback);		
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

	/**
	 * Takes the fbSearchResponse object and prints the search results 
	 * @param response
	 * @param fbSearchResponse
	 * @param callback
	 * @throws IOException
	 */
	public void printFreebaseSearchResponse(HttpServletResponse response, FreebaseJSONSearchResponse fbSearchResponse, String callback) throws IOException
	{
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();		
		
			try {
				out.println(callback + "(");
				out.print(fbSearchResponse.toJSON().toString(5));
				out.println(")");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.flush();
			out.close();
	}
	
	/**
	 * Takes the fbSearchResponse JSONObject and prints the search results 
	 * @param response
	 * @param fbSearchResponse (JSONObject)
	 * @param callback
	 * @throws IOException
	 */
	public void printFreebaseSearchResponse(HttpServletResponse response, JSONObject fbSearchResponse, String callback) throws IOException
	{
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();		
		
			try {
				out.println(callback + "(");
				out.print(fbSearchResponse.toString(5));
				out.println(")");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.flush();
			out.close();
	}
	
}
