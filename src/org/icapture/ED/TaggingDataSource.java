package org.icapture.ED;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 * Keeps default values for the DataSource (www.freebase.com) and 
 * default values for the SPARQLEndpoint but
 * contains methods for setting new values for both DataSource and SPARQLEndpoint
 * and setting those values in ED on deployment by calling the process method
 * in the context loader
 * 
 * @author Ed_Kawas
 *
 */
public class TaggingDataSource {

	/*DATA SOURCE DEFAULT VALUES: */
    private String ac_param = "{" 
	    + "			type: '/common/topic', "
	    + "			category: \"instance\", " 
	    + "			get_all_types: \"0\", "
	    + "			disamb: \"1\", " 
	    + "			limit: \"10\", "
	    + "			strict:\"true\"" 
	    + "		}";

    private String service_url = "http://www.freebase.com";

    private String ac_path = "/api/service/search";

    private String ac_qstr = "prefix";

    private String blurb_path = "/api/trans/blurb";

    private String blurb_param = "{ maxlength: 300 }";

    private String thumbnail_path = "/api/trans/image_thumb";

    private String default_type = "/common/topic";

    /**SPARQLENDPOINT DEFAULT VALUES: **/

    /**SPARQLENDPOINT DEFAULT VALUES FOR ED: **/
    private static String SPARQL_EP_DATASOURCE_url = "http://biomoby.elmonline.ca/sparql";

    private static String SPARQL_EP_DATASOURCE_query_param_name = "query";
    
    private static String SPARQL_EP_DATASOURCE_parameters = null;
    /*
     * Static Beacuse they will be used by other classes outside of what is being done in the ContextLoader
     */
    
    /*DATASOURCE VARIABLE NAMES: */

    public static String AC_PARAM = "ac_param";
    public static String SERVICE_URL = "service_url";
    public static String AC_PATH = "ac_path";
    public static String AC_QSTR = "ac_qstr";
    public static String BLURB_PATH = "blurb_path";
    public static String BLURB_PARAM = "blurb_param";
    public static String THUMBNAIL_PATH = "thumbnail_path";
    public static String DEFAULT_TYPE = "default_type";
    
    /*SPARQLENDPOINT VARIABLE NAMES FOR ED: */
    public static String SPARQL_EP_DATASOURCE_URL = "SPARQL_EP_DATASOURCE_url";
    public static String SPARQL_EP_DATASOURCE_QUERY_PARAM_NAME = "SPARQL_EP_DATASOURCE_query_param_name";
    public static String SPARQL_EP_DATASOURCE_PARAMETERS = "SPARQL_EP_DATASOURCE_parameters";    
    
    /**
     * Default constructor.
     */
    public TaggingDataSource(){
	
    }

    /**DATASOURCE GETTERS & SETTERS**/   
    /**
     * @return the ac_param
     */
    public String getAc_param() {
        return ac_param;
    }

    /**
     * @param ac_param the ac_param to set
     */
    public void setAc_param(String ac_param) {
        this.ac_param = ac_param;
    }

    /**
     * @return the service_url
     */
    public String getService_url() {
        return service_url;
    }

    /**
     * @param service_url the service_url to set
     */
    public void setService_url(String service_url) {
        this.service_url = service_url;
    }

    /**
     * @return the ac_path
     */
    public String getAc_path() {
        return ac_path;
    }

    /**
     * @param ac_path the ac_path to set
     */
    public void setAc_path(String ac_path) {
        this.ac_path = ac_path;
    }

    /**
     * @return the ac_qstr
     */
    public String getAc_qstr() {
        return ac_qstr;
    }

    /**
     * @param ac_qstr the ac_qstr to set
     */
    public void setAc_qstr(String ac_qstr) {
        this.ac_qstr = ac_qstr;
    }

    /**
     * @return the blurb_path
     */
    public String getBlurb_path() {
        return blurb_path;
    }

    /**
     * @param blurb_path the blurb_path to set
     */
    public void setBlurb_path(String blurb_path) {
        this.blurb_path = blurb_path;
    }

    /**
     * @return the blurb_param
     */
    public String getBlurb_param() {
        return blurb_param;
    }

    /**
     * @param blurb_param the blurb_param to set
     */
    public void setBlurb_param(String blurb_param) {
        this.blurb_param = blurb_param;
    }

    /**
     * @return the thumbnail_path
     */
    public String getThumbnail_path() {
        return thumbnail_path;
    }
    
    /**
     * 
     */
    public String getDefault_type() {
    	return default_type;
    }
    
    /**
     * 
     * @param default_type
     */
    public void setDefault_type(String default_type) {
    	this.default_type = default_type;
    }
    /**
     * @param thumbnail_path the thumbnail_path to set
     */
    public void setThumbnail_path(String thumbnail_path) {
        this.thumbnail_path = thumbnail_path;
    }

    /**
     * @return the url of the SPARQL Endpoint used to retrieve data
     */
    public static String getSPARQL_EP_DATASOURCE_url() 
    {
    	return SPARQL_EP_DATASOURCE_url;
    }
    
    /**
     * @param url the url of the SPARQL Endpoint to send queries to
     */
    public static void setSPARQL_EP_DATASOURCE_url(String url) 
    {
    	SPARQL_EP_DATASOURCE_url = url;
    }
    
    /**
     * @return name of the query paramter used by the SPARQLEndpoint
     */
    public static String getSPARQL_EP_DATASOURCE_query_param_name() 
    {
    	return SPARQL_EP_DATASOURCE_query_param_name;
    }
    
    /**
     * @param query_param_name name of the query paramter used by the SPARQLEndpoint
     */
    public static void setSPARQL_EP_DATASOURCE_query_param_name(String query_param_name) 
    {
    	SPARQL_EP_DATASOURCE_query_param_name = query_param_name;
    }
    
    /**
     * @return A String reresentation of the JSONObject containing the default parameters for the SPARQL Endpoint
     */
    public static String getSPARQL_EP_DATASOURCE_parameters()
    {
    	return SPARQL_EP_DATASOURCE_parameters;
    }
    
    /**
     * @param parameters A String representation of the JSONObject containing the default parameters for the SPARQL Endpoint
     */
    public static void setSPARQL_EP_DATASOURCE_parameters(String parameters)
    {
    	SPARQL_EP_DATASOURCE_parameters = parameters;
    }

}
