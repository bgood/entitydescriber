package org.icapture.ED.describer;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.icapture.ED.EDAuthenticator;
import org.icapture.ED.config.EdConfig;
import org.icapture.web.HttpUtil;

/**
 * Servlet used to manage Entity Describer bookmark page.  Checks if the user exists and if
 * it's secure Cookie is valid before proceeding to dispatch it to the bookmark page (describer.jsp)
 * @author Paul Lu
 *
 */
public class Describer extends HttpServlet 
{
	private String root_domain = EdConfig.getED_root_url();

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * Checks to make sure the User is logged in and dispatches to the bookmark describer page 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String paramString = request.getQueryString() != null ? "?" + request.getQueryString() : "";
		//GAUNTLET: CHECK IF USER EXISTS
		String USERNAME = HttpUtil.getCookieValue(request.getCookies(), "EDES_USERNAME", null);			
		String USERHASH = HttpUtil.getCookieValue(request.getCookies(), "EDES_USERHASH", null);
		
		//GAUNTLET: CHECK IF USER IS LOGGED IN
		if (!EDAuthenticator.isHashCookieValid(request) || USERHASH == null)
		{
			response.sendRedirect(root_domain + "openid.html" + paramString);
			return;
		}
		
		if(USERNAME == null)
		{
			String nickname = EDAuthenticator.doesUserExist(USERHASH);
			if(nickname == null)
			{
				response.sendRedirect(root_domain + "userinfo" + paramString);
				return;
			}else{
					Cookie EDES_USERNAME = new Cookie("EDES_USERNAME", nickname);
					USERNAME = nickname;
					response.addCookie(EDES_USERNAME);
				 }
		}
		
		request.setAttribute("EDES_USERNAME", USERNAME);
		request.setAttribute("EDES_USERHASH", USERHASH);

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/Describer.jsp");
	 	dispatcher.forward(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * Used to logout a user or send them to the tagging complete page
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		String logout = request.getParameter("logout");
		if(logout != null)
		{
			EDAuthenticator.logout(request, response);
			return;
		}else{
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/complete.jsp");
		 	dispatcher.forward(request, response);
		}
		
	}

}
