package org.icapture.ED.openID;


import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import java.io.IOException;

import java.net.URLDecoder;
import java.net.URLEncoder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import org.icapture.ED.EDAuthenticator;
import org.icapture.ED.config.EdConfig;
import org.icapture.web.HttpUtil;
import org.openid4java.OpenIDException;
import org.openid4java.association.AssociationException;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.MessageException;
import org.openid4java.message.MessageExtension;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.AxMessage;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.ax.FetchResponse;
import org.openid4java.message.sreg.SRegMessage;
import org.openid4java.message.sreg.SRegRequest;
import org.openid4java.message.sreg.SRegResponse;

import sun.misc.BASE64Encoder;

/**
 * OpenID Servlet for submitting a user's openID URL 
 * Handles the communication between this website and the openID provider
 * to verify the user's identity
 * @author Paul Lu
 *
 */
public class OpenIDHandler extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet 
{
	private String url_prefix = org.icapture.ED.config.EdConfig.getED_root_url();
	//suffix of the XRDS document file path
	private String file_loc = org.icapture.ED.config.EdConfig.getED_yadis_url();
	private ConsumerManager manager = null;
	private MessageDigest md = null;
	private String nextPage = null;
	private boolean hasParams = false;
	
	
	static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(OpenIDHandler.class);

	/**
	 * Initialization of the servlet. <br>
	 * @throws ServletException if an error occurs
	 */
	public void init(ServletConfig config) throws ServletException {
		
		super.init(config);
		try {
			//Create new ConsumerManager to manage the openID
			this.manager = new ConsumerManager();
			manager.getRealmVerifier().setEnforceRpId(false); 
		} catch (ConsumerException e) {
			throw new ServletException(e);
		}
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to Get.
	 * This method is called to produce the openID login form
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		//value of return parameter
		String rtn = request.getParameter("return");
		String logout = request.getParameter("logout");

		//Points to where the Yadis document is in the header for whatever OP is looking for it
		if(rtn == null && logout == null) 
		{
			String xrdf_path = file_loc;
			//Add this to header so that OpenID provider knows where to find the xrds document
			response.addHeader("X-XRDS-Location", xrdf_path);
			return;
		}
		
		if(rtn != null)
		{
			String endURL = null;
			String nextpage = HttpUtil.getCookieValue(request.getCookies(), "NEXTPAGE", null);
			String paramString = HttpUtil.getCookieValue(request.getCookies(), "PARAMSTRING", null);
			String destination_url = url_prefix + HttpUtil.getCookieValue(request.getCookies(), "NEXTPAGE", null);
	
			if(paramString != null && paramString.compareTo("") != 0)
			{
				destination_url = destination_url + URLDecoder.decode(paramString, "UTF-8");
			}
			
			EDAuthenticator.removeCookie(request, response, "NEXTPAGE");
			EDAuthenticator.removeCookie(request, response, "PARAMSTRING");
			
			String error_url = url_prefix + "error.html?error=authenticating";
			
			
			if(doVerification(request, response))//call doVerification to handle the verification of the openID Provider's response
			{
				endURL = destination_url;
				
			}else{
				endURL = error_url;

			}
				response.sendRedirect(endURL);
				return;
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * OpenID Login form has been submitted to this servlet for processing and authentication
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		//save the parameter String
		String paramString = request.getParameter("paramString");
		System.out.println("paramString: " + paramString);
		System.out.println(url_prefix);
		EDAuthenticator.removeCookie(request, response, "NEXTPAGE"); //get rid of the old one
		EDAuthenticator.removeCookie(request, response, "PARAMSTRING");
		//save next page
		String nextPage = request.getParameter("nextPage");
		
		//need to do this otherwise cookie cuts the string up where there's a '?' or a '='
		paramString = URLDecoder.decode(paramString, "UTF-8"); //decode first to get EVERYTHING decoded
		paramString = URLEncoder.encode(paramString, "UTF-8"); //then re-encode to make sure everything IS encoded
		
		Cookie NEXTPAGE = new Cookie("NEXTPAGE", nextPage); //create username cookie
		Cookie PARAMSTRING = new Cookie("PARAMSTRING", paramString); //create username cookie
		response.addCookie(NEXTPAGE);
		response.addCookie(PARAMSTRING);
		
		// OpenID URL has been submitted for authentication via the login
		String error_url = url_prefix + "error.html?error=authenticating";
		String openid_url = request.getParameter("openid_url"); //get openid url
		
		if(openid_url != null) //trim openid url
			openid_url = openid_url.trim();
		
		//set session attributes - information from the login 
		HttpSession curr_session = request.getSession();
			curr_session.setAttribute("openid_url", openid_url);
		//	curr_session.setAttribute("original_paramMap", originalParams);
			
			boolean isError = false;
			/*Place the Authentication Request by calling authRequest*/
		
			try 
			{
				this.authRequest(openid_url, request, response);
			} catch (DiscoveryException e) {
				isError = true;
					logger.error("Problem during OP discover - check openid url");
					logger.error(e.getMessage());
					e.printStackTrace();		
			} catch (MessageException e) {
				isError = true;
					logger.error("Problem in creating AuthRequest -");
					logger.error(e.getMessage());
					e.printStackTrace();
			} catch (ConsumerException e) {
				isError = true;
					logger.error(e.getMessage());
					e.printStackTrace();
			}
		
			if(isError)
				response.sendRedirect(error_url);
	}

	/**
	 * Places the Authentication Request to the OpenID Provider	
	 * 
	 * @param userSuppliedString The openID URL provided by the user
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 *
	 * @throws IOException
	 * @throws DiscoveryException 
	 * @throws ConsumerException 
	 * @throws MessageException 
	 * @throws ServletException 
	 * @throws ServletException
	 */
		public void authRequest(String userSuppliedString, HttpServletRequest request, HttpServletResponse response) 
		throws IOException, DiscoveryException, MessageException, ConsumerException, ServletException 
		{
			//the return to URL where OP will look for your Yadis document
				String returnToUrl = url_prefix + request.getServletPath().substring(1); 
			//Discover the openID provider and check the openid is with them
				List discoveries = manager.discover(userSuppliedString); 
			//establish association with OP and get an OP service endpoint for authentication
				String destination_url = request.getParameter("destination_url") != null ? (String)request.getParameter("destination_url") : null;
				
				DiscoveryInformation discovered = manager.associate(discoveries); 
					request.getSession().setAttribute("openid-disc", discovered);
					request.getSession().setAttribute("destination_url", destination_url);
			//obtain a AuthRequest message to be sent to the OpenID provider
			//NOTE: Calling authenticate will cause your server to perform a GET on your returntoURL for the Yadis document
				AuthRequest authReq = manager.authenticate(discovered, returnToUrl, url_prefix);
					String processed_returnToURL = authReq.getReturnTo();
				
				if(processed_returnToURL.contains("?"))
				{
					returnToUrl = processed_returnToURL + "&return=true";
				}else{
						returnToUrl = processed_returnToURL + "?return=true";
					 }
				
				authReq.setReturnTo(returnToUrl); //add return parameter to authReq to distinguish between a Yadis document request and returning from the OP 
				
			//Print Values in authRequest		
				HashMap authParams = (HashMap) authReq.getParameterMap();
				Set keyset =  authParams.keySet();
		
				System.out.println("AUTHREQUEST PARAMETERS");
				for ( Iterator<String> paramIter = keyset.iterator(); paramIter.hasNext(); ) 
				{
					String key = paramIter.next();
					System.out.println(key + " : " + authParams.get(key));
				}

			//Here is where you would fetch information if it is a new user to your website and you want information on them
					// Most sites still work with SREG 
					/*
						SRegRequest sregReq = SRegRequest.createFetchRequest();
							sregReq.addAttribute("email", true);
							sregReq.addAttribute("nickname", true);
						authReq.addExtension(sregReq); //add on to authReq
					*/
					// Fetch
					/*
						FetchRequest fetch = FetchRequest.createFetchRequest(); 	
							fetch.addAttribute("email", //attribute alias
							"http://schema.openid.net/contact/email", // type URI
							true); // required
						authReq.addExtension(fetch); // attach fetch to the authreq
					*/
			
			//Figure out the version of openid the OP uses and send them the parameters in the appropriate way		
			if (!discovered.isVersion2()) 
			{ 
				response.sendRedirect(authReq.getDestinationUrl(true));
				return;
			} else	
				{
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/FormRedirect.jsp");
				 	request.setAttribute("parameterMap", authReq.getParameterMap());
				 	request.setAttribute("OPEndpoint", authReq.getOPEndpoint()); 
				 dispatcher.forward(request, response);
				}
			return;
		}
	
/**
 * Verifies the OpenID upon return from the OpenID Provider and creates 
 * the secure cookies to identify a user.
 * @param request
 * @param response
 * @return boolean - if response from OP is verified or not
 * @throws IOException
 */	
public boolean doVerification(HttpServletRequest request, HttpServletResponse response) throws IOException
{
	//will be null if user's openid uri is invalid
	Identifier iden = verifyResponse(request, response);
	
	if(iden != null) //is valid
	{
		String openID = iden.getIdentifier();  //get the openid of the user and cut off the '#' if there is one and take off the protocol
		if(openID.lastIndexOf("#") != -1)
			openID = openID.substring(0, openID.lastIndexOf("#"));
			openID = openID.split("://")[1]; 
			
		
		//make cookies
			Cookie ED_USERHASH = EDAuthenticator.makeUserHashCookie(openID); //create username cookie
			Cookie ED_HASH = EDAuthenticator.makeHashCookie(openID, System.currentTimeMillis()); //create hash cookie
		
		//Add cookies
			response.addCookie(ED_USERHASH);
			response.addCookie(ED_HASH);
		
			return true;
	}else{ 			
			return false;	
		 }
}
	
	

	/**
	 * Process the Response from the OpenID Provider. <br>
	 * Determines if the OpenID provider has verified the identity of the user.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @return verified is null if the identity is not verified
	 * @throws IOException 
	 */
	public Identifier verifyResponse(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Identifier verified = null;
		
		try {
			
		//extract the parameters from the authentication response
			ParameterList resp_params = new ParameterList(request.getParameterMap());
		//retrieve the previously stored discovery information
			DiscoveryInformation discovered = (DiscoveryInformation) request.getSession().getAttribute("openid-disc");
		//create receiving URL 
			StringBuffer receivingURL = new StringBuffer();
			receivingURL.append(url_prefix + request.getServletPath().substring(1));
		
		//append query String to receiving URL	
			String queryString = request.getQueryString();
			
			if (queryString != null && queryString.length() > 0)
				receivingURL.append("?").append(request.getQueryString());

		//verify the response; ConsumerManager needs to be the same
			VerificationResult verification = manager.verify(receivingURL.toString(), resp_params, discovered);

		//examine the verification result and extract the verified identifier.  if null - not verified
			verified = verification.getVerifiedId();
			
			if (verified != null) 
			{
				AuthSuccess authSuccess = (AuthSuccess) verification.getAuthResponse();
				
			/* FETCHING information on a person who has not registered for your website */
			
			/*	SReg works with vidoop and myopenIid but not with yahoo.
			 * 	SReg is the old way of doing it with OpenID version 1.0.  Some OPs
			 * 	still use it.
			 */	
				
				/*	
					if (authSuccess.hasExtension(SRegMessage.OPENID_NS_SREG)) { //check for extension
						//Get the extension
						MessageExtension ext = authSuccess.getExtension(SRegMessage.OPENID_NS_SREG);
						//check type
						if (ext instanceof SRegResponse)
						{
							SRegResponse sregResp = (SRegResponse) ext; //cast
							String email = sregResp.getAttributeValue("email"); //pull out email
							String nickname = sregResp.getAttributeValue("nickname");
							System.out.println(email);
							System.out.println(nickname);
						}
					}
				*/
				
			/*	NS_AX is the current way of fetching user information for OpenID version 2.0
			 * 	OPs conform to this.  Haven't gotten it working yet due to my java libraries for
			 *  OpenID being out of date. Will get newest later.  Please check eddevloper blogspot for
			 *  more details
			 */
				/*
					if(authSuccess.hasExtension(AxMessage.OPENID_NS_AX))
					{
						MessageExtension ext = authSuccess.getExtension(AxMessage.OPENID_NS_AX);
						if(ext instanceof FetchResponse)
						{
							FetchResponse fetchResp = (FetchResponse) ext; 
							String email = fetchResp.getAttributeValue("email");
							System.out.println(email);
							
						}
					}
				*/
				
				
				return verified; // success if verified != null
			}
		} catch (OpenIDException e) {
			// present error to the user
			return verified;
		}
		return verified;
	}

}
